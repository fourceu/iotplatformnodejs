// Import Axios to fetch data from FourC DB
const axios = require("axios");

// Import jsonwebtoken for generating tokens
const jwt = require("jsonwebtoken");

// Import config module to access env configurations
const config = require("config");

// Import bcrypt to unhash password
const bcrypt = require("bcrypt");

// Import joi for validation
const Joi = require("joi");

// Import UsersModel to get all users
const { Get4CUsers } = require("./UsersModel");

const express = require("express");
const router = express.Router();

router.post("/", async (req, res) => {
  const { error } = validate(req.body);
  if (error) return res.status(400).send(error.details[0].message);

  let request = {
    email: req.body.email,
    password: req.body.password,
  };

  //   console.log(request);

  const users = await Get4CUsers();

  let user = users.find((item) => item.email === request.email);
  if (!user) return res.status(400).send("Invalid email or password!");

  const validPassword = await bcrypt.compare(request.password, user.password);
  if (!validPassword) return res.status(400).send("Invalid email or password!");

  //   console.log(user);

  const token = jwt.sign(
    { id: user.id, api_key: user.api_key },
    config.get("jwtPrivateKey")
  );

  res.send({ token: token });
});

function validate(req) {
  const schema = Joi.object({
    email: Joi.string().min(5).max(255).required().email(),
    password: Joi.string().min(5).max(255).required(),
  });

  return (result = schema.validate(req));
}

module.exports = router;
