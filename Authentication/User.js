// Import custom auth middleware
const auth = require("../Middleware/auth");

// Import Users model module
const {
  Get4CUsers,
  validateUser,
  Create4CUser,
  Get4CUserById,
  ResetPassword,
} = require("./UsersModel");

// Import bcrypt to hash password
const bcrypt = require("bcrypt");

// For creating random api key
const cryptoRandomString = require("crypto-random-string");

// importng axios to get new created user
var axios = require("axios");

// Import jsonwebtoken for generating tokens
const jwt = require("jsonwebtoken");

// Import config module to access env configurations
const config = require("config");

const express = require("express");
const { route } = require("../Devices/devices");
const router = express.Router();

var newUser = {
  username: "",
  email: "",
  password: "",
  timestamp: "",
  api_key: "",
};

function ApiKeyGenerator() {
  let ApiKey = cryptoRandomString({ length: 30, type: "base64" });
  return ApiKey;
}

async function HashPassword(pass) {
  //number of rounds the algorithm takes to generate salt || default value=10
  const salt = await bcrypt.genSalt(10);
  const hashedPassword = await bcrypt.hash(pass, salt);
  return hashedPassword;
}

async function CreateUser(req, res) {
  const { error } = validateUser(req.body);
  if (error) return res.status(400).send(error.details[0].message);

  let date = new Date().toISOString().slice(0, 19).replace("T", " ");

  newUser = {
    username: req.body.username,
    email: req.body.email,
    password: await HashPassword(req.body.password),
    timestamp: date,
    api_key: ApiKeyGenerator(),
  };

  // get all users and verify username / email doesn't exist
  Get4CUsers().then((data) => {
    if (data.find((user) => user.email === newUser.email)) {
      res.status(400).send("Email already exists!");
      return;
    } else if (data.find((user) => user.username === newUser.username)) {
      res.status(400).send("Username already exists!");
      return;
    } else {
      // create new user in db
      Create4CUser(newUser).then((data) => {
        axios.get(data).then((response) => {
          this.response = {
            message: "New user created",
            item: response.data.result,
          };

          const token = jwt.sign(
            {
              id: response.data.result.id,
              api_key: response.data.result.api_key,
            },
            config.get("jwtPrivateKey")
          );

          res.header("x-auth-token", token).send(this.response);
        });
      });
    }
  });
}

// api endpoint to get users
router.get("/", auth, async (req, res) => {
  const users = await Get4CUsers();
  res.send(users);
});

// api endpoint to get user by id
router.get("/me", auth, async (req, res) => {
  const user = await Get4CUserById(req.user.id);
  // console.log(req.user.id)
  // console.log(user)

  if (user.length < 1) {
    res.send(`No data found with Id: ${req.params.id}`);
  }

  res.send(user[0]);
});

// api endpoint to create new user
router.post("/", (req, res) => {
  CreateUser(req, res);
});

// api endpoint to reset user password
router.post("/resetpassword", async (req, res) => {
  const email = req.body.email;
  const resetPassword = req.body.password;
  const confirmPassword = req.body.confirm_password;

  if (!email)
    return res.status(400).send("Email is required for password reset!");
  if (!resetPassword)
    return res.status(400).send("New Password must be provided.");
  if (resetPassword !== confirmPassword)
    return res
      .status(400)
      .send("Password and Confirm Password should be same.");
  
  const users = await Get4CUsers()
  const user = users.find(u => u.email === email)
  
  if(!user) return res.status(404).send('Email id does not exist!')
  const id = user.id

  const hashResetPassword = await HashPassword(resetPassword);

  const resp = await ResetPassword(id, hashResetPassword);
  res.send(resp);
});

module.exports = router;
