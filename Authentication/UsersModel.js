// Import Axios to fetch data from FourC DB
const axios = require("axios");

// Import joi for validation
const Joi = require("joi");

// Import json sort library to sort incoming data form db
var sortJsonArray = require("sort-json-array");

// Getting user data from fourc db
const url4CMain = "http://10.44.45.80:8080";
const url4cUsers = url4CMain + "/access/users";

function Get4CUsers() {
  return axios
    .get(url4cUsers)
    .then((users) => {
      this.response = sortJsonArray(users.data.result, "id", "des"); //returned array in descending order
      return this.response;
    })
    .catch((error) => {
      this.response = {
        message: "Error communicating to the database",
        error: error.message,
      };
      return this.response;
    });
}

function Get4CUserById(id) {
  return axios.get(`${url4cUsers}(${id})`).then((user) => {
    this.response = user.data.result;
    return this.response;
  });
}

async function Create4CUser(data) {
  var newUser = JSON.stringify(data);

  return await axios
    .post(url4cUsers, (data = newUser))
    .then((response) => {
      this.response = response.headers.location;
      return this.response;
    })
    .catch((error) => {
      this.response = {
        message: "Error creating new user!",
        error: error,
      };
      return this.response;
    });
}

async function ResetPassword(id, newPassword) {
  // let users;
  const resetPasswordData = {
    password: newPassword,
  };

  // // get users from db and find user with same email
  // await axios
  //   .get(url4cUsers)
  //   .then((resp) => {
  //     users = resp.data.result;
  //   })
  //   .catch((err) => console.log(err));

  // const user = users.find((i) => i.email === email);
  // const id = user.id;

  // push new password in database
  return await axios
    .patch(
      `${url4cUsers}(id=${id})`,
      (data = JSON.stringify(resetPasswordData))
    )
    .then((resp) => {
      this.response = resp.data;
      return this.response;
    })
    .catch((err) => {
      this.response = {
        message: "Error creating new user!",
        error: error,
      };
      return this.response;
    });
}

function validateUser(data) {
  const schema = Joi.object({
    username: Joi.string().min(3).required(),
    email: Joi.string().min(5).max(255).required().email(),
    password: Joi.string().min(5).max(255).required(),
  });

  return (result = schema.validate(data));
}

module.exports = {
  Get4CUsers,
  Create4CUser,
  validateUser,
  Get4CUserById,
  ResetPassword,
};
