// Custom middleware function for login
function Login(req, res, next) {
  console.log("Logging...");
  // next is the reference to the next middleware function in the pipeline
  // without this, the req, res will be hanging, i.e. no response
  next();
}

// Custom middleware function for authenticating
function Authenticating(req, res, next) {
  console.log("Authenticating...");
  // next is the reference to the next middleware function in the pipeline
  // without this, the req, res will be hanging, i.e. no response
  next();
}

module.exports = { Login, Authenticating };
