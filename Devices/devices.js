// Import custom auth middleware
const auth = require("../Middleware/auth");
// Import json sort library to sort incoming data form db
var sortJsonArray = require("sort-json-array");

// Import 4C DB modules
const {
  Get4CDevices,
  Get4CDeviceById,
  Edit4CDevice,
  Delete4CDevice,
} = require("./FourCDB/devices4C");

// for demo: import dummy data
const data = require("../Data/devices.json");

// joi module for validation
// const Joi = require("joi");

const express = require("express");
const {
  Get4CTemperaturesById,
} = require("../Sensors/Temperature/FourCDB/temperature4C");
const { Get4CHumidityById } = require("../Sensors/Humidity/FourCDB/humidity4C");
const { Get4CPositionById } = require("../Sensors/GPS/FourCDB/position4C");
const { sortArrayDescId } = require("../Middleware/sort");
const router = express.Router();

// Route to get all devices
router.get("/", auth, async (req, res) => {
  // const devices = data;

  const devices = await Get4CDevices();
  if (devices?.error)
    return res.status(500).send("Error communicating with database");

  const devicesDescOrder = sortArrayDescId(devices);

  res.send(devicesDescOrder);
});

// Route to get a device by id
router.get("/:id", auth, async (req, res) => {
  // const device = data.find((d) => d.id === parseInt(req.params.id));

  const device = await Get4CDeviceById(req.params.id);

  if (!device)
    return res.status(404).send("No device found with ID: " + req.params.id);
  res.send(device);
});

// NOTE: device is created by python script so no api endpoint for that
router.post("/", (req, res) => {
  res.status(403).send("Creating a device with api is not allowed!");
});

// Route to edit a device
router.put("/", (req, res) => {
  if (!req.params.id)
    return res
      .status(404)
      .send("Use PATCH to edit a device | Please specify id in the header");
  res.send("Use PATCH to edit a device");
});
router.put("/:id", (req, res) => {
  if (!req.params.id)
    return res
      .status(404)
      .send("Use PATCH to edit a device | Please specify id in the header");
  res.send("Use PATCH to edit a device");
});

// Function to edit device using api endpoint
async function EditDevice(req, res) {
  editedItem = {
    active: null,
    // name: null,
    networkprovider_device_id: null,
    installation_location: null,
    track: null,
    offline_messageignoretime: null,
    group: null,
    networkprovider: null,
    product: null,
    display: null,
    battery_status: null,
    // humidity: [],
    // message: [],
    // position: [],
    // temperature: [],
  };

  editedItem.active = req.body.active;
  // editedItem.name = req.body.name;
  // editedItem.networkprovider_device_id = req.body.networkprovider_device_id;
  editedItem.installation_location = req.body.installation_location;
  editedItem.firmware_version = req.body.firmware_version;
  editedItem.track = req.body.track;
  editedItem.offline_messageignoretime = req.body.offline_messageignoretime;
  editedItem.group = req.body.group;
  editedItem.networkprovider = req.body.networkprovider;
  editedItem.product = req.body.product;
  editedItem.display = req.body.display;
  editedItem.battery_status = req.body.battery_status;
  // editedItem.humidity = req.body.humidity;
  // editedItem.message = req.body.message;
  // editedItem.position = req.body.position;
  // editedItem.temperature = req.body.temperature;

  await Edit4CDevice(req.params.id, editedItem)
    .then((response) => {
      if (response.error) {
        res.status(400).send(response);
      }
      res.send(response);
    })
    .catch((error) => {
      res.status(400).send(error);
    });
}

router.patch("/:id", auth, async (req, res) => {
  const device = await Get4CDeviceById(req.params.id);
  if (!device)
    return res.status(404).send("No device found with ID: " + req.params.id);

  if (req.body.name !== device.name) {
    return res
      .status(400)
      .send(
        "Not allowed to change the name!  Need to be changed from the respective provider."
      );
  } else if (
    req.body.networkprovider_device_id !== device.networkprovider_device_id
  ) {
    return res
      .status(400)
      .send(
        "Not allowed to change the network provider id for the device! Need to be changed from the respective provider."
      );
  }

  try {
    await EditDevice(req, res);
  } catch (ex) {
    console.log("Error: " + ex);
  }
});

// Route to delete a device
router.delete("/:id", auth, async (req, res) => {
  const device = await Get4CDeviceById(req.params.id);
  if (!device.data)
    return res.status(404).send("No device found with ID: " + req.params.id);

  const deleteResponse = await Delete4CDevice(req.params.id, device);

  res.status(204).send(deleteResponse);
});

// Route to get device temperatures
router.get("/:id/temperature", auth, async (req, res) => {
  const device = await Get4CDeviceById(req.params.id);
  if (!device.data)
    return res.status(404).send("No device found with ID: " + req.params.id);

  const deviceData = device.data;

  if (deviceData.temperature && deviceData.temperature.length > 0) {
    let deviceTemps = [];
    let arrayLength = deviceData.temperature.length;
    let tempIds = deviceData.temperature;
    for (let i = 0; i < arrayLength; i++) {
      let dTemp = await Get4CTemperaturesById(tempIds[i].id);
      deviceTemps.push(dTemp[0]);
    }

    return res.send(deviceTemps);
  } else {
    return res.status(404).send([]);
  }
});

// Get last hour temperature values of a device
router.get("/:id/temp-last-hour", auth, async (req, res) => {
  const device = await Get4CDeviceById(req.params.id);
  if (!device.data)
    return res.status(404).send("No device found with ID: " + req.params.id);

  if (device.data.temperature && device.data.temperature.length > 0) {
    // device.data.temperature.forEach(async (dt) => {
    //   var dTemp = await Get4CTemperaturesById(dt.id);
    //   var data = dTemp[0];
    //   // console.log(data);
    //   deviceTemps.push(data);
    //   console.log(deviceTemps);
    // });
    // axios.get(url4cTemperatures);

    let currentDateTime = new Date();

    let deviceTemps = [];
    let hourTempData = [];
    let arrayLength = device.data.temperature.length;
    let tempIds = device.data.temperature;
    for (let i = 0; i < arrayLength; i++) {
      var dTemp = await Get4CTemperaturesById(tempIds[i].id);
      // console.log(dTemp[0]);
      deviceTemps.push(dTemp[0]);
    }

    deviceTemps.forEach((d) => {
      let dt = new Date(d.timestamp);
      // console.log(Math.abs(currentDateTime.getTime() - dt.getTime()));
      if (Math.abs(currentDateTime.getTime() - dt.getTime()) <= 3600000) {
        hourTempData.push(d);
      }
    });
    // dtL = new Date(deviceTemps[deviceTemps.length - 1].timestamp);
    // console.log(Math.abs(currentDateTime.getHours() - dtL.getHours()))

    // console.log(hourTempData);

    if (hourTempData.length > 0) {
      let dataForSlice = hourTempData;
      let lastFiveData = dataForSlice.slice(
        Math.max(dataForSlice.length - 5, 0)
      );

      return res
        .status(200)
        .send([
          { latestFive: sortJsonArray(lastFiveData, "id", "asc") },
          { hourData: sortJsonArray(hourTempData, "id", "asc") },
        ]);
    } else {
      return res.status(404).send(hourTempData);
    }
  } else return res.status(404).send([]);
});

// Route to get device humidity data
router.get("/:id/humidity", auth, async (req, res) => {
  const device = await Get4CDeviceById(req.params.id);
  if (!device.data)
    return res.status(404).send("No device found with ID: " + req.params.id);

  const deviceData = device.data;

  if (deviceData.humidity && deviceData.humidity.length > 0) {
    let deviceHums = [];
    let arrayLength = deviceData.humidity.length;
    let humIds = deviceData.humidity;
    for (let i = 0; i < arrayLength; i++) {
      let dHum = await Get4CHumidityById(humIds[i].id);
      deviceHums.push(dHum[0]);
    }

    return res.send(deviceHums);
  } else {
    return res.status(404).send([]);
  }
});

// Get last hour humidity values of a device
router.get("/:id/hum-last-hour", auth, async (req, res) => {
  const device = await Get4CDeviceById(req.params.id);
  if (!device.data)
    return res.status(404).send("No device found with ID: " + req.params.id);

  if (device.data.humidity && device.data.humidity.length > 0) {
    let currentDateTime = new Date();

    let deviceHums = [];
    let hourHumData = [];
    let arrayLength = device.data.humidity.length;
    let humIds = device.data.humidity;
    for (let i = 0; i < arrayLength; i++) {
      var dHum = await Get4CHumidityById(humIds[i].id);
      deviceHums.push(dHum[0]);
    }

    deviceHums.forEach((d) => {
      let dt = new Date(d.timestamp);
      if (Math.abs(currentDateTime.getTime() - dt.getTime()) <= 3600000) {
        hourHumData.push(d);
      }
    });

    if (hourHumData.length > 0) {
      let dataForSlice = hourHumData;
      let lastFiveData = dataForSlice.slice(
        Math.max(dataForSlice.length - 5, 0)
      );

      return res
        .status(200)
        .send([
          { latestFive: sortJsonArray(lastFiveData, "id", "asc") },
          { hourData: sortJsonArray(hourHumData, "id", "asc") },
        ]);
    } else {
      return res.status(404).send(hourHumData);
    }
  } else return res.status(404).send([]);
});

// Get device first position data
router.get("/:id/firstposition", auth, async (req, res) => {
  const device = await Get4CDeviceById(req.params.id);
  if (!device.data)
    return res.status(404).send("No device found with ID: " + req.params.id);

  if (device.data.position && device.data.position.length > 0) {
    let firstPositionData = device.data.position[0];
    let firstPositionId = firstPositionData.id;
    let devicePosition = await Get4CPositionById(firstPositionId);

    return res.status(200).send(devicePosition);
  } else return res.status(404).send([]);
});

// Get device last position data
router.get("/:id/lastposition", auth, async (req, res) => {
  const device = await Get4CDeviceById(req.params.id);
  if (!device.data)
    return res.status(404).send("No device found with ID: " + req.params.id);

  if (device.data.track === 0) return res.status(404).send([]);

  if (device.data.position && device.data.position.length > 0) {
    let lastPositionData =
      device.data.position[device.data.position.length - 1];
    let lastPositionId = lastPositionData.id;
    let devicePosition = await Get4CPositionById(lastPositionId);

    return res.status(200).send(devicePosition);
  } else return res.status(404).send([]);
});

// Route to get device position data
router.get("/:id/position", auth, async (req, res) => {
  const device = await Get4CDeviceById(req.params.id);
  if (!device.data)
    return res.status(404).send("No device found with ID: " + req.params.id);

  const deviceData = device.data;

  if (deviceData.position && deviceData.position.length > 0) {
    let devicePos = [];
    let arrayLength = deviceData.position.length;
    let posIds = deviceData.position;
    for (let i = 0; i < arrayLength; i++) {
      let dPos = await Get4CPositionById(posIds[i].id);
      devicePos.push(dPos[0]);
    }

    return res.send(devicePos);
  } else {
    return res.status(404).send([]);
  }
});

// Function to validate json data/schema
// function ValidateDevice(device) {
//   const schema = Joi.object({
//     name: Joi.string().min(3).required(),
//   });

//   return (result = schema.validate(device));
// }

module.exports = router;
