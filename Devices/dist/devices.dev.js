"use strict";

// Import custom auth middleware
var auth = require("../Middleware/auth"); // Import 4C DB modules


var _require = require("./FourCDB/devices4C"),
    Get4CDevices = _require.Get4CDevices,
    Get4CDeviceById = _require.Get4CDeviceById,
    Edit4CDevice = _require.Edit4CDevice,
    Delete4CDevice = _require.Delete4CDevice; // for demo: import dummy data


var data = require("../Data/devices.json"); // joi module for validation


var Joi = require("joi");

var express = require("express");

var _require2 = require("express"),
    response = _require2.response;

var router = express.Router(); // Route to get all devices

router.get("/", auth, function _callee(req, res) {
  var devices;
  return regeneratorRuntime.async(function _callee$(_context) {
    while (1) {
      switch (_context.prev = _context.next) {
        case 0:
          _context.next = 2;
          return regeneratorRuntime.awrap(Get4CDevices());

        case 2:
          devices = _context.sent;
          res.send(devices);

        case 4:
        case "end":
          return _context.stop();
      }
    }
  });
}); // Route to get a device by id

router.get("/:id", auth, function _callee2(req, res) {
  var device;
  return regeneratorRuntime.async(function _callee2$(_context2) {
    while (1) {
      switch (_context2.prev = _context2.next) {
        case 0:
          _context2.next = 2;
          return regeneratorRuntime.awrap(Get4CDeviceById(req.params.id));

        case 2:
          device = _context2.sent;

          if (device) {
            _context2.next = 5;
            break;
          }

          return _context2.abrupt("return", res.status(404).send("No device found with ID: " + req.params.id));

        case 5:
          res.send(device);

        case 6:
        case "end":
          return _context2.stop();
      }
    }
  });
}); // NOTE: device is created by python script so no api endpoint for that

router.post("/", function (req, res) {
  res.send("Creating a device with api is not allowed!");
}); // Route to edit a device

router.patch("/", function (req, res) {
  if (!req.params.id) return res.status(404).send("Use PUT to edit a device | Please specify id in the header");
  res.send("Use PUT to edit a device");
});
router.patch("/:id", function (req, res) {
  if (!req.params.id) return res.status(404).send("Use PUT to edit a device | Please specify id in the header");
  res.send("Use PUT to edit a device");
}); // Function to edit device using api endpoint

function EditDevice(req, res) {
  return regeneratorRuntime.async(function EditDevice$(_context3) {
    while (1) {
      switch (_context3.prev = _context3.next) {
        case 0:
          editedItem = {
            active: null,
            name: null,
            networkprovider_device_id: null,
            installation_location: null,
            track: null,
            offline_messageignoretime: null,
            group: null,
            networkprovider: null,
            product: null,
            display: null
          };
          editedItem.active = req.body.active;
          editedItem.name = req.body.name;
          editedItem.networkprovider_device_id = req.body.networkprovider_device_id;
          editedItem.installation_location = req.body.installation_location;
          editedItem.firmware_version = req.body.firmware_version;
          editedItem.track = req.body.track;
          editedItem.offline_messageignoretime = req.body.offline_messageignoretime;
          editedItem.group = req.body.group;
          editedItem.networkprovider = req.body.networkprovider;
          editedItem.product = req.body.product;
          editedItem.display = req.body.display;
          _context3.next = 14;
          return regeneratorRuntime.awrap(Edit4CDevice(req.params.id, editedItem).then(function (response) {
            res.send(response);
          }));

        case 14:
        case "end":
          return _context3.stop();
      }
    }
  });
}

router.put("/:id", auth, function _callee3(req, res) {
  var device;
  return regeneratorRuntime.async(function _callee3$(_context4) {
    while (1) {
      switch (_context4.prev = _context4.next) {
        case 0:
          _context4.next = 2;
          return regeneratorRuntime.awrap(Get4CDeviceById(req.params.id));

        case 2:
          device = _context4.sent;

          if (device) {
            _context4.next = 5;
            break;
          }

          return _context4.abrupt("return", res.status(404).send("No device found with ID: " + req.params.id));

        case 5:
          _context4.prev = 5;
          _context4.next = 8;
          return regeneratorRuntime.awrap(EditDevice(req, res));

        case 8:
          _context4.next = 13;
          break;

        case 10:
          _context4.prev = 10;
          _context4.t0 = _context4["catch"](5);
          console.log("Error: " + _context4.t0);

        case 13:
        case "end":
          return _context4.stop();
      }
    }
  }, null, null, [[5, 10]]);
}); // Route to delete a device

router["delete"]("/:id", auth, function _callee4(req, res) {
  var device, deleteResponse;
  return regeneratorRuntime.async(function _callee4$(_context5) {
    while (1) {
      switch (_context5.prev = _context5.next) {
        case 0:
          _context5.next = 2;
          return regeneratorRuntime.awrap(Get4CDeviceById(req.params.id));

        case 2:
          device = _context5.sent;

          if (device) {
            _context5.next = 5;
            break;
          }

          return _context5.abrupt("return", res.status(404).send("No device found with ID: " + req.params.id));

        case 5:
          _context5.next = 7;
          return regeneratorRuntime.awrap(Delete4CDevice(req.params.id, device));

        case 7:
          deleteResponse = _context5.sent;
          res.send(deleteResponse);

        case 9:
        case "end":
          return _context5.stop();
      }
    }
  });
}); // Function to validate json data/schema

function ValidateDevice(device) {
  var schema = Joi.object({
    name: Joi.string().min(3).required()
  });
  return result = schema.validate(device);
}

module.exports = router;