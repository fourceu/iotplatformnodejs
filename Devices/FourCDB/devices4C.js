// Import config to set db used by api from env variable
const config = require("config");

// Import Axios to fetch data from FourC DB
const axios = require("axios");

// Import db link module
const { url_IOT_Stop, url_Hotdog } = require("../../config/dbTypes");

// Variable for getting devices from 4C db
let url4cDevices = "";
if (config.get("dbName") === "iotstop") {
  url4cDevices = url_IOT_Stop + "device"; // Devices path using iotstop db
} else if (config.get("dbName") === "hotdog") {
  url4cDevices = url_Hotdog + "device"; // Devices path using hotdog db
}

// Import json sort library to sort incoming data form db
var sortJsonArray = require("sort-json-array");

// Function to get all FourC devices
async function Get4CDevices() {
  return await axios
    .get(url4cDevices)
    .then((response) => {
      this.response = response.data.result;
      return this.response;
    })
    .catch((error) => {
      this.response = {
        message: "Error communicating with the database!",
        error: error,
      };
      return this.response;
    });
}

// Function to get a specific FourC device
async function Get4CDeviceById(id) {
  return await axios
    .get(`${url4cDevices}(${id})`)
    .then((response) => {
      if (response.data["@count"] === 0) {
        this.response = {
          message: "item NOT found",
          data: null,
        };
        return this.response;
      } else {
        this.response = {
          message: "item found",
          data: response.data.result[0],
        };
        return this.response;
      }
    })
    .catch((error) => {
      this.response = {
        message: "Error communicating with the database!",
        error: error,
      };
      return this.response;
    });
}

// Note Creating a device by api is not allowed
// It is handled by the python script that creates devices
// based on the ones registered in mobile scubscriber's platform

// Function to edit a device on FourC DB
async function Edit4CDevice(id, data) {
  var editItem = JSON.stringify(data);

  // console.log(`${url4cDevices}(${id})`);
  // console.log(editItem);

  return await axios
    .patch(`${url4cDevices}(${id})`, (data = editItem))
    .then((response) => {
      this.response = response.data;
      return this.response;
    })
    .catch((error) => {
      this.response = {
        message: "Error communicating with the database!",
        error: error,
      };
      return this.response;
    });
}

// Function to delete a specific FourC device
async function Delete4CDevice(id, data) {
  return await axios
    .delete(url4cDevices + `(${id})`)
    .then((response) => {
      if (response.status === 204) {
        this.response = {
          message: "Item has been deleted",
          item: data.data,
        };
        return this.response;
      }
    })
    .catch((error) => {
      this.response = {
        message: "Error communicating with the database!",
        error: error,
      };
      return this.response;
    });
}

module.exports = {
  Get4CDevices,
  Get4CDeviceById,
  Edit4CDevice,
  Delete4CDevice,
};
