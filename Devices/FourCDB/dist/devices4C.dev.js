"use strict";

// Import config to set db used by api from env variable
var config = require("config"); // Import Axios to fetch data from FourC DB


var axios = require("axios"); // Import db link module


var _require = require("../../config/dbTypes"),
    url_IOT_Stop = _require.url_IOT_Stop,
    url_Hotdog = _require.url_Hotdog; // Variable for getting devices from 4C db


var url4cDevices = "";

if (config.get("dbName") === "iotstop") {
  url4cDevices = url_IOT_Stop + "device"; // Groups path using iotstop db
} else if (config.get("dbName") === "hotdog") {
  url4cDevices = url_Hotdog + "device"; // Groups path using hotdog db
} // Import json sort library to sort incoming data form db


var sortJsonArray = require("sort-json-array");

var _require2 = require("express"),
    response = _require2.response; // Function to get all FourC devices


function Get4CDevices() {
  var _this = this;

  return regeneratorRuntime.async(function Get4CDevices$(_context) {
    while (1) {
      switch (_context.prev = _context.next) {
        case 0:
          _context.next = 2;
          return regeneratorRuntime.awrap(axios.get(url4cDevices).then(function (response) {
            _this.response = sortJsonArray(response.data.result, "id", "des");
            return _this.response;
          })["catch"](function (error) {
            _this.response = {
              message: "Error communicating with the database!",
              error: error
            };
            return _this.response;
          }));

        case 2:
          return _context.abrupt("return", _context.sent);

        case 3:
        case "end":
          return _context.stop();
      }
    }
  });
} // Function to get a specific FourC device


function Get4CDeviceById(id) {
  var _this2 = this;

  return regeneratorRuntime.async(function Get4CDeviceById$(_context2) {
    while (1) {
      switch (_context2.prev = _context2.next) {
        case 0:
          _context2.next = 2;
          return regeneratorRuntime.awrap(axios.get("".concat(url4cDevices, "(").concat(id, ")")).then(function (response) {
            if (response.data["@count"] === 0) {
              _this2.response = {
                message: "item NOT found",
                data: response.data.result
              };
              return _this2.response;
            } else {
              _this2.response = {
                message: "item found",
                data: response.data.result
              };
              return _this2.response;
            }
          })["catch"](function (error) {
            _this2.response = {
              message: "Error communicating with the database!",
              error: error
            };
            return _this2.response;
          }));

        case 2:
          return _context2.abrupt("return", _context2.sent);

        case 3:
        case "end":
          return _context2.stop();
      }
    }
  });
} // Note Creating a device by api is not allowed
// It is handled by the python script that creates devices
// based on the ones registered in mobile scubscriber's platform
// Function to edit a device on FourC DB


function Edit4CDevice(id, data) {
  var _this3 = this;

  var editItem;
  return regeneratorRuntime.async(function Edit4CDevice$(_context3) {
    while (1) {
      switch (_context3.prev = _context3.next) {
        case 0:
          editItem = JSON.stringify(data); // console.log(`${url4cDevices}(${id})`);
          // console.log(editItem);

          _context3.next = 3;
          return regeneratorRuntime.awrap(axios.patch("".concat(url4cDevices, "(").concat(id, ")"), data = editItem).then(function (response) {
            _this3.response = response.data;
            return _this3.response;
          })["catch"](function (error) {
            _this3.response = {
              message: "Error communicating with the database!",
              error: error
            };
            return _this3.response;
          }));

        case 3:
          return _context3.abrupt("return", _context3.sent);

        case 4:
        case "end":
          return _context3.stop();
      }
    }
  });
} // Function to delete a specific FourC device


function Delete4CDevice(id, data) {
  var _this4 = this;

  return regeneratorRuntime.async(function Delete4CDevice$(_context4) {
    while (1) {
      switch (_context4.prev = _context4.next) {
        case 0:
          _context4.next = 2;
          return regeneratorRuntime.awrap(axios["delete"](url4cDevices + "(".concat(id, ")")).then(function (response) {
            if (response.status === 204) {
              _this4.response = {
                message: "Item has been deleted",
                item: data.data
              };
              return _this4.response;
            }
          })["catch"](function (error) {
            _this4.response = {
              message: "Error communicating with the database!",
              error: error
            };
            return _this4.response;
          }));

        case 2:
          return _context4.abrupt("return", _context4.sent);

        case 3:
        case "end":
          return _context4.stop();
      }
    }
  });
}

module.exports = {
  Get4CDevices: Get4CDevices,
  Get4CDeviceById: Get4CDeviceById,
  Edit4CDevice: Edit4CDevice,
  Delete4CDevice: Delete4CDevice
};