"use strict";

// Import config to set db used by api from env variable
var config = require("config"); // Import Axios to fetch data from FourC DB


var axios = require("axios"); // Import db link module


var _require = require("../../config/dbTypes"),
    url_IOT_Stop = _require.url_IOT_Stop,
    url_Hotdog = _require.url_Hotdog; // Import json sort library to sort incoming data form db


var sortJsonArray = require("sort-json-array");

var _require2 = require("express"),
    response = _require2.response; // Variable for getting devices from 4C db


var url4cNetworkProviders = "";

if (config.get("dbName") === "iotstop") {
  url4cNetworkProviders = url_IOT_Stop + "networkprovider"; // Groups path using iotstop db
} else if (config.get("dbName") === "hotdog") {
  url4cNetworkProviders = url_Hotdog + "networkprovider"; // Groups path using hotdog db
}

function Get4CNetworkProviders() {
  var _this = this;

  return regeneratorRuntime.async(function Get4CNetworkProviders$(_context) {
    while (1) {
      switch (_context.prev = _context.next) {
        case 0:
          _context.next = 2;
          return regeneratorRuntime.awrap(axios.get(url4cNetworkProviders).then(function (response) {
            _this.response = sortJsonArray(response.data.result, "id", "des"); //returned array in descending order

            return _this.response;
          })["catch"](function (error) {
            _this.response = {
              message: "Error communicating with the database!",
              error: error
            };
            return _this.response;
          }));

        case 2:
          return _context.abrupt("return", _context.sent);

        case 3:
        case "end":
          return _context.stop();
      }
    }
  });
}

function Get4CNetworkProvidersById(id) {
  var _this2 = this;

  return regeneratorRuntime.async(function Get4CNetworkProvidersById$(_context2) {
    while (1) {
      switch (_context2.prev = _context2.next) {
        case 0:
          _context2.next = 2;
          return regeneratorRuntime.awrap(axios.get("".concat(url4cNetworkProviders, "(").concat(id, ")")).then(function (response) {
            if (response.data["@count"] === 0) {
              _this2.response = {
                message: "item NOT found",
                data: response.data.result
              };
              return _this2.response;
            } else {
              _this2.response = {
                message: "item found",
                data: response.data.result
              };
              return _this2.response;
            }
          })["catch"](function (error) {
            _this2.response = {
              message: "Error communicating with the database!",
              error: error
            };
            return _this2.response;
          }));

        case 2:
          return _context2.abrupt("return", _context2.sent);

        case 3:
        case "end":
          return _context2.stop();
      }
    }
  });
}

function Create4CNetworkProvider(data) {
  var _this3 = this;

  var newItem;
  return regeneratorRuntime.async(function Create4CNetworkProvider$(_context3) {
    while (1) {
      switch (_context3.prev = _context3.next) {
        case 0:
          newItem = JSON.stringify(data);
          _context3.next = 3;
          return regeneratorRuntime.awrap(axios.post(url4cNetworkProviders, data = newItem).then(function (response) {
            _this3.response = response.headers.location;
            return _this3.response;
          })["catch"](function (error) {
            _this3.response = {
              message: "Error communicating with the database!",
              error: error
            };
            return _this3.response;
          }));

        case 3:
          return _context3.abrupt("return", _context3.sent);

        case 4:
        case "end":
          return _context3.stop();
      }
    }
  });
}

function Edit4CNetworkProvider(id, data) {
  var _this4 = this;

  var editItem;
  return regeneratorRuntime.async(function Edit4CNetworkProvider$(_context4) {
    while (1) {
      switch (_context4.prev = _context4.next) {
        case 0:
          editItem = JSON.stringify(data);
          _context4.next = 3;
          return regeneratorRuntime.awrap(axios.patch("".concat(url4cNetworkProviders, "(").concat(id, ")"), data = editItem).then(function (response) {
            _this4.response = response.data;
            return _this4.response;
          })["catch"](function (error) {
            _this4.response = {
              message: "Error communicating with the database!",
              error: error
            };
            return _this4.response;
          }));

        case 3:
          return _context4.abrupt("return", _context4.sent);

        case 4:
        case "end":
          return _context4.stop();
      }
    }
  });
}

function DeleteCNetworkProvider(id, data) {
  var _this5 = this;

  return regeneratorRuntime.async(function DeleteCNetworkProvider$(_context5) {
    while (1) {
      switch (_context5.prev = _context5.next) {
        case 0:
          _context5.next = 2;
          return regeneratorRuntime.awrap(axios["delete"]("".concat(url4cNetworkProviders, "(").concat(id, ")")).then(function (response) {
            if (response.status === 204) {
              _this5.response = {
                message: "Item has been deleted",
                item: data.data
              };
              return _this5.response;
            }
          })["catch"](function (error) {
            _this5.response = {
              message: "Error communicating with the database!",
              error: error
            };
            return _this5.response;
          }));

        case 2:
          return _context5.abrupt("return", _context5.sent);

        case 3:
        case "end":
          return _context5.stop();
      }
    }
  });
}

module.exports = {
  Get4CNetworkProviders: Get4CNetworkProviders,
  Get4CNetworkProvidersById: Get4CNetworkProvidersById,
  Create4CNetworkProvider: Create4CNetworkProvider,
  Edit4CNetworkProvider: Edit4CNetworkProvider,
  DeleteCNetworkProvider: DeleteCNetworkProvider
};