// // for demo: import dummy data
// const data = require("../Data/network_providers.json");

// Import Axios to fetch data from FourC DB
const axios = require("axios");

// Import module to use 4C db api
const {
  Get4CNetworkProviders,
  Get4CNetworkProvidersById,
  Create4CNetworkProvider,
  Edit4CNetworkProvider,
  DeleteCNetworkProvider,
} = require("./FourCDB/np4C");

// loading 4C devices module
const {
  Get4CDevices,
  Get4CDeviceById,
} = require("../Devices/FourCDB/devices4C");

// Import custom auth middleware
const auth = require("../Middleware/auth");

// joi module for validation
const Joi = require("joi");

const express = require("express");
const { Get4CPositionById } = require("../Sensors/GPS/FourCDB/position4C");
const router = express.Router();

// Route to get all network providers
router.get("/", auth, async (req, res) => {
  const nps = await Get4CNetworkProviders();
  res.send(nps);
});

// Route to get a network provider by id
router.get("/:id", auth, async (req, res) => {
  const np = await Get4CNetworkProvidersById(req.params.id);
  if (!np || np.data.length < 1)
    return res
      .status(404)
      .send("No network provider found with ID: " + req.params.id);
  res.send(np);
});

// Route to get a mno device
router.get("/:id/devices", auth, async (req, res) => {
  const np = await Get4CNetworkProvidersById(req.params.id);
  if (!np || np.data.length < 1)
    return res
      .status(404)
      .send("No network provider found with ID: " + req.params.id);

  const deviceArray = np.data.device;

  if (deviceArray.length < 1) {
    res.status(404).send(`No devices available yet for this provider!`);
  } else {
    let npDevices = [];
    for (let i = 0; i < deviceArray.length; i++) {
      let device = await Get4CDeviceById(deviceArray[i].id);
      if (device && device.data) {
        npDevices.push(device.data);
      }
    }

    res.send(npDevices);
  }
});

router.get("/:id/devices/position", auth, async (req, res) => {
  const np = await Get4CNetworkProvidersById(req.params.id);
  if (!np || np.data.length < 1)
    return res
      .status(404)
      .send("No network provider found with ID: " + req.params.id);

  const deviceArray = np.data.device;

  if (deviceArray.length < 1) {
    res.status(404).send(`No devices available yet for this group!`);
  } else {
    let npDevicesPos = [];
    for (let i = 0; i < deviceArray.length; i++) {
      let device = await Get4CDeviceById(deviceArray[i].id);
      if (device && device.data && device.data.position.length > 0) {
        let npDevPos = await Get4CPositionById(device.data?.position[0].id);
        npDevicesPos.push(npDevPos);
      }
    }
    res.send(npDevicesPos);
  }
});

// Function to create a new network provider
async function CreateNetworkProvider(req, res) {
  const item = {
    name: req.body.name,
  };

  let location;

  try {
    location = await Create4CNetworkProvider(item);
    // console.log(location);
    if (location === false) {
      res.status(400).send("Network provider name already exists");
    } else {
      await axios.get(location).then((np) => {
        res
          .status(201)
          .send({ message: "Item created", data: np.data.result[0] });
      });
    }
  } catch (ex) {
    console.log("Error: " + ex);
  }
}

// Route to create a network provider
router.post("/", auth, async (req, res) => {
  const { error } = ValidateNp(req.body);
  if (error) return res.status(400).send(error.details[0].message);

  await Get4CNetworkProviders().then(async (nps) => {
    if (nps.find((np) => np.name === req.body.name)) {
      return res.status(400).send("Network Provider name already exists");
    } else {
      await CreateNetworkProvider(req, res);
    }
  });
});

// Route to edit a network provider
router.patch("/", (req, res) => {
  if (!req.params.id)
    return res
      .status(404)
      .send(
        "Use PUT to edit a network provider | Please specify id in the header"
      );
  res.send("Use PUT to edit a network provider");
});
router.patch("/:id", (req, res) => {
  if (!req.params.id)
    return res
      .status(404)
      .send(
        "Use PUT to edit a network provider | Please specify id in the header"
      );
  res.send("Use PUT to edit a network provider");
});

router.put("/:id", auth, async (req, res) => {
  const np = await Get4CNetworkProvidersById(req.params.id);
  if (!np || np.data.length < 1)
    return res
      .status(404)
      .send("No network provider found with ID: " + req.params.id);

  const { error } = ValidateNp(req.body);
  if (error) return res.status(400).send(error.details[0].message);

  let editData = {
    name: req.body.name,
  };

  await Get4CNetworkProviders().then(async (nps) => {
    if (nps.find((np) => np.name === editData.name)) {
      return res.status(400).send("Network Provider name already exists");
    } else {
      await Edit4CNetworkProvider(req.params.id, editData).then((response) => {
        res.send({ message: "Item Edited", data: response });
      });
    }
  });
});

// Route to delete a network provider
router.delete("/:id", auth, async (req, res) => {
  // Get all devices from 4C db
  const devices = await Get4CDevices();

  let associatedDevices = [];

  const np = await Get4CNetworkProvidersById(req.params.id);

  // console.log(np);

  if (!np || np.data.length < 1)
    return res
      .status(404)
      .send("No network provider found with ID: " + req.params.id);

  const netProvider = np.data;
  if (netProvider.device && netProvider.device.length > 0) {
    // pushing associated devices to the array
    netProvider.device.map((i) => {
      associatedDevices.push(devices.find((d) => d.id === i.id));
    });

    // console.log(associatedDevices);

    res.status(400).send({
      error: "Cannot delete netowrk provider. Device(s) are associated!",
      device: associatedDevices,
    });
    return;
  } else {
    // delete group
    DeleteCNetworkProvider(req.params.id, np).then((response) => {
      res.send(response);
    });
  }
});

// Function to validate json data/schema
function ValidateNp(np) {
  const schema = Joi.object({
    name: Joi.string().min(3).required(),
  });

  return (result = schema.validate(np));
}

module.exports = router;
