"use strict";

// // for demo: import dummy data
// const data = require("../Data/network_providers.json");
// Import Axios to fetch data from FourC DB
var axios = require("axios"); // Import module to use 4C db api


var _require = require("./FourCDB/np4C"),
    Get4CNetworkProviders = _require.Get4CNetworkProviders,
    Get4CNetworkProvidersById = _require.Get4CNetworkProvidersById,
    Create4CNetworkProvider = _require.Create4CNetworkProvider,
    Edit4CNetworkProvider = _require.Edit4CNetworkProvider,
    DeleteCNetworkProvider = _require.DeleteCNetworkProvider; // Import custom auth middleware


var auth = require("../Middleware/auth"); // joi module for validation


var Joi = require("joi");

var express = require("express");

var router = express.Router(); // Route to get all network providers

router.get("/", auth, function _callee(req, res) {
  var nps;
  return regeneratorRuntime.async(function _callee$(_context) {
    while (1) {
      switch (_context.prev = _context.next) {
        case 0:
          _context.next = 2;
          return regeneratorRuntime.awrap(Get4CNetworkProviders());

        case 2:
          nps = _context.sent;
          res.send(nps);

        case 4:
        case "end":
          return _context.stop();
      }
    }
  });
}); // Route to get a network provider by id

router.get("/:id", auth, function _callee2(req, res) {
  var np;
  return regeneratorRuntime.async(function _callee2$(_context2) {
    while (1) {
      switch (_context2.prev = _context2.next) {
        case 0:
          _context2.next = 2;
          return regeneratorRuntime.awrap(Get4CNetworkProvidersById(req.params.id));

        case 2:
          np = _context2.sent;

          if (!(!np || np.data.length < 1)) {
            _context2.next = 5;
            break;
          }

          return _context2.abrupt("return", res.status(404).send("No network provider found with ID: " + req.params.id));

        case 5:
          res.send(np);

        case 6:
        case "end":
          return _context2.stop();
      }
    }
  });
}); // Function to create a new network provider

function CreateNetworkProvider(req, res) {
  var item, location;
  return regeneratorRuntime.async(function CreateNetworkProvider$(_context3) {
    while (1) {
      switch (_context3.prev = _context3.next) {
        case 0:
          item = {
            name: req.body.name
          };
          location = "";
          _context3.prev = 2;
          _context3.next = 5;
          return regeneratorRuntime.awrap(Create4CNetworkProvider(item));

        case 5:
          location = _context3.sent;
          _context3.next = 11;
          break;

        case 8:
          _context3.prev = 8;
          _context3.t0 = _context3["catch"](2);
          console.log("Error: " + _context3.t0);

        case 11:
          _context3.next = 13;
          return regeneratorRuntime.awrap(axios.get(location).then(function (np) {
            res.send({
              message: "Item created",
              data: np.data.result
            });
          }));

        case 13:
        case "end":
          return _context3.stop();
      }
    }
  }, null, null, [[2, 8]]);
} // Route to create a network provider


router.post("/", auth, function _callee3(req, res) {
  var _ValidateNp, error;

  return regeneratorRuntime.async(function _callee3$(_context4) {
    while (1) {
      switch (_context4.prev = _context4.next) {
        case 0:
          _ValidateNp = ValidateNp(req.body), error = _ValidateNp.error;

          if (!error) {
            _context4.next = 3;
            break;
          }

          return _context4.abrupt("return", res.status(400).send(error.details[0].message));

        case 3:
          _context4.next = 5;
          return regeneratorRuntime.awrap(CreateNetworkProvider(req, res));

        case 5:
        case "end":
          return _context4.stop();
      }
    }
  });
}); // Route to edit a network provider

router.patch("/", function (req, res) {
  if (!req.params.id) return res.status(404).send("Use PUT to edit a network provider | Please specify id in the header");
  res.send("Use PUT to edit a network provider");
});
router.patch("/:id", function (req, res) {
  if (!req.params.id) return res.status(404).send("Use PUT to edit a network provider | Please specify id in the header");
  res.send("Use PUT to edit a network provider");
});
router.put("/:id", auth, function _callee4(req, res) {
  var np, _ValidateNp2, error, editData;

  return regeneratorRuntime.async(function _callee4$(_context5) {
    while (1) {
      switch (_context5.prev = _context5.next) {
        case 0:
          _context5.next = 2;
          return regeneratorRuntime.awrap(Get4CNetworkProvidersById(req.params.id));

        case 2:
          np = _context5.sent;

          if (!(!np || np.data.length < 1)) {
            _context5.next = 5;
            break;
          }

          return _context5.abrupt("return", res.status(404).send("No network provider found with ID: " + req.params.id));

        case 5:
          _ValidateNp2 = ValidateNp(req.body), error = _ValidateNp2.error;

          if (!error) {
            _context5.next = 8;
            break;
          }

          return _context5.abrupt("return", res.status(400).send(error.details[0].message));

        case 8:
          editData = {
            name: req.body.name
          };
          _context5.next = 11;
          return regeneratorRuntime.awrap(Edit4CNetworkProvider(req.params.id, editData).then(function (response) {
            res.send(response);
          }));

        case 11:
        case "end":
          return _context5.stop();
      }
    }
  });
}); // Route to delete a network provider

router["delete"]("/:id", auth, function _callee5(req, res) {
  var np;
  return regeneratorRuntime.async(function _callee5$(_context6) {
    while (1) {
      switch (_context6.prev = _context6.next) {
        case 0:
          _context6.next = 2;
          return regeneratorRuntime.awrap(Get4CNetworkProvidersById(req.params.id));

        case 2:
          np = _context6.sent;

          if (!(!np || np.data.length < 1)) {
            _context6.next = 5;
            break;
          }

          return _context6.abrupt("return", res.status(404).send("No network provider found with ID: " + req.params.id));

        case 5:
          _context6.next = 7;
          return regeneratorRuntime.awrap(DeleteCNetworkProvider(req.params.id, np).then(function (response) {
            res.send(response);
          }));

        case 7:
        case "end":
          return _context6.stop();
      }
    }
  });
}); // Function to validate json data/schema

function ValidateNp(np) {
  var schema = Joi.object({
    name: Joi.string().min(3).required()
  });
  return result = schema.validate(np);
}

module.exports = router;