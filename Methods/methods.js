// Import custom auth middleware
const auth = require("../Middleware/auth");
// joi module for validation
const Joi = require("joi");

const express = require("express");
const router = express.Router();

const { getMethods, getMethodById, create4CMethod } = require("./dbServices");
const {
  Get4CObjectTypes,
  Get4CObjectTypeById,
} = require("../ObjectType/dbServices");
const { default: axios } = require("axios");

// Function to validate req body
function ValidateNewMethod(item) {
  const schema = Joi.object({
    description: Joi.string().required(),
    objecttypeid: Joi.number().required(),
  });

  return (result = schema.validate(item));
}

// Get all methods
router.get("/", auth, async (req, res) => {
  let methodsArray = [];

  const methodResponse = await getMethods();
  const objectResponse = await Get4CObjectTypes();

  if (methodResponse?.error)
    return res.status(500).send("Error communicating with the database");
  if (objectResponse?.error)
    return res.status(500).send("Error communicating with the database");

  if (methodResponse.length > 0) {
    for (i = 0; i < methodResponse.length; i++) {
      const methodItem = methodResponse[i];
      const obj = objectResponse.find((o) => o.id === methodItem.objecttype.id);
      const methodObj = {
        ...methodItem,
        objecttype: {
          id: obj.id,
          name: obj.description,
        },
      };

      methodsArray.push(methodObj);
    }
    const sortedData = methodsArray.slice().sort((a, b) => b.id - a.id);
    res.send(sortedData);
  } else {
    res.status(404).send("No methods found!");
  }

});

// Get method by Id
router.get("/:id", auth, async (req, res) => {
  const method = await getMethodById(req.params.id);
  if (!method)
    return res.status(404).send(`No method found with ID: ${req.params.id}`);

  if (method?.objecttype) {
    const ot = await Get4CObjectTypeById(method?.objecttype.id);
    return res.send({
      ...method,
      objecttype: { id: ot.id, description: ot.description },
    });
  } else {
    return res.status(404).send("Method does not have an object type!");
  }
});

// Get method by object Id
router.get("/objecttype/:id", auth, async (req, res) => {
  let methodsArray = [];
  let methodsArrayById = [];

  const methodResponse = await getMethods();
  const objectResponse = await Get4CObjectTypes();

  if (!objectResponse.find((o) => parseInt(o.id) === parseInt(req.params.id)))
    return res.status(404).send(`No object found with ID: ${req.params.id}`);

  if (methodResponse.length > 0) {
    for (i = 0; i < methodResponse.length; i++) {
      const methodItem = methodResponse[i];
      const obj = objectResponse.find((o) => o.id === methodItem.objecttype.id);
      const methodObj = {
        ...methodItem,
        objecttype: {
          id: obj.id,
          name: obj.description,
        },
      };

      methodsArray.push(methodObj);
    }
  } else {
    res.status(404).send("No methods found!");
  }

  if (methodsArray.length > 0) {
    for (i = 0; i < methodsArray.length; i++) {
      const item = methodsArray[i];
      if (item.objecttype.id === parseInt(req.params.id)) {
        methodsArrayById.push(item);
      }
    }
  } else {
    res.status(404).send(`No methods found for object ID: ${req.params.id}`);
  }

  res.send(methodsArrayById);
});

// Function to create a method
async function CreateMethod(req, res) {
  const item = {
    description: req.body.description,
    objecttype: {
      id: req.body.objecttypeid,
    },
  };

  let location;
  try {
    location = await create4CMethod(item);
    if (location) {
      await axios
        .get(location)
        .then((m) => {
          res.status(201).send(m.data.result[0]);
        })
        .catch((err) => res.status(400).send(err));
    }
  } catch (error) {
    console.log("Error: " + error);
  }
}

// Route to create new method
router.post("/", auth, async (req, res) => {
  const { error } = ValidateNewMethod(req.body);
  if (error) return res.status(400).send(error.details[0].message);

  await CreateMethod(req, res);
});

module.exports = router;
