// Import config to set db used by api from env variable
const config = require("config");
// Import Axios to fetch data from FourC DB
const axios = require("axios");
// Import url for iotstop and hotdog
const { url_IOT_Stop, url_Hotdog } = require("../config/dbTypes");

let url4CMethod = "";
if (config.get("dbName") === "iotstop") {
  url4CMethod = url_IOT_Stop + "method";
} else if (config.get("dbName") === "hotdog") {
  url4CMethod = url_Hotdog + "method";
}

// Function to get all methods from 4C Iot db
async function getMethods() {
  return await axios
    .get(url4CMethod)
    .then((res) => {
      this.response = res.data.result;
      return this.response;
    })
    .catch((err) => {
      this.response = {
        message: "Error communicating with the database!",
        error: err,
      };
      return this.response;
    });
}

// Function to get method by id from 4C Iot db
async function getMethodById(id) {
  return await axios
    .get(`${url4CMethod}(id=${id})`)
    .then((res) => {
      this.response = res.data.result[0];
      return this.response;
    })
    .catch((err) => {
      this.response = {
        message: "Error communicating with the database!",
        error: err,
      };
      return this.response;
    });
}

// Function to create method in 4C db
async function create4CMethod(newItem) {
  return await axios
    .post(url4CMethod, (data = newItem))
    .then((res) => {
      this.response = res.headers.location;
      return this.response;
    })
    .catch((err) => {
      this.response = {
        message: "Error communicating with the database!",
        error: err,
      };
      return this.response;
    });
}

module.exports = {
  getMethods,
  getMethodById,
  create4CMethod,
};
