// Import custom auth middleware
const auth = require("../Middleware/auth");

// joi module for validation
const Joi = require("joi");

const express = require("express");
const {
  Get4CProducts,
  Create4CProduct,
  Edit4CProduct,
  Delete4CProduct,
} = require("./FourCDB/products4C");

// Import Axios to fetch data from FourC DB
const axios = require("axios");

const router = express.Router();

// Route to get all products
router.get("/", auth, async (req, res) => {
  const products = await Get4CProducts();

  res.send(products);
});

// Route to get a product by Id
router.get("/:id", auth, async (req, res) => {
  const products = await Get4CProducts();
  const reqId = parseInt(req.params.id); // convert id to int and store in variable for access

  let product = products.find((el) => el.id === reqId);

  if (!product)
    return res.status(404).send("No products found with ID: " + reqId);

  res.send({
    message: "Item found",
    data: product,
  });
});

// Route to create a new product
router.post("/", auth, async (req, res) => {
  const { error } = ValidateProduct(req.body);
  if (error) return res.status(400).send(error.details[0].message);

  // Verify duplicate product name
  await Get4CProducts().then(async (products) => {
    if (products.find((product) => product.name === req.body.name)) {
      return res.status(400).send("Product name already exists");
    } else {
      try {
        let item = {
          name: req.body.name,
          description: req.body.description,
        };

        let location = await Create4CProduct(item);

        await axios.get(location).then((prod) => {
          res
            .status(201)
            .send({ message: "Item created", data: prod.data.result[0] });
        });
      } catch (error) {
        console.log(error);
      }
    }
  });
});

// Route to edit a product
router.patch("/", (req, res) => {
  if (!req.params.id)
    return res
      .status(404)
      .send("Use PUT to edit a product | Please specify id in the header");
  res.send("Use PUT to edit a product");
});
router.patch("/:id", (req, res) => {
  if (!req.params.id)
    return res
      .status(404)
      .send("Use PUT to edit a product | Please specify id in the header");
  res.send("Use PUT to edit a product");
});

// Function to validate json data/schema
function ValidateProduct(product) {
  const schema = Joi.object({
    name: Joi.string().min(3).required(),
    description: Joi.string().min(10).required(),
  });

  return (result = schema.validate(product));
}

router.put("/:id", auth, async (req, res) => {
  //convert id from string to int
  const reqId = parseInt(req.params.id);
  // get the products
  const products = await Get4CProducts();
  // scan to find the item
  const product = products.find((el) => el.id === reqId);

  //verify if device exists else send error
  if (!product)
    return res.status(404).send("No products found with ID: " + reqId);

  if (products.find((el) => el.name === req.body.name))
    return res.status(400).send("Product with same name already exists");

  try {
    let editedItem = {
      name: req.body.name,
      description: req.body.description,
    };

    await Edit4CProduct(reqId, editedItem).then((result) => {
      return res.send({
        message: "Item edited",
        data: result,
      });
    });
  } catch (error) {
    return res
      .status(304)
      .send("Something went wrong with the request. Please try again");
  }
});

// Route to delete a product
// Validation: If product is binded with device / object type / displays, it can't be deleted
router.delete("/:id", auth, async (req, res) => {
  //convert id from string to int
  const reqId = parseInt(req.params.id);
  // get the products
  const products = await Get4CProducts();
  // scan to find the item
  const product = products.find((el) => el.id === reqId);

  //verify if device exists else send error
  if (!product)
    return res.status(404).send("No products found with ID: " + reqId);

  if (product.device && product.device.length > 0) {
    return res.status(400).send({
      error_message: "Can't delete product, because of device association",
      deviceIds: product.device,
    });
  } else if (product.objecttype && product.objecttype.length > 0) {
    return res.status(400).send({
      error_message: "Can't delete product, because of object_type association",
      ojecttypeIds: product.objecttype,
    });
  } else if (
    product.display_supported &&
    product.display_supported.length > 0
  ) {
    return res.status(400).send({
      error_message:
        "Can't delete product, because of display_supported association",
      display_supportedIds: product.display_supported,
    });
  } else {
    try {
      await Delete4CProduct(reqId).then((result) => {
        if (result === 204) {
          let confirmDelete = {
            mesasge: "Item has been deleted",
            data: product,
          };
          return res.status(204).send(confirmDelete);
        }
      });
    } catch (error) {
      return res
        .status(304)
        .send("Something went wrong with the request. Please try again");
    }
  }
});

module.exports = router;
