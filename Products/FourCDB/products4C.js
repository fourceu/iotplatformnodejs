// Import config to set db used by api from env variable
const config = require("config");

// Import Axios to fetch data from FourC DB
const axios = require("axios");

// Import db link module
const { url_IOT_Stop, url_Hotdog } = require("../../config/dbTypes");

// Variable for getting devices from 4C db
let url4cProducts = "";
if (config.get("dbName") === "iotstop") {
  url4cProducts = url_IOT_Stop + "product"; // Products path using iotstop db
} else if (config.get("dbName") === "hotdog") {
  url4cProducts = url_Hotdog + "product"; // Products path using hotdog db
}

// Import json sort library to sort incoming data form db
var sortJsonArray = require("sort-json-array");

// Function to get all products from FourC DB
async function Get4CProducts() {
  return await axios
    .get(url4cProducts)
    .then((response) => {
      this.response = sortJsonArray(response.data.result, "id", "des");
      return this.response;
    })
    .catch((error) => {
      this.response = {
        message: "Error communicating with the database!",
        error: error,
      };

      return this.response;
    });
}

// Function to create a new product
async function Create4CProduct(data) {
  let newItem = JSON.stringify(data);

  return await axios
    .post(url4cProducts, (data = newItem))
    .then((res) => {
      this.response = res.headers.location;
      return this.response;
    })
    .catch((err) => {
      this.response = {
        message: "Error communicating with the database!",
        error: err,
      };
      return this.response;
    });
}

// Function to edit product by Id
async function Edit4CProduct(id, data) {
  let editedItem = JSON.stringify(data);

  return await axios
    .patch(`${url4cProducts}(${id})`, (data = editedItem))
    .then((res) => {
      this.response = res.data;
      return this.response;
    })
    .catch((err) => {
      this.response = {
        error_message: "Error communicating with the database",
        error: err,
      };
      return this.response;
    });
}

// Funtion to delete a product
async function Delete4CProduct(id) {
  return await axios
    .delete(`${url4cProducts}(${id})`)
    .then((res) => {
      this.response = res.status;
      return this.response;
    })
    .catch((err) => {
      this.response = {
        error_message: "Error communicating with the database",
        error: err,
      };
    });
}

module.exports = {
  Get4CProducts,
  Create4CProduct,
  Edit4CProduct,
  Delete4CProduct,
};
