#!/bin/bash
export env="production"
export iotplatform_jwtPrivateKey="jwt12345"
export DEBUG=app:startup
export app_password=sa12345
export spanApiKey=31ce8d71efa75215ba657876946f8e5774840e27df8bdb2a5f54c4315dd62e05
export iotp_db="iotstop"
export app_ip="localhost"
export PORT=3010
export certDir="/etc/certs/cert.pem"
export keyDir="/etc/certs/privkey.pem"
nodemon app.js