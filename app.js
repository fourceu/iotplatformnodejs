require("express-async-errors");

// logger library
const winston = require("winston");

// Swagger modules for documentation
const swaggerJsDoc = require("swagger-jsdoc");
const swaggerUi = require("swagger-ui-express");

// Import Authentication Modules
const Auth = require("./Authentication/Auth");
const Users = require("./Authentication/User");
const { Login, Authenticating } = require("./Authentication/Logger");

// Import Span Module
const SpanRoutes = require("./Span/span");

// Swagger documentation area
/**
 * @swagger
 * /api/groups:
 *  get:
 *    description: This endpoint is used to get all groups from FourC IOT DB.
 *    note: User must have authentication api key.
 *    responses:
 *      '200':
 *        description: A successful response
 *
 */
// Swagger documentation area

// Import Error middleware module
const error = require("./Middleware/error");

// Import Group Module
const GroupRoutes = require("./Groups/groupRoutes");

// Import Devices Module
const DeviceRoutes = require("./Devices/devices");

// Import Network provider Module
const NetworkProviders = require("./NetworkProviders/networkprovider");

// Import Products Module
const ProductsRoutes = require("./Products/products");

// Import Position/GPS Location Module
const Position = require("./Sensors/GPS/position");

// Import Humidity Module
const Humidity = require("./Sensors/Humidity/humidity");

// Import Temperature Module
const Temperature = require("./Sensors/Temperature/temperature");

// Import ObjectType Module
const ObjectType = require("./ObjectType/objectType");

// Import Method Module
const Methods = require("./Methods/methods");

// Import Message Module
const Message = require("./Message/message");

// Import Commands Module
const Commands = require("./Commands/commands");

// Import config module
const config = require("config");

// Import debug module
const debug = require("debug")("app:startup");

// Express setup
const express = require("express");
const app = express();

// Import Path module
const path = require("path");

// Import helmet
const helmet = require("helmet");

// Import morgan
const morgan = require("morgan");

// Handle uncaught exceptions
winston.handleExceptions(
  new winston.transports.File({ filename: "uncaughtExceptions.log" })
);

// Handle unhandled rejections
process.on("unhandledRejection", (ex) => {
  // console.log("GOT AN UNHANDLED REJECTION");
  throw ex;
});

// Writing log to file using winstonLogger
winston.add(new winston.transports.File({ filename: "logfile.log" }));

// Setting configuration
// Create following env variable before using this config
// app_password = your password
// NODE_ENV=development || production || default

console.log("Application Name: " + config.get("name"));
console.log("Mail Server: " + config.get("mail.host"));
// console.log("Mail Server Password: " + config.get("mail.password"));
console.log("DB in use: " + config.get("dbName"));

// Setting up HTTPS service

const fs = require("fs");
const https = require("https");

// Look for certificate location in config
if (!config.get("certDir")) {
  console.error("FATAL ERROR: location for certificate is not defined!");
  process.exit(1);
}
// Look for key location in config
if (!config.get("keyDir")) {
  console.error("FATAL ERROR: location for key file is not defined!");
  process.exit(1);
}

const httpsOptions = {
  cert: fs.readFileSync(path.join(config.get("certDir"))),
  key: fs.readFileSync(path.join(config.get("keyDir"))),
};

// const httpsOptions = {
//   cert: fs.readFileSync(path.join("/etc/certs/cert.pem")),
//   key: fs.readFileSync(path.join("/etc/certs/privkey.pem")),
// };

const server = https.createServer(httpsOptions, app);

//////////////////////////////////////

// Import CORS middleware to fix cross origin issue
const cors = require("cors");
app.use(cors());

// Express middleware
app.use(express.json()); // read json body
app.use(express.static("public")); // serve static folders for html
app.use(
  express.urlencoded({
    extended: true,
  }) // used for reading form data from html page
);

// Using helmet for securing the node app
app.use(helmet());

// Look for jwt app key in config
if (!config.get("jwtPrivateKey")) {
  console.error("FATAL ERROR: jwt private key (jwtPrivateKey) is not defined!");
  process.exit(1);
}

// Using morgan for HTTP request logger on dev env
if (app.get("env") === "development") {
  app.use(morgan("tiny"));
  debug("Morgan Enabled...");
}

// Calling custom middleware function for login
app.use(Login);

// Calling custom middleware function for authentication
app.use(Authenticating);

// Using Templating/View engine
app.set("view engine", "pug");
app.set("views", "./views");

// Api root route
app.get("/", (req, res) => {
  res.sendFile("index.html", { root: path.join(__dirname, "./views") });
});

// // Api pug view route
// app.get("/view", (req, res) => {
//   res.render("index.pug", {
//     title: "IOT Platform App",
//     message: "Welcome to FourC IOT Platform",
//   });
// });

// Swagger options
const swaggerOptions = {
  swaggerDefinition: {
    openapi: "3.0.0",
    info: {
      title: "IOT Platform API",
      description:
        "FourC IOT platform API provides users to manage the IoT devices provided by FourC.",
      contact: {
        name: "FourC AS",
      },
      servers: ["http://localhost:3001"],
    },
  },
  components: {
    securitySchemes: {
      bearerAuth: {
        type: "http",
        scheme: "bearer",
        bearerFormat: "JWT",
      },
    },
    securityDefinitions: {
      api_key: {
        type: "apiKey",
        name: "x-auth-token",
        in: "header",
      },
    },

    security: [
      {
        bearerAuth: [],
      },
    ],
  },

  apis: ["app.js", "./Groups/*.js"],
};

const swaggerDocs = swaggerJsDoc(swaggerOptions);
app.use("/api/api-docs", swaggerUi.serve, swaggerUi.setup(swaggerDocs));

// Routes
// Span route function
app.use("/api/span", SpanRoutes);

// Users route function
app.use("/api/users", Users);

// Authentication route function
app.use("/api/auth", Auth);

// Group routes function
app.use("/api/groups", GroupRoutes);

// Using Devices route module
app.use("/api/devices", DeviceRoutes);

// Using netowrk providers route module
app.use("/api/networkproviders", NetworkProviders);

// Using products route module
app.use("/api/products", ProductsRoutes);

// Using positions route module
app.use("/api/positions", Position);

// Using humidity values route module
app.use("/api/humidities", Humidity);

// Using temperature values route module
app.use("/api/temperatures", Temperature);

// Using objecttype route module
app.use("/api/objecttype", ObjectType);

// Using method route module
app.use("/api/methods", Methods);

// Using message route module
app.use("/api/message", Message);

// Using commands route module
app.use("/api/command", Commands);

// Using express error middlewate
// Always after all the existing middleware function
app.use(error);

// Listner
// PORT from env var
const port = process.env.PORT || 3001;
const ip = process.env.app_ip || "localhost";

app.listen(port, ip, () => console.log(`listen on ${ip}:${port}...`));
// server.listen(port, ip, () => console.log(`listen on ${ip}:${port}...`));
