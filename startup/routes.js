module.exports = function (app) {
  // Routes
  // Span route function
  app.use("/api/span", SpanRoutes);

  // Users route function
  app.use("/api/users", Users);

  // Authentication route function
  app.use("/api/auth", Auth);

  // Group routes function
  app.use("/api/groups", GroupRoutes);

  // Using Devices route module
  app.use("/api/devices", DeviceRoutes);

  // Using netowrk providers route module
  app.use("/api/networkproviders", NetworkProviders);

  // Using products route module
  app.use("/api/products", ProductsRoutes);

  // Using positions route module
  app.use("/api/positions", Position);

  // Using humidity values route module
  app.use("/api/humidities", Humidity);

  // Using temperature values route module
  app.use("/api/temperatures", Temperature);

  // Using objecttype route module
  app.use("/api/objecttype", ObjectType);

  // Using method route module
  app.use("/api/methods", Methods);

  // Using message route module
  app.use("/api/message", Message);

  // Using commands route module
  app.use("/api/command", Commands);

  // Using express error middlewate
  // Always after all the existing middleware function
  app.use(error);
};
