const auth = require("../Middleware/auth");
const axios = require("axios");
const config = require("config");
const express = require("express");
const router = express.Router();
const Joi = require("joi");
const {
  sendPayloadToDevice,
  CreateSpanDevice,
  DeleteSpanDevice,
  syncMnoWithDb,
} = require("./Devices/spanDevices");
const { Get4CDeviceById } = require("../Devices/FourCDB/devices4C");

const fs = require("fs");
const path = require("path");

var whResponse;

// function to validate json bodies
function validatePayload(data) {
  const schema = Joi.object({
    payload: Joi.string().required(),
  });

  return (result = schema.validate(data));
}

function validataNewDevice(data) {
  const schema = Joi.object({
    name: Joi.string().required(),
    imei: Joi.number().integer().min(15).required(),
    imsi: Joi.number().integer().min(15).required(),
  });

  return (result = schema.validate(data));
}

function validateSetDisplay(data) {
  const schema = Joi.object({
    upperleft: Joi.array().items(
      Joi.number().allow(null).label("Upper left x coordinate (int)"),
      Joi.number().allow(null).label("Upper left y coordinate (int)")
    ),
    lowerright: Joi.array().items(
      Joi.number().allow(null).label("Lower left x coordinate (int)"),
      Joi.number().allow(null).label("Lower left y coordinate (int)")
    ),
    bitmap: Joi.array().items(),
  });

  return (result = schema.validate(data));
}
// function to validate json bodies

router.get("/syncmno", auth, async (req, res) => {
  const response = await syncMnoWithDb();

  if (!response || response.length < 1) return res.status(204).send([]);

  res.status(201).send(response);
});

router.post("/addDevice", auth, async (req, res) => {
  const { error } = validataNewDevice(req.body);
  if (error) return res.status(400).send(error.details[0].message);

  const response = await CreateSpanDevice(req.body);

  if (response.message) return res.status(400).send(response.message);

  res.send(response);
});

router.delete("/deleteDevice/:id", auth, async (req, res) => {
  const device = await Get4CDeviceById(req.params.id);
  if (!device.data)
    return res.status(404).send(`No device found with ID: ${req.params.id}`);
  if (!device.data.networkprovider_device_id)
    return res
      .status(404)
      .send(
        `No device id found for network provider for device ID: ${req.params.id}`
      );

  const response = await DeleteSpanDevice(req.params.id, device.data);

  // console.log(response);

  res.status(204).send(response);
});

router.post("/sendpayloadto/:id", auth, async (req, res) => {
  const { error } = validatePayload(req.body);
  if (error) return res.status(400).send(error.details[0].message);

  const response = await sendPayloadToDevice(req.params.id, req.body.payload);
  console.log(response);

  // res.send(response);
  res.status(200).end();
});

router.post("/webhook/output", async (req, res) => {
  var body = req.body;
  // console.log(body);

  var ogPayload = body.messages[0];
  var deviceInfo = {
    collection_id: ogPayload.device.collectionId,
    device_id: ogPayload.device.deviceId,
    device_name: ogPayload.device.tags.name,
  };
  var initPayload = ogPayload.payload;
  var buff = Buffer.from(initPayload, "base64");
  var decodedPayload = buff.toString("utf-8");
  var jsonString = decodedPayload.replace(/(\n\t|\n|\t)/g, "");
  var object = JSON.parse(jsonString);
  var objData = object?.data;
  var data;
  if (objData) {
    var dataString = objData.replace(/(\n\t|\n|\t)/g, "");
    data = JSON.parse(dataString);
    object.data = data;
  }

  var obj = {
    // init_payload: ogPayload,
    device_info: deviceInfo,
    payload: object,
  };

  // console.log(JSON.stringify(obj, null, 2));

  // fs.writeFile(
  //   "span_wh_out.json",
  //   JSON.stringify(obj, null, 2) + "\n",
  //   {
  //     flag: "a",
  //     encoding: "utf8",
  //   },
  //   (err) => {
  //     console.log("File writing error: ", err);
  //   }
  // );

  const filename = "whOut.json";
  whResponse = obj;

  // fs.appendFile(filename, JSON.stringify(obj), "utf-8", function (err) {
  //   if (err) throw err;
  //   console.log("Written to file: " + filename);
  // });

  // res.send(body);
  res.status(200).end();
});

router.get("/spanWhRes", (req, res) => {
  setTimeout(function () {
    if (whResponse) {
      res.status(200).send(whResponse);
    } else {
      res.status(404).send("Payload error!");
    }
  }, 7000);
});

router.post("/set-display-content/:id", auth, (req, res) => {
  const { error } = validateSetDisplay(req.body);
  if (error) return res.status(400).send(error.details[0].message);

  const deviceId = req.params.id;

  // find the device based on id
  // find the network provider device id from the device
  
});

module.exports = router;
