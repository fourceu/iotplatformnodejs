// Import config to set db used by api from env variable
const config = require("config");

// Import Axios to fetch data from FourC DB
const axios = require("axios");
const {
  iotstop_db,
  span_url,
  hotdog_db,
  iotstop_collection_id,
  hotdog_collection_id,
  span_headers,
  url_IOT_Stop,
  url_Hotdog,
} = require("../../config/dbTypes");
const {
  Get4CDevices,
  Delete4CDevice,
  Edit4CDevice,
} = require("../../Devices/FourCDB/devices4C");
const e = require("express");

// Variable for getting devices from 4C db
let url4cDevices = "";
let dbName = "dbName";
if (config.get(dbName) === iotstop_db) {
  url4cDevices = url_IOT_Stop + "device"; // Devices path using iotstop db
} else if (config.get(dbName) === hotdog_db) {
  url4cDevices = url_Hotdog + "device"; // Devices path using hotdog db
}

let urlSpanDevices = "";
let collection_id = "";
if (config.get(dbName) === iotstop_db) {
  urlSpanDevices = span_url + `/collections/${iotstop_collection_id}/devices`;
  collection_id = iotstop_collection_id;
} else if (config.get(dbName) === hotdog_db) {
  urlSpanDevices = span_url + `/collections/${hotdog_collection_id}/devices`;
  collection_id = hotdog_collection_id;
}

/*
Function to add device to span platform
Add the device to the db automatically after it's added to span
*/
async function CreateSpanDevice(data) {
  const newDevice = {
    imei: data.imei.toString(10),
    imsi: data.imsi.toString(10),
    tags: {
      name: data.name,
    },
  };

  try {
    const span_response = await axios.post(urlSpanDevices, (date = newDevice), {
      headers: span_headers,
    });

    if (span_response.status === 200) {
      // console.log(span_response.data);

      const new4CDevice = {
        name: span_response.data.tags.name,
        networkprovider_device_id: span_response.data.deviceId,
        product: {
          id: 1,
        },
      };

      axios
        .post(url4cDevices, (data = new4CDevice))
        .then((resp) => console.log(resp.data))
        .catch((err) => console.log("Error communicating with db: " + err));

      return span_response.data;
    }
  } catch (error) {
    return await {
      errorMessage: "Couldn't create device on span!",
      message: "A device with the given imsi or imei already exists.",
      error: error,
    };
  }
}

/* function actions:
 * Get devices from fourc db
 * Get devices from span
 * Compare two array of devices
 * If new item added on span, add it to fourc db
 * If device name changed on span, change it on iot db
 * If no new or edited device on span, do no action and return message
 */
async function syncMnoWithDb() {
  const spanResponse = await axios.get(urlSpanDevices, {
    headers: span_headers,
  });
  const spanDevices = spanResponse.data.devices;

  const fourcDevices = await Get4CDevices();

  let newDevices = [];
  let editedDevices = [];

  const newDevicesArray = spanDevices.filter(
    (sd) =>
      !fourcDevices.some((fd) => fd.networkprovider_device_id === sd.deviceId)
  );

  const devicesRenamedArray = spanDevices.filter((sd) =>
    fourcDevices.some(
      (fd) =>
        fd.networkprovider_device_id === sd.deviceId && fd.name !== sd.tags.name
    )
  );

  const devicesToRenamedArray = fourcDevices.filter((fd) =>
    spanDevices.some(
      (sd) =>
        fd.networkprovider_device_id === sd.deviceId && fd.name !== sd.tags.name
    )
  );

  if (newDevicesArray.length > 0) {
    newDevicesArray.forEach((nd) => {
      if (nd.network.allocatedIp !== "") {
        newDevices.push({
          name: nd.tags.name,
          active: 1,
          networkprovider_device_id: nd.deviceId,
          product: { id: 1 },
        });
      } else {
        newDevices.push({
          name: nd.tags.name,
          networkprovider_device_id: nd.deviceId,
          product: { id: 1 },
        });
      }
    });
  }

  if (devicesRenamedArray.length > 0) {
    devicesRenamedArray.forEach((ed) => {
      let device = devicesToRenamedArray.find(
        (rd) => rd.networkprovider_device_id === ed.deviceId
      );
      if (ed.network.allocatedIp !== "") {
        editedDevices.push({ id: device.id, name: ed.tags.name, active: 1 });
      } else {
        editedDevices.push({ id: device.id, name: ed.tags.name });
      }
    });
  }

  if (newDevices.length > 0) {
    newDevices.forEach(async (nd) => {
      // console.log("sending new data to db || data: " + nd)
      console.log(nd);
      await axios
        .post(url4cDevices, (data = nd))
        .then((resp) => console.log(resp.data))
        .catch((error) => console.log("Error communicating with db: " + error));
    });
  }

  if (editedDevices.length > 0) {
    editedDevices.forEach(async (ed) => {
      console.log("sending edited data to db || data: " + ed);
      await axios
        .patch(`${url4cDevices}(${ed.id})`, (data = ed))
        .then((resp) => {
          console.log(resp.data);
        })
        .catch((error) => console.log("Error communicating with db: " + error));
    });
  }

  const response = {
    new_devices: newDevices,
    edited_devices: editedDevices,
  };

  return response;
}
//--------------------------------------------------

/*
 * Function actions:
 * catch the device id
 * create json structure for payload as per span json body requirement
 * send payload over span with the device id
 * TODO: call span to check message sent to device
 * TODO: match the message sent on the request with the message from recall response
 * TODO: if not matched, send error to user
 * json response message when message is successfully sent
 */
async function sendPayloadToDevice(id, data) {
  let payloadBody = {
    collectionId: collection_id,
    deviceId: id,
    port: 5683,
    payload: Buffer.from(data).toString("base64"),
    transport: "coap-pull",
  };

  try {
    const span_resp = await axios.post(
      `${urlSpanDevices}/${id}/to`,
      (data = payloadBody),
      {
        headers: span_headers,
      }
    );

    if (span_resp.status === 200) {
      return await payloadBody;
    }
  } catch (error) {
    return await { errorMessage: "Something went wrong!", error: error };
  }

  // console.log(span_resp);
  if (span_resp.status === 200) {
  }
}

/*
Function to delete a device from span collection
Delete device from fourc db first
then delete device from span
*/
async function DeleteSpanDevice(deviceId, data) {
  const dbResponse = await Delete4CDevice(deviceId, data);
  const spanResponse = await axios.delete(
    `${urlSpanDevices}/${data.networkprovider_device_id}`,
    {
      headers: span_headers,
    }
  );

  data = [
    { "4cData": data, "4cStatus": dbResponse.status },
    { spanData: spanResponse.data, spanStatus: spanResponse.status },
  ];

  return data;
}

module.exports = {
  syncMnoWithDb,
  sendPayloadToDevice,
  CreateSpanDevice,
  DeleteSpanDevice,
  collection_id,
};
