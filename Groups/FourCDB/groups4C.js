// Import config to set db used by api from env variable
const config = require("config");

// Import Axios to fetch data from FourC DB
const axios = require("axios");

// Import json sort library to sort incoming data form db
var sortJsonArray = require("sort-json-array");

// Import db link module
const { url_IOT_Stop, url_Hotdog } = require("../../config/dbTypes");

// Variable for getting devices from 4C db
let url4cGroups = "";
if (config.get("dbName") === "iotstop") {
  url4cGroups = url_IOT_Stop + "group"; // Groups path using iotstop db
} else if (config.get("dbName") === "hotdog") {
  url4cGroups = url_Hotdog + "group"; // Groups path using hotdog db
}

async function Get4cGroups() {
  return await axios
    .get(url4cGroups)
    .then((response) => {
      this.response = sortJsonArray(response.data.result, "id", "des"); //returned array in descending order
      return this.response;
    })
    .catch((error) => {
      this.response = {
        message: "Error communicating with the database!",
        error: error,
      };
      return this.response;
    });
}

async function Get4cGroupsById(id) {
  return await axios
    .get(url4cGroups + "(" + id + ")")
    .then((response) => {
      if (response.data["@count"] === 0) {
        this.response = {
          message: "item NOT found",
          data: response.data.result,
        };
        return this.response;
      } else {
        this.response = {
          message: "item found",
          data: response.data.result[0],
        };
        return this.response;
      }
    })
    .catch((error) => {
      this.response = {
        message: "Error communicating with the database!",
        error: error,
      };
      return this.response;
    });
}

async function Create4cGroup(data) {
  var newItem = JSON.stringify(data);

  return await axios
    .post(url4cGroups, (data = newItem))
    .then((response) => {
      this.response = response.headers.location;
      return this.response;
    })
    .catch((error) => {
      this.response = {
        message: "Error communicating with the database!",
        error: error,
      };
      return this.response;
    });
}

async function Edit4cGroup(id, data) {
  var editItem = JSON.stringify(data);

  return await axios
    .patch(url4cGroups + "(" + id + ")", (data = editItem))
    .then((response) => {
      this.response = response.data;
      return this.response;
    })
    .catch((error) => {
      this.response = {
        message: "Error communicating with the database!",
        error: error,
      };
      return this.response;
    });
}

async function Delete4cGroup(id, data) {
  return await axios
    .delete(url4cGroups + "(" + id + ")")
    .then((response) => {
      if (response.status === 204) {
        this.response = {
          message: "Item has been deleted",
          item: data.data,
        };
        return this.response;
      }
    })
    .catch((error) => {
      this.response = {
        message: "Error communicating with the database!",
        error: error,
      };
      return this.response;
    });
}

module.exports = {
  Get4cGroups,
  Get4cGroupsById,
  Create4cGroup,
  Edit4cGroup,
  Delete4cGroup,
};
