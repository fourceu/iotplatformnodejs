// loading 4C groups module with data
const {
  Get4cGroups,
  Get4cGroupsById,
  Create4cGroup,
  Edit4cGroup,
  Delete4cGroup,
} = require("./FourCDB/groups4C");

// loading 4C devices module
const {
  Get4CDevices,
  Get4CDeviceById,
} = require("../Devices/FourCDB/devices4C");

// importng axios to get new created group
var axios = require("axios");

// joi module for validation
const Joi = require("joi");
const { Get4CPositionById } = require("../Sensors/GPS/FourCDB/position4C");

// validation function using Joi
function validateGroup(group) {
  // joi schema
  const schema = Joi.object({
    name: Joi.string().min(3).required(),
    default_offline_messageignoretime: Joi.number(),
  });

  return (result = schema.validate(group));
}

module.exports = {
  // Get all groups function
  GetGroups: function GetGroups(req, res) {
    Get4cGroups().then((data) => {
      res.send(data);
    });
  },

  // Get group by requested ID function
  GetGroupsById: function GetGroupsById(req, res) {
    Get4cGroupsById(req.params.id).then((data) => {
      if (data.data.length < 1) {
        //   404 error
        res.status(404).send(`No resources found with id: ${req.params.id}`);
        return;
      }
      return res.send(data);
    });
  },

  // Get group device details function
  GetGroupDevices: async function GetGroupDevices(req, res) {
    const group = await Get4cGroupsById(req.params.id);
    if (group.data.length < 1)
      return res.status(404).send(`Not group found with id: ${req.params.id}`);

    const deviceArray = group.data.device;

    if (deviceArray.length < 1) {
      res.status(404).send(`No devices available yet for this group!`);
    } else {
      let groupDevices = [];
      for (let i = 0; i < deviceArray.length; i++) {
        let device = await Get4CDeviceById(deviceArray[i].id);
        if (device && device.data) {
          groupDevices.push(device.data);
        }
      }
      res.send(groupDevices);
    }
  },

  //Get group devices initial position
  GetGroupDevicesPosition: async function GetGroupDevicesPosition(req, res) {
    const group = await Get4cGroupsById(req.params.id);
    if (group.data.length < 1)
      return res.status(404).send(`Not group found with id: ${req.params.id}`);

    const deviceArray = group.data.device;

    if (deviceArray.length < 1) {
      res.status(404).send(`No devices available yet for this group!`);
    } else {
      let groupDevicesPos = [];
      for (let i = 0; i < deviceArray.length; i++) {
        let device = await Get4CDeviceById(deviceArray[i].id);
        if (device && device.data && device.data.position.length > 0) {
          let grDevPos = await Get4CPositionById(device.data?.position[0].id);
          groupDevicesPos.push(grDevPos);
        }
      }
      res.send(groupDevicesPos);
    }
  },

  // Create a new group item function
  CreateGroup: function CreateGroup(req, res) {
    // validate
    const { error } = validateGroup(req.body);

    if (error) {
      // 400 Bad Request
      res.status(400).send(error.details[0].message);
      return;
    }

    const newGroup = {
      name: req.body.name,
      default_offline_messageignoretime:
        req.body.default_offline_messageignoretime,
    };

    // get all groups and verify name doesn't exist
    Get4cGroups().then((data) => {
      if (data.find((group) => group.name === newGroup.name)) {
        res.status(400).send("Group Name already exists");
        return;
      } else {
        // push new item to db and send response to user
        Create4cGroup(newGroup).then((data) => {
          axios.get(data).then((response) => {
            this.response = {
              message:
                "New item created. Name: " + response.data.result[0].name,
              item: response.data.result[0],
            };
            res.status(201).send(this.response);
          });
        });
      }
    });
  },

  // Edit a group item function
  EditGroup: function EditGroup(req, res) {
    // lookup the group
    Get4cGroupsById(req.params.id).then((data) => {
      // error 404 is not found
      if (data.data.length < 1) {
        res.status(404).send(`No resources found with id: ${req.params.id}`);
        return;
      }

      // // validate
      // const { error } = validateGroup(req.body);

      // // if invalid return 400 - Bad request
      // if (error) {
      //   res.status(400).send(error.details[0].message);
      //   return;
      // }

      // update group
      const editGroup = {
        name: req.body.name,
        default_offline_messageignoretime:
          req.body.default_offline_messageignoretime,
      };

      // get all groups and verify name doesn't exist
      Get4cGroups().then((data) => {
        if (data.find((group) => group.name === editGroup.name)) {
          res.status(400).send("Group Name already exists");
          return;
        } else {
          // push edited item to db and send response to user
          Edit4cGroup(req.params.id, editGroup).then((data) => {
            res.send(data);
          });
        }
      });
    });
  },

  // Delete a group item by id
  DeleteGroup: async function DeleteGroup(req, res) {
    // Get all devices from 4C db
    const devices = await Get4CDevices();

    let associatedDevices = [];

    // lookup the group
    Get4cGroupsById(req.params.id).then((data) => {
      // error 404 is not found
      if (data.data.length < 1) {
        res.status(404).send(`No resources found with id: ${req.params.id}`);
        return;
      }
      const group = data.data;

      // verify that group doesnt have associated devices
      if (group.device && group.device.length > 0) {
        // pushing associated devices to the array
        group.device.map((i) => {
          associatedDevices.push(devices.find((d) => d.id === i.id));
        });

        // console.log(associatedDevices);

        res.status(400).send({
          error: "Cannot delete group. Device(s) are associated!",
          device: associatedDevices,
        });
        return;
      } else {
        // delete group
        Delete4cGroup(req.params.id, data).then((resp) => {
          res.status(204).send(resp);
        });
      }
    });
  },
};
