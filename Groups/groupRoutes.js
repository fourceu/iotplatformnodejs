// Swagger documentation area
/**
 * @swagger
 * /api/groups:
 *  get:
 *    description: This endpoint is used to get all groups from FourC IOT DB.
 *    note: User must have authentication api key.
 *    responses:
 *      '200':
 *        description: A successful response
 *
 */
// Swagger documentation area

// Import custom auth middleware
const auth = require("../Middleware/auth");

// Import Group Module
const {
  GetGroups,
  GetGroupsById,
  CreateGroup,
  EditGroup,
  DeleteGroup,
  GetGroupDevices,
  GetGroupDevicesPosition,
} = require("./groups");

const express = require("express");
const router = express.Router();

// Define urls for groups
const urlGroups = "/api/groups";
const urlGroupById = "/api/groups/:id";

// Route to get all groups
router.get("/", auth, (req, res) => {
  GetGroups(req, res);
});

// Route to get a group by id
router.get("/:id", auth, (req, res) => {
  GetGroupsById(req, res);
});

// Route to get a group devices
router.get("/:id/devices", auth, (req, res) => {
  GetGroupDevices(req, res);
});

// Route to get group device initial postiions
router.get("/:id/devices/position", auth, (req, res) => {
  GetGroupDevicesPosition(req, res);
});

// Route to create a group
router.post("/", auth, (req, res) => {
  CreateGroup(req, res);
});

// Route to edit a device
router.patch("/", auth, (req, res) => {
  if (!req.params.id)
    return res
      .status(404)
      .send("Use PUT to edit a group | Please specify id in the header");
  res.send("Use PUT to edit a group");
});
router.patch("/:id", auth, (req, res) => {
  if (!req.params.id)
    return res
      .status(404)
      .send("Use PUT to edit a group | Please specify id in the header");
  res.send("Use PUT to edit a group");
});

router.put("/:id", auth, (req, res) => {
  EditGroup(req, res);
});

// Route to delete a device
router.delete("/:id", auth, (req, res) => {
  DeleteGroup(req, res);
});

module.exports = router;

// function GroupRoutes(app) {
//   // Api route for groups
//   app.get(urlGroups, (req, res) => {
//     GetGroups(req, res);
//   });

//   // Api route to get group by id
//   app.get(urlGroupById, (req, res) => {
//     GetGroupsById(req, res);
//   });

//   // Api route to create group
//   app.post(urlGroups, (req, res) => {
//     CreateGroup(req, res);
//   });

//   // Api route to update/edit a group
//   app.put(urlGroupById, (req, res) => {
//     EditGroup(req, res);
//   });

//   // Api route to delete a group
//   app.delete(urlGroupById, (req, res) => {
//     DeleteGroup(req, res);
//   });
// }

// module.exports = GroupRoutes;
