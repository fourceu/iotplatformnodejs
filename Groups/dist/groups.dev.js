"use strict";

// loading 4C groups module with data
var _require = require("./FourCDB/groups4C"),
    Get4cGroups = _require.Get4cGroups,
    Get4cGroupsById = _require.Get4cGroupsById,
    Create4cGroup = _require.Create4cGroup,
    Edit4cGroup = _require.Edit4cGroup,
    Delete4cGroup = _require.Delete4cGroup; // importng axios to get new created group


var axios = require("axios"); // joi module for validation


var Joi = require("joi"); // validation function using Joi


function validateGroup(group) {
  // joi schema
  var schema = Joi.object({
    name: Joi.string().min(3).required()
  });
  return result = schema.validate(group);
}

module.exports = {
  // Get all groups function
  GetGroups: function GetGroups(req, res) {
    Get4cGroups().then(function (data) {
      res.send(data);
    });
  },
  // Get group by requested ID function
  GetGroupsById: function GetGroupsById(req, res) {
    Get4cGroupsById(req.params.id).then(function (data) {
      if (data.data.length < 1) {
        //   404 error
        res.status(404).send("No resources found with id: ".concat(req.params.id));
        return;
      }

      return res.send(data);
    });
  },
  // Create a new group item function
  CreateGroup: function CreateGroup(req, res) {
    var _this = this;

    // validate
    var _validateGroup = validateGroup(req.body),
        error = _validateGroup.error;

    if (error) {
      // 400 Bad Request
      res.status(400).send(error.details[0].message);
      return;
    }

    var newGroup = {
      name: req.body.name
    }; // get all groups and verify name doesn't exist

    Get4cGroups().then(function (data) {
      if (data.find(function (group) {
        return group.name === newGroup.name;
      })) {
        res.send("Group Name already exists");
        return;
      } else {
        // push new item to db and send response to user
        Create4cGroup(newGroup).then(function (data) {
          axios.get(data).then(function (response) {
            _this.response = {
              message: "New item created",
              item: response.data.result
            };
            res.send(_this.response);
          });
        });
      }
    });
  },
  // Edit a group item function
  EditGroup: function EditGroup(req, res) {
    // lookup the group
    Get4cGroupsById(req.params.id).then(function (data) {
      // error 404 is not found
      if (data.data.length < 1) {
        res.status(404).send("No resources found with id: ".concat(req.params.id));
        return;
      } // validate


      var _validateGroup2 = validateGroup(req.body),
          error = _validateGroup2.error; // if invalid return 400 - Bad request


      if (error) {
        res.status(400).send(error.details[0].message);
        return;
      } // update group


      var editGroup = {
        name: req.body.name
      }; // get all groups and verify name doesn't exist

      Get4cGroups().then(function (data) {
        if (data.find(function (group) {
          return group.name === editGroup.name;
        })) {
          res.send("Group Name already exists");
          return;
        } else {
          // push edited item to db and send response to user
          Edit4cGroup(req.params.id, editGroup).then(function (data) {
            res.send(data);
          });
        }
      });
    });
  },
  // Delete a group item by id
  DeleteGroup: function DeleteGroup(req, res) {
    // lookup the group
    Get4cGroupsById(req.params.id).then(function (data) {
      // error 404 is not found
      if (data.data.length < 1) {
        res.status(404).send("No resources found with id: ".concat(req.params.id));
        return;
      } // delete group


      Delete4cGroup(req.params.id, data).then(function (resp) {
        res.send(resp);
      });
    });
  }
};