FROM node:latest

RUN mkdir -p /iotplatformiotstop

WORKDIR /iotplatformiotstop

COPY package.json /iotplatformiotstop/package.json

RUN npm install
# RUN npm install -g npm@7.5.3

COPY . /iotplatformiotstop



EXPOSE 3007

ENTRYPOINT [ "npm", "start" ]
