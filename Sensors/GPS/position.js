// Import custom auth middleware
const auth = require("../../Middleware/auth");

// for demo: import dummy data
// const data = require("../../Data/position.json");

// Import Axios to fetch data from FourC DB
const axios = require("axios");

// joi module for validation
const Joi = require("joi");

// Function to validate json data/schema
function ValidatePosition(data) {
  const schema = Joi.object({
    timestamp: Joi.string().required(),
    speed: Joi.number(),
    direction: Joi.number(),
    longitude: Joi.number().required(),
    latitude: Joi.number().required(),
    altitude: Joi.number(),
    device: Joi.object().required(),
  });

  return (result = schema.validate(data));
}

const express = require("express");
const {
  Get4CPositions,
  Create4CPosition,
  Delete4CPosition,
} = require("./FourCDB/position4C");
const { Get4CDevices } = require("../../Devices/FourCDB/devices4C");
const router = express.Router();

// Route to get all positions
router.get("/", auth, async (req, res) => {
  // const positions = data;
  const positions = await Get4CPositions();
  res.send(positions);
});

// Route to get a position by id
router.get("/:id", auth, async (req, res) => {
  const data = await Get4CPositions();

  const position = data.find((d) => d.id === parseInt(req.params.id));
  if (!position)
    return res.status(404).send("No data found with ID: " + req.params.id);
  res.send(position);
});

// Route to create a new position item
router.post("/", auth, async (req, res) => {
  const devices = await Get4CDevices();
  const device = devices.find((d) => d.id === parseInt(req.body.device.id));

  if (!device)
    return res.status(404).send(`Device ID: ${req.body.device.id} not found!`);

  const { error } = ValidatePosition(req.body);
  if (error) return res.status(400).send(error.details[0].message);

  const newItem = {
    timestamp: req.body.timestamp,
    speed: req.body.speed,
    direction: req.body.direction,
    longitude: req.body.longitude,
    latitude: req.body.latitude,
    altitude: req.body.altitude,
    device: req.body.device,
  };

  try {
    let location = await Create4CPosition(newItem);

    if (location) {
      try {
        await axios.get(location).then((pos) => {
          res
            .status(201)
            .send({ message: "Item created", data: pos.data.result[0] });
        });
      } catch (err) {
        return res.status(304).send({
          error_message: "Something went wrong! Please try again",
          error: err,
        });
      }
    }
  } catch (err) {
    return res.status(304).send({
      error_message: "Something went wrong! Please try again",
      error: err,
    });
  }
});

// Route to edit a position item
router.patch("/", auth, async (req, res) => {
  if (!req.params.id)
    return res.status(404).send("Editing position data is not allowed.");
  res.send("Editing position data is not allowed.");
});
router.patch("/:id", (req, res) => {
  if (!req.params.id)
    return res.status(404).send("Editing position data is not allowed.");
  res.send("Editing position data is not allowed.");
});

router.put("/:id", auth, (req, res) => {
  // const position = data.find((d) => d.id === parseInt(req.params.id));
  // if (!position)
  //   return res.status(404).send("No data found with ID: " + req.params.id);

  // const { error } = ValidateNp(req.body);
  // if (error) return res.status(400).send(error.details[0].message);

  // position.longitude = req.body.longitude;
  // position.latitude = req.body.latitude;

  res.send("Editing position data is not allowed.");
});

// Route to delete a position item
router.delete("/:id", auth, async (req, res) => {
  //convert id from string to int
  const reqId = parseInt(req.params.id);
  const data = await Get4CPositions();
  const position = data.find((d) => d.id === reqId);
  if (!position) return res.status(404).send("No data found with ID: " + reqId);

  try {
    await Delete4CPosition(reqId)
      .then((result) => {
        if (result === 204) {
          let confirmDelete = {
            mesasge: "Item has been deleted",
            data: position,
          };
          return res.status(204).send(confirmDelete);
        }
      })
      .catch((err) => {
        return res.status(404).send({
          error_message: "Error Communicating with the database!",
          error: err,
        });
      });
  } catch (err) {
    return res.status(304).send({
      error_message: "Something went wrong with the request. Please try again",
      error: err,
    });
  }
});

module.exports = router;
