// Import config to set db used by api from env variable
const config = require("config");

// Import Axios to fetch data from FourC DB
const axios = require("axios");

// Import db link module
const { url_IOT_Stop, url_Hotdog } = require("../../../config/dbTypes");

// Variable for getting devices from 4C db
let url4cPosition = "";
if (config.get("dbName") === "iotstop") {
  url4cPosition = url_IOT_Stop + "position"; // Products path using iotstop db
} else if (config.get("dbName") === "hotdog") {
  url4cPosition = url_Hotdog + "position"; // Products path using hotdog db
}

// Import json sort library to sort incoming data form db
var sortJsonArray = require("sort-json-array");

// Function to get all positions from FourC DB
async function Get4CPositions() {
  return await axios
    .get(url4cPosition)
    .then((response) => {
      this.response = sortJsonArray(response.data.result, "timestamp", "des");
      return this.response;
    })
    .catch((error) => {
      this.response = {
        message: "Error communicating with the database!",
        error: error,
      };

      return this.response;
    });
}

// Function to get position data by id
async function Get4CPositionById(id) {
  return await axios
    .get(`${url4cPosition}(${id})`)
    .then((response) => {
      if (response.data["@count" === 0]) {
        this.response.response = [];
        return this.response;
      } else {
        this.response = response.data.result;
        return this.response;
      }
    })
    .catch((error) => {
      this.response = {
        message: "Error communicating with the database",
        error: error,
      };
      return this.response;
    });
}

// Function to create position to FourC db
async function Create4CPosition(data) {
  let newItem = JSON.stringify(data);

  return await axios
    .post(url4cPosition, (data = newItem))
    .then((res) => {
      this.response = res.headers.location;
      return this.response;
    })
    .catch((err) => {
      this.response = {
        error_message: "Error communicating with the database!",
        error: err,
      };
    });
}

// Function to edit positon by in to FourC db
async function Edit4CPosition(id, data) {
  let editedItem = JSON.stringify(data);

  return await axios
    .patch(`${url4cPosition}(${id})`, (data = editedItem))
    .then((res) => {
      this.response = res.data;
      return this.response;
    })
    .catch((err) => {
      this.response = {
        error_message: "Error communicating with the database",
        error: err,
      };
      return this.response;
    });
}

// Function to delete position from FourC db
async function Delete4CPosition(id) {
  return await axios
    .delete(`${url4cPosition}(${id})`)
    .then((res) => {
      this.response = res.status;
      return this.response;
    })
    .catch((err) => {
      this.response = {
        error_message: "Error communicating with the database",
        error: err,
      };
    });
}

module.exports = {
  Get4CPositions,
  Get4CPositionById,
  Create4CPosition,
  Edit4CPosition,
  Delete4CPosition,
};
