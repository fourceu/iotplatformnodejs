// for demo: import dummy data
// const data = require("../../Data/temperature.json");

// Import custom auth middleware
const auth = require("../../Middleware/auth");

// Import Axios to fetch data from FourC DB
const axios = require("axios");

// Import data from 4C module
const {
  Get4CTemperatures,
  Get4CTemperaturesById,
  Create4CTemperature,
  Delete4CTemperature,
} = require("./FourCDB/temperature4C");
const { Get4CDevices } = require("../../Devices/FourCDB/devices4C");

// joi module for validation
const Joi = require("joi");

// Function to validate json data/schema
function ValidateData(data) {
  const schema = Joi.object({
    timestamp: Joi.string().required(),
    temp: Joi.number(),
    device: Joi.object().required(),
  });

  return (result = schema.validate(data));
}

// Function to generate datetime in MySQL format
function DateTimeNow() {
  let dateTimeNow = new Date();
  let dt =
    dateTimeNow.toISOString().split("T")[0] +
    " " +
    dateTimeNow.toTimeString().split(" ")[0];

  return dt;
}

// Function to generate random number between -99 to 99
function RanNumGen() {
  var num = Math.floor(Math.random() * 99) + 1; // this will get a number between 1 and 99;
  num *= Math.floor(Math.random() * 2) == 1 ? 1 : -1; // this will add minus sign in 50% of cases

  return num;
}

const express = require("express");
const router = express.Router();

// Route to get all temperature data
router.get("/", auth, async (req, res) => {
  const temperatures = await Get4CTemperatures();

  res.send(temperatures);
});

// Route to get a temperature data by id
router.get("/:id", auth, async (req, res) => {
  const temperatures = await Get4CTemperatures();
  const temperature = temperatures.find(
    (d) => d.id === parseInt(req.params.id)
  );
  if (!temperature)
    return res.status(404).send("No data found with ID: " + req.params.id);
  res.send(temperature);
});

// Route to create a temperature data
router.post("/", auth, async (req, res) => {
  const devices = await Get4CDevices();
  const device = devices.find((d) => d.id === parseInt(req.body.device.id));

  if (!device)
    return res.status(404).send(`Device ID: ${req.body.device.id} not found!`);

  const { error } = ValidateData(req.body);
  if (error) return res.status(400).send(error.details[0].message);

  const newItem = {
    timestamp: req.body.timestamp,
    temp: req.body.temp,
    device: req.body.device,
  };

  try {
    let location = await Create4CTemperature(newItem);

    if (location) {
      try {
        await axios.get(location).then((temp) => {
          res
            .status(201)
            .send({ message: "Item created", data: temp.data.result[0] });
        });
      } catch (err) {
        return res.status(304).send({
          error_message: "Something went wrong! Please try again",
          error: err,
        });
      }
    }
  } catch (err) {
    return res.status(304).send({
      error_message: "Something went wrong! Please try again",
      error: err,
    });
  }
});

// Route to edit a temperature data
router.patch("/", (req, res) => {
  // if (!req.params.id)
  //   return res
  //     .status(404)
  //     .send("Use PUT to edit an item | Please specify id in the header");
  // res.send("Use PUT to edit a network provider");
  return res.status(403).send("Editing temperature is not allowed!");
});
router.patch("/:id", (req, res) => {
  // if (!req.params.id)
  //   return res
  //     .status(404)
  //     .send("Use PUT to edit an item | Please specify id in the header");
  // res.send("Use PUT to edit a network provider");
  return res.status(403).send("Editing temperature is not allowed!");
});

router.put("/:id", (req, res) => {
  // const temperature = data.find((d) => d.id === parseInt(req.params.id));
  // if (!temperature)
  //   return res.status(404).send("No data found with ID: " + req.params.id);

  // const { error } = ValidateData(req.body);
  // if (error) return res.status(400).send(error.details[0].message);

  // temperature.temp = req.body.temp;

  // res.send(temperature);
  return res.status(403).send("Editing temperature is not allowed!");
});

// Route to delete a temperature data
router.delete("/:id", auth, async (req, res) => {
  //convert id from string to int
  const reqId = parseInt(req.params.id);
  const temperatures = await Get4CTemperatures();
  const temperature = temperatures.find((d) => d.id === reqId);
  if (!temperature)
    return res.status(404).send("No data found with ID: " + req.params.id);

  try {
    await Delete4CTemperature(reqId)
      .then((result) => {
        if (result === 204) {
          let confirmDelete = {
            mesasge: "Item has been deleted",
            data: temperature,
          };
          return res.status(204).send(confirmDelete);
        }
      })
      .catch((err) => {
        return res.status(404).send({
          error_message: "Error Communicating with the database!",
          error: err,
        });
      });
  } catch (err) {
    return res.status(304).send({
      error_message: "Something went wrong with the request. Please try again",
      error: err,
    });
  }
});

module.exports = router;
