// Import config to set db used by api from env variable
const config = require("config");

// Import Axios to fetch data from FourC DB
const axios = require("axios");

// Import db link module
const { url_IOT_Stop, url_Hotdog } = require("../../../config/dbTypes");

// Import json sort library to sort incoming data form db
var sortJsonArray = require("sort-json-array");

// Variable for getting devices from 4C db
let url4cTemperatures = "";
if (config.get("dbName") === "iotstop") {
  url4cTemperatures = url_IOT_Stop + "temperature"; // Temperature path using iotstop db
} else if (config.get("dbName") === "hotdog") {
  url4cTemperatures = url_Hotdog + "temperature"; // Temperature path using hotdog db
}

async function Get4CTemperatures() {
  return await axios
    .get(url4cTemperatures)
    .then((response) => {
      this.response = sortJsonArray(response.data.result, "id", "des"); //returned array in descending order
      return this.response;
    })
    .catch((error) => {
      this.response = {
        message: "Error communicating with the database!",
        error: error,
      };
      return this.response;
    });
}

async function Get4CTemperaturesById(id) {
  return await axios
    .get(`${url4cTemperatures}(${id})`)
    .then((response) => {
      if (response.data["@count"] === 0) {
        this.response = "item NOT found";
        return this.response;
      } else {
        this.response = response.data.result;
        return this.response;
      }
    })
    .catch((error) => {
      this.response = {
        message: "Error communicating with the database!",
        error: error,
      };
      return this.response;
    });
}

async function Create4CTemperature(data) {
  var newItem = JSON.stringify(data);

  return await axios
    .post(url4cTemperatures, (data = newItem))
    .then((res) => {
      this.response = res.headers.location;
      return this.response;
    })
    .catch((err) => {
      this.response = {
        message: "Error communicating with the database!",
        error: err,
      };
      return this.response;
    });
}

async function Delete4CTemperature(id) {
  return await axios
    .delete(`${url4cTemperatures}(${id})`)
    .then((res) => {
      this.response = res.status;
      return this.response;
    })
    .catch((err) => {
      this.response = {
        error_message: "Error communicating with the database",
        error: err,
      };
    });
}

module.exports = {
  url4cTemperatures,
  Get4CTemperatures,
  Get4CTemperaturesById,
  Create4CTemperature,
  Delete4CTemperature,
};
