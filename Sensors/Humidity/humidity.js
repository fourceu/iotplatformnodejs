// for demo: import dummy data
// const data = require("../../Data/humidity.json");

// Import custom auth middleware
const auth = require("../../Middleware/auth");
const {
  Get4CHumidities,
  Create4CHumidity,
  Delete4CHumidity,
} = require("./FourCDB/humidity4C");
const { Get4CDevices } = require("../../Devices/FourCDB/devices4C");

// Import Axios to fetch data from FourC DB
const axios = require("axios");

// joi module for validation
const Joi = require("joi");

// Function to validate json data/schema
function ValidateData(data) {
  const schema = Joi.object({
    timestamp: Joi.string().required(),
    level: Joi.number(),
    device: Joi.object().required(),
  });

  return (result = schema.validate(data));
}

const express = require("express");
const router = express.Router();

// Route to get all humidity data
router.get("/", auth, async (req, res) => {
  const humidities = await Get4CHumidities();
  res.send(humidities);
});

// Route to get a humidity data by id
router.get("/:id", auth, async (req, res) => {
  const data = await Get4CHumidities();
  const reqId = parseInt(req.params.id);
  const humidity = data.find((d) => d.id === reqId);
  if (!humidity)
    return res.status(404).send("No data found with ID: " + req.params.id);
  res.send(humidity);
});

// Route to create a humidity data
router.post("/", auth, async (req, res) => {
  const devices = await Get4CDevices();
  const device = devices.find((d) => d.id === parseInt(req.body.device.id));

  if (!device)
    return res.status(404).send(`Device ID: ${req.body.device.id} not found!`);

  const { error } = ValidateData(req.body);
  if (error) return res.status(400).send(error.details[0].message);

  const newItem = {
    timestamp: req.body.timestamp,
    level: req.body.level,
    device: req.body.device,
  };

  try {
    let location = await Create4CHumidity(newItem);

    if (location) {
      await axios.get(location).then((hum) => {
        res.send({ message: "Item created", data: hum.data.result[0] });
      });
    }
  } catch (err) {
    return res.status(304).send({
      error_message: "Something went wrong! Please try again",
      error: err,
    });
  }
});

// Route to edit a humidity data
router.patch("/", auth, async (req, res) => {
  if (!req.params.id)
    return res.status(404).send("Editing humidity data is not allowed!");
  res.send("Editing humidity data is not allowed!");
});
router.patch("/:id", (req, res) => {
  if (!req.params.id)
    return res.status(404).send("Editing humidity data is not allowed!");
  res.send("Editing humidity data is not allowed!");
});

router.put("/:id", auth, async (req, res) => {
  // const humidity = data.find((d) => d.id === parseInt(req.params.id));
  // if (!humidity)
  //   return res.status(404).send("No data found with ID: " + req.params.id);

  // const { error } = ValidateData(req.body);
  // if (error) return res.status(400).send(error.details[0].message);

  // humidity.level = req.body.level;

  res.send("Editing humidity data is not allowed!");
});

// Route to delete a humidity data
router.delete("/:id", auth, async (req, res) => {
  //convert id from string to int
  const reqId = parseInt(req.params.id);
  const data = await Get4CHumidities();
  const humidity = data.find((d) => d.id === reqId);
  if (!humidity) return res.status(404).send("No data found with ID: " + reqId);

  try {
    await Delete4CHumidity(reqId)
      .then((result) => {
        if (result === 204) {
          let confirmDelete = {
            mesasge: "Item has been deleted",
            data: humidity,
          };
          return res.status(204).send(confirmDelete);
        }
      })
      .catch((err) => {
        return res.status(404).send({
          error_message: "Error Communicating with the database!",
          error: err,
        });
      });
  } catch (err) {
    return res.status(304).send({
      error_message: "Something went wrong with the request. Please try again",
      error: err,
    });
  }
});

module.exports = router;
