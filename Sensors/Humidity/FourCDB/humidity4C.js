// Import config to set db used by api from env variable
const config = require("config");

// Import Axios to fetch data from FourC DB
const axios = require("axios");

// Import db link module
const { url_IOT_Stop, url_Hotdog } = require("../../../config/dbTypes");

// Import json sort library to sort incoming data form db
var sortJsonArray = require("sort-json-array");

// Variable for getting devices from 4C db
let url4cHumidity = "";
if (config.get("dbName") === "iotstop") {
  url4cHumidity = url_IOT_Stop + "humidity"; // Temperature path using iotstop db
} else if (config.get("dbName") === "hotdog") {
  url4cHumidity = url_Hotdog + "humidity"; // Temperature path using hotdog db
}

async function Get4CHumidities() {
  return await axios
    .get(url4cHumidity)
    .then((response) => {
      this.response = sortJsonArray(response.data.result, "id", "des"); //returned array in descending order
      return this.response;
    })
    .catch((error) => {
      this.response = {
        message: "Error communicating with the database!",
        error: error,
      };
      return this.response;
    });
}

async function Get4CHumidityById(id) {
  return await axios
    .get(`${url4cHumidity}(${id})`)
    .then((response) => {
      if (response.data["@count"] === 0) {
        this.response = "item NOT found";
        return this.response;
      } else {
        this.response = response.data.result;
        return this.response;
      }
    })
    .catch((error) => {
      this.response = {
        message: "Error communicating with the database!",
        error: error,
      };
      return this.response;
    });
}

async function Create4CHumidity(data) {
  var newItem = JSON.stringify(data);

  return await axios
    .post(url4cHumidity, (data = newItem))
    .then((res) => {
      this.response = res.headers.location;
      return this.response;
    })
    .catch((err) => {
      this.response = {
        message: "Error communicating with the database!",
        error: err,
      };
      return this.response;
    });
}

async function Delete4CHumidity(id) {
  return await axios
    .delete(`${url4cHumidity}(${id})`)
    .then((res) => {
      this.response = res.status;
      return this.response;
    })
    .catch((err) => {
      this.response = {
        error_message: "Error communicating with the database",
        error: err,
      };
    });
}

module.exports = {
  Get4CHumidities,
  Get4CHumidityById,
  Create4CHumidity,
  Delete4CHumidity,
};
