// Import config to set db used by api from env variable
const config = require("config");
// Import url for iotstop and hotdog
const { url_IOT_Stop, url_Hotdog } = require("../config/dbTypes");

function setUrl(pathname) {
  let url4CPath = "";
  if (config.get("dbName") === "iotstop") {
    url4CPath = url_IOT_Stop + pathname;
  } else if (config.get("dbName") === "hotdog") {
    url4CPath = url_Hotdog + pathname;
  }

  return url4CPath;
}

module.exports = {
  setUrl,
};
