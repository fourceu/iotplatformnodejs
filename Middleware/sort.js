function sortArrayDescId(array) {
  if (!array || array.length < 1) {
    return null;
  } else {
    return array.slice().sort((a, b) => b.id - a.id);
  }
}

module.exports = {
  sortArrayDescId,
};
