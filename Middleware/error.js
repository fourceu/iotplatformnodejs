const winston = require("winston");

module.exports = function (err, req, res, next) {
  // Log the exception
  /* Error levels:
   * error
   * warning
   * info
   * verbose
   * debug
   * silly
   */
  winston.error(err.message, err);

  // Return error response
  res.status(500).send("Oopss! Something went wrong...");
};
