"use strict";

// Import Authentication Modules
var Auth = require("./Authentication/Auth");

var Users = require("./Authentication/User");

var _require = require("./Authentication/Logger"),
    Login = _require.Login,
    Authenticating = _require.Authenticating; // Import Group Module


var GroupRoutes = require("./Groups/groupRoutes"); // Import Devices Module


var DeviceRoutes = require("./Devices/devices"); // Import Network provider Module


var NetworkProviders = require("./NetworkProviders/networkprovider"); // Import Position/GPS Location Module


var Position = require("./Sensors/GPS/position"); // Import Humidity Module


var Humidity = require("./Sensors/Humidity/humidity"); // Import Temperature Module


var Temperature = require("./Sensors/Temperature/temperature"); // Import config module


var config = require("config"); // Import debug module


var debug = require("debug")("app:startup"); // Express setup


var express = require("express");

var app = express(); // Import Path module

var path = require("path"); // Import helmet


var helmet = require("helmet"); // Import morgan


var morgan = require("morgan"); // Setting configuration
// Create following env variable before using this config
// app_password = your password
// NODE_ENV=development || production || default


console.log("Application Name: " + config.get("name"));
console.log("Mail Server: " + config.get("mail.host"));
console.log("Mail Server Password: " + config.get("mail.password")); // Import CORS middleware to fix cross origin issue

var cors = require("cors");

app.use(cors()); // Express middleware

app.use(express.json()); // read json body

app.use(express["static"]("public")); // serve static folders for html

app.use(express.urlencoded({
  extended: true
}) // used for reading form data from html page
); // Using helmet for securing the node app

app.use(helmet()); // Look for jwt app key in config

if (!config.get("jwtPrivateKey")) {
  console.error("FATAL ERROR: jwt private key (jwtPrivateKey) is not defined!");
  process.exit(1);
} // Using morgan for HTTP request logger on dev env


if (app.get("env") === "development") {
  app.use(morgan("tiny"));
  debug("Morgan Enabled...");
} // Calling custom middleware function for login


app.use(Login); // Calling custom middleware function for authentication

app.use(Authenticating); // Using Templating/View engine

app.set("view engine", "pug");
app.set("views", "./views"); // Api root route

app.get("/", function (req, res) {
  res.sendFile("index.html", {
    root: path.join(__dirname, "./views")
  });
}); // // Api pug view route
// app.get("/view", (req, res) => {
//   res.render("index.pug", {
//     title: "IOT Platform App",
//     message: "Welcome to FourC IOT Platform",
//   });
// });
// Users route function

app.use("/api/users", Users); // Authentication route function

app.use("/api/auth", Auth); // Group routes function

app.use("/api/groups", GroupRoutes); // Using Devices route module

app.use("/api/devices", DeviceRoutes); // Using netowrk providers route module

app.use("/api/networkproviders", NetworkProviders); // Using positions route module

app.use("/api/positions", Position); // Using humidity values route module

app.use("/api/humidities", Humidity); // Using temperature values route module

app.use("/api/temperatures", Temperature); // Listner
// PORT from env var

var port = process.env.PORT || 3001;
app.listen(port, function () {
  return console.log("listen on port:".concat(port, "..."));
});