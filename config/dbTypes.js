// Import config to set db used by api from env variable
const config = require("config");

const url_4C_main = "http://10.44.45.80:8080";

const url_IOT_Stop = url_4C_main + "/iotstop/";
const iotstop_db = "iotstop";

const url_Hotdog = url_4C_main + "/hotdog/";
const hotdog_db = "hotdog";

const span_url = "https://api.lab5e.com/span";
const hotdog_collection_id = "2f3a11687f79jd";
const iotstop_collection_id = "2f3a11687f79je";
const span_headers = {
  "X-API-Token": config.get("spanApiKey"),

  "Content-Type": "application/json",
};

module.exports = {
  url_4C_main,
  url_IOT_Stop,
  url_Hotdog,
  iotstop_db,
  hotdog_db,
  span_url,
  hotdog_collection_id,
  iotstop_collection_id,
  span_headers,
};
