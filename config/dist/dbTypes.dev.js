"use strict";

var url_4C_main = "http://10.44.45.80:8080";
var url_IOT_Stop = url_4C_main + "/iotstop/";
var url_Hotdog = url_4C_main + "/hotdog/";
module.exports = {
  url_IOT_Stop: url_IOT_Stop,
  url_Hotdog: url_Hotdog
};