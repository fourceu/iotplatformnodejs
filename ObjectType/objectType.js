// Import custom auth middleware
const auth = require("../Middleware/auth");
// joi module for validation
const Joi = require("joi");
// Import Axios to fetch data from FourC DB
const axios = require("axios");

const express = require("express");
const router = express.Router();

const {
  Get4CObjectTypes,
  Create4CObjectTypes,
  Delete4CObjectType,
  Get4CObjectTypeById,
  Edit4CObjectType,
} = require("./dbServices");

// Function to validate req body
function ValidateNewOT(ot) {
  const schema = Joi.object({
    description: Joi.string().required(),
  });

  return (result = schema.validate(ot));
}

// Route to get all object types
router.get("/", auth, async (req, res) => {
  const objectTypes = await Get4CObjectTypes();

  const sortedData = objectTypes.slice().sort((a, b) => b.id - a.id);

  res.send(sortedData);
});

// Route to get object type by id
router.get("/:id", auth, async (req, res) => {
  const objectType = await Get4CObjectTypeById(parseInt(req.params.id));

  if (!objectType)
    return res.status(404).send(`No item found with ID: ${req.params.id}`);

  res.send(objectType);
});

// Function to create an object type
async function CreateOt(req, res) {
  const item = {
    description: req.body.description,
  };

  let location;

  try {
    location = await Create4CObjectTypes(item);
    if (location) {
      // console.log(location);
      await axios
        .get(location)
        .then((ot) => {
          res.status(201).send(ot.data.result[0]);
        })
        .catch((err) => res.status(400).send(err));
    }
  } catch (error) {
    console.log("Error: " + error);
  }
}

// Route to create a object type
router.post("/", auth, async (req, res) => {
  const { error } = ValidateNewOT(req.body);
  if (error) return res.status(400).send(error.details[0].message);

  await Get4CObjectTypes().then(async (ot) => {
    if (ot.find((o) => o.description === req.body.description)) {
      return res.status(400).send("Object type already exists!");
    } else {
      await CreateOt(req, res);
    }
  });
});

// Route to edit a object type
router.patch("/:id", auth, async (req, res) => {
  // Get the object type by id
  const ObjectType = await Get4CObjectTypeById(parseInt(req.params.id));
  // Get all object types and make sure edited name doesn't conflict
  const ObjectTypes = await Get4CObjectTypes();
  const Ot = ObjectTypes.find((o) => o.description === req.body.description);

  if (!ObjectType)
    return res.status(404).send(`No item found with ID: ${req.params.id}`);

  if (Ot)
    return res
      .status(404)
      .send(`Item with same description/name already exists`);

  const { error } = ValidateNewOT(req.body);
  if (error) return res.status(400).send(error.details[0].message);

  const response = await Edit4CObjectType(parseInt(req.params.id), {
    description: req.body.description,
  });

  res.status(200).send({ message: "Item edited", item: response });
});

// Route to delete a object type
router.delete("/:id", auth, async (req, res) => {
  // Get objectt types to ensure requested id exists
  const ObjectType = await Get4CObjectTypeById(parseInt(req.params.id));

  if (!ObjectType)
    return res
      .status(404)
      .send(`Object type with ID: ${req.params.id} doesn't exist`);

  const response = await Delete4CObjectType(
    parseInt(req.params.id),
    ObjectType
  );

  if (response?.item) {
    res.status(200).send(response.item);
  }
});

module.exports = router;
