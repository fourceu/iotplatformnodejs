// Import config to set db used by api from env variable
const config = require("config");
// Import Axios to fetch data from FourC DB
const axios = require("axios");
// Import url for iotstop and hotdog
const { url_IOT_Stop, url_Hotdog } = require("../config/dbTypes");

let url4CObjectType = "";
if (config.get("dbName") === "iotstop") {
  url4CObjectType = url_IOT_Stop + "objecttype";
} else if (config.get("dbName") === "hotdog") {
  url4CObjectType = url_Hotdog + "objecttype";
}

// Function to get all object types
async function Get4CObjectTypes() {
  return await axios
    .get(url4CObjectType)
    .then((res) => {
      this.response = res.data.result;
      return this.response;
    })
    .catch((err) => {
      this.response = {
        message: "Error communicating with the database!",
        error: err,
      };
      return this.response;
    });
}

// Function to get object type by id
async function Get4CObjectTypeById(id) {
  return await axios
    .get(`${url4CObjectType}(id=${id})`)
    .then((res) => {
      this.response = res.data.result[0];
      return this.response;
    })
    .catch((err) => {
      this.response = {
        message: "Error communicating with the database!",
        error: err,
      };
      return this.response;
    });
}

// Function to create a object types
async function Create4CObjectTypes(newData) {
  return await axios
    .post(url4CObjectType, (data = newData))
    .then((res) => {
      this.response = res.headers.location;
      return this.response;
    })
    .catch((err) => {
      this.response = {
        message: "Error communicating with the database!",
        error: err,
      };
      return this.response;
    });
}

//Function to edit/patch a object type
async function Edit4CObjectType(id, edData) {
  console.log(edData);
  return await axios
    .patch(`${url4CObjectType}(id=${id})`, (data = edData))
    .then((res) => {
      this.response = res.data;
      return this.response;
    })
    .catch((err) => {
      this.response = {
        message: "Error communicating with the database!",
        error: err,
      };
      return this.response;
    });
}

// Function to delete a object type
async function Delete4CObjectType(id, item) {
  return await axios
    .delete(`${url4CObjectType}(id=${id})`)
    .then((res) => {
      if (res.status === 204) {
        this.response = {
          message: "Item Deleted",
          item: item,
        };
        return this.response;
      }
    })
    .catch((err) => {
      this.response = {
        message: "Error communicating with the database!",
        error: err,
      };
      return this.response;
    });
}

module.exports = {
  Get4CObjectTypes,
  Get4CObjectTypeById,
  Create4CObjectTypes,
  Edit4CObjectType,
  Delete4CObjectType,
};
