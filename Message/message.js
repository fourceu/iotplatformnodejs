// Import UUID to create message unique identifier
const uuidv4 = require("uuid");
// joi module for validation
const Joi = require("joi");

// Import custom auth middleware
const auth = require("../Middleware/auth");
const express = require("express");
const router = express.Router();

// import dependant variables and functions
const {
  collection_id,
  sendPayloadToDevice,
} = require("../Span/Devices/spanDevices");
const { Get4CMessages } = require("./dbServices");
const { sortArrayDescId } = require("../Middleware/sort");
const { Get4CDeviceById } = require("../Devices/FourCDB/devices4C");
const { getMethods } = require("../Methods/dbServices");
const { Get4CObjectTypes } = require("../ObjectType/dbServices");

// Dummy long ass message
const msg = `Base64 encoding is a way to convert data (typically binary) 
into the ASCII character set. It is important to mention here that 
Base64 is not an encryption or compression technique, 
`;

// Function to validate req body
// Funciton to validate long payload
function ValidateMsgPayload(item) {
  const schema = Joi.object({
    message: Joi.string().required(),
  });

  return (result = schema.validate(item));
}

// Function to validate Groups
function ValidateGPSBody(item) {
  const schema = Joi.object({
    status: Joi.boolean().required(),
  });

  return (result = schema.validate(item));
}

// Calculate byte
function calculateTotalByteSize(data) {
  return Buffer.byteLength(data, "utf8");
}

// Divide long message into chuncks
function chunk(s, maxBytes) {
  let buf = Buffer.from(s);
  const result = [];
  while (buf.length) {
    let i = buf.lastIndexOf(32, maxBytes + 1);
    // If no space found, try forward search
    if (i < 0) i = buf.indexOf(32, maxBytes);
    // If there's no space at all, take the whole string
    if (i < 0) i = buf.length;
    // This is a safe cut-off point; never half-way a multi-byte
    result.push(buf.slice(0, i).toString());
    buf = buf.slice(i + 1); // Skip space (if any)
  }
  return result;
}

// Create frames from chuncks
function createFrames(data) {
  const totalFrames = data.length;
  let frames = [];
  for (i = 0; i < totalFrames; i++) {
    frames.push({ frame: i + 1, frames: totalFrames, data: data[i] });
  }
  return frames;
}

// Create message structure for multiple message
function createMessageStructure(i, device, frames, size, msg, method) {
  const content = {
    object: method?.objecttype?.name,
    method: method?.description,
    values: {
      message: frames[i].data,
    },
  };
  const message = {
    message_id: uuidv4(),
    device_to: device?.networkprovider_device_id,
    device_from: { id: device?.id, name: device?.name } || null,
    in_reply_to: null,
    created: new Date(),
    ttl: new Date(),
    frame: frames[i]?.frame || 1,
    frames: frames[i]?.frames || 1,
    length: frames.length > 1 ? calculateTotalByteSize(frames[i]?.data) : size,
    data: frames.length > 1 ? content : msg,
  };

  const stringMessage = JSON.stringify(message);

  // Encode message to base64 to send over span
  const base64Message = Buffer.from(stringMessage).toString("base64");

  return { original: message, b64Encoded: base64Message };
}

// Create message structure for single message
function singleMessageStructure(device, size, msg) {
  const message = {
    message_id: uuidv4(),
    device_to: device?.networkprovider_device_id,
    created: new Date(),
    ttl: new Date(),
    length: size,
    data: msg,
  };

  const stringMessage = JSON.stringify(message);

  // Encode message to base64 to send over span
  const base64Message = Buffer.from(stringMessage).toString("base64");

  return { original: message, b64Encoded: base64Message };
}

// Get Methods and Objects and create structure of methods with object name
async function getMothodsObjects() {
  let methodsArray = [];
  //get methods from db
  const methodResponse = await getMethods();
  // get objects from db
  const objectResponse = await Get4CObjectTypes();
  // find the reset method based on the description
  if (methodResponse.length > 0) {
    for (i = 0; i < methodResponse.length; i++) {
      const methodItem = methodResponse[i];
      const obj = objectResponse.find((o) => o.id === methodItem.objecttype.id);
      const methodObj = {
        ...methodItem,
        objecttype: {
          id: obj.id,
          name: obj.description,
        },
      };

      methodsArray.push(methodObj);
    }
    return methodsArray;
  } else {
    return [];
  }
}

////////////////////////////////////////////////////////////////////////////////////
// Route to get all messages
///////////////////////////////////////////////////////////////////////////////////
router.get("/all", auth, async (req, res) => {
  const messagesArray = await Get4CMessages();
  const messagesDescendingOrder = sortArrayDescId(messagesArray);

  res.send(messagesDescendingOrder);
});
////////////////////////////////////////////////////////////////////////////////////
// Route to restart a device
///////////////////////////////////////////////////////////////////////////////////
router.post("/restartdevice/:id", auth, async (req, res) => {
  const deviceId = req.params.id;
  if (!deviceId)
    return res
      .status(400)
      .send("Device ID is requried! Please specify the FourC Device ID.");

  // Get device by ID
  const deviceResponse = await Get4CDeviceById(deviceId);
  const device = deviceResponse.data;
  // Return if no device found
  if (!device || device === null)
    return res
      .status(404)
      .send(`No device found with ID: ${deviceId}. Command cannot be sent!`);

  // Return if Device has not network provider device id
  if (
    !device?.networkprovider_device_id ||
    device?.networkprovider_device_id === null
  )
    return res
      .status(400)
      .send(
        `Device with ID: ${deviceId} does not have any associated Network provider device id. Message cannot be sent to this device.`
      );

  const methods = await getMothodsObjects();
  if (!methods) return res.status(404).send("No methods found in db");

  let method = methods.find(
    (m) => m.description === "RESTART" && m.objecttype.name === "device"
  );
  if (!method)
    return res
      .status(404)
      .send("No method found for restart OR Method not allowed");

  // create a message structure with the content
  const content = {
    object: method?.objecttype?.name,
    method: method?.description,
  };

  const contentString = JSON.stringify(content);

  // Calculate total size of the content in bytes
  const size = calculateTotalByteSize(contentString);

  let singleMessage = singleMessageStructure(device, size, contentString);
  const spanDeviceId = device?.networkprovider_device_id;
  // Send message over span
  const spanPayload = JSON.stringify(singleMessage.original);

  const spanResponse = await sendPayloadToDevice(spanDeviceId, spanPayload);

  if (spanResponse) {
    res.send({
      span_response: spanResponse,
      original_message: {
        size: calculateTotalByteSize(spanPayload),
        payload: singleMessage.original,
      },
    });
  } else {
    res.status(400).send("Could not send data over span!");
  }
});
////////////////////////////////////////////////////////////////////////////////////////////
// Route to turn off a device
////////////////////////////////////////////////////////////////////////////////////////////
router.post("/turnoffdevice/:id", auth, async (req, res) => {
  const deviceId = req.params.id;
  if (!deviceId)
    return res
      .status(400)
      .send("Device ID is requried! Please specify the FourC Device ID.");

  // Get device by ID
  const deviceResponse = await Get4CDeviceById(deviceId);
  const device = deviceResponse.data;
  // Return if no device found
  if (!device || device === null)
    return res
      .status(404)
      .send(`No device found with ID: ${deviceId}. Command cannot be sent!`);

  // Return if Device has not network provider device id
  if (
    !device?.networkprovider_device_id ||
    device?.networkprovider_device_id === null
  )
    return res
      .status(400)
      .send(
        `Device with ID: ${deviceId} does not have any associated Network provider device id. Message cannot be sent to this device.`
      );

  const methods = await getMothodsObjects();
  if (!methods) return res.status(404).send("No methods found in db");

  let method = methods.find(
    (m) => m.description === "TURNOFF" && m.objecttype.name === "device"
  );
  if (!method)
    return res
      .status(404)
      .send("No method found for power off OR Method not allowed");

  // create a message structure with the content
  const content = {
    object: method?.objecttype?.name,
    method: method?.description,
  };

  const contentString = JSON.stringify(content);

  // Calculate total size of the content in bytes
  const size = calculateTotalByteSize(contentString);

  let singleMessage = singleMessageStructure(device, size, contentString);

  const spanDeviceId = device?.networkprovider_device_id;
  const spanPayload = JSON.stringify(singleMessage.original);
  // Send message over span
  const spanResponse = await sendPayloadToDevice(spanDeviceId, spanPayload);

  if (spanResponse) {
    res.send({
      span_response: spanResponse,
      original_message: {
        size: calculateTotalByteSize(spanPayload),
        payload: singleMessage.original,
      },
    });
  } else {
    res.status(400).send("Could not send data over span!");
  }
});
////////////////////////////////////////////////////////////////////////////////////////
// Route to turn off GPS/GNSS
////////////////////////////////////////////////////////////////////////////////////////
router.post("/gps/:id", auth, async (req, res) => {
  const { error } = ValidateGPSBody(req.body);
  if (error) return res.status(400).send(error.details[0].message);

  const gpsStatus = req.body.status;

  const deviceId = req.params.id;
  if (!deviceId)
    return res
      .status(400)
      .send("Device ID is requried! Please specify the FourC Device ID.");

  // Get device by ID
  const deviceResponse = await Get4CDeviceById(deviceId);
  const device = deviceResponse.data;
  // Return if no device found
  if (!device || device === null)
    return res
      .status(404)
      .send(`No device found with ID: ${deviceId}. Command cannot be sent!`);

  // Return if Device has not network provider device id
  if (
    !device?.networkprovider_device_id ||
    device?.networkprovider_device_id === null
  )
    return res
      .status(400)
      .send(
        `Device with ID: ${deviceId} does not have any associated Network provider device id. Message cannot be sent to this device.`
      );

  const methods = await getMothodsObjects();
  if (!methods) return res.status(404).send("No methods found in db");

  let methodTurnOn = methods.find(
    (m) => m.description === "TURNON" && m.objecttype.name === "sensor"
  );
  if (!methodTurnOn)
    return res
      .status(404)
      .send("No method found to turn on gps/gnss OR Method not allowed");

  let methodTurnOff = methods.find(
    (m) => m.description === "TURNOFF" && m.objecttype.name === "sensor"
  );
  if (!methodTurnOff)
    return res
      .status(404)
      .send("No method found to turn off gps/gnss OR Method not allowed");

  // create a message structure with the content
  // if status in req.body is true, send turn on gps command
  // if status in req.body is false, send turn off gps command
  // object and method type switches based on req.body.status
  const content = {
    object: gpsStatus
      ? methodTurnOn?.objecttype?.name
      : methodTurnOff?.objecttype?.name,
    method: gpsStatus ? methodTurnOn?.description : methodTurnOff?.description,
    values: {
      sensors: ["GPS"],
    },
  };

  const contentString = JSON.stringify(content);

  // Calculate total size of the content in bytes
  const size = calculateTotalByteSize(contentString);

  let singleMessage = singleMessageStructure(device, size, contentString);

  const spanDeviceId = device?.networkprovider_device_id;
  const spanPayload = JSON.stringify(singleMessage.original);
  // Send message using span
  const spanResponse = await sendPayloadToDevice(spanDeviceId, spanPayload);
  // console.log(spanResponse);

  if (spanResponse) {
    res.send({
      span_response: spanResponse,
      original_message: {
        size: calculateTotalByteSize(spanPayload),
        payload: singleMessage.original,
      },
    });
  } else {
    res.status(400).send("Could not send data over span!");
  }
});
///////////////////////////////////////////////////////////////////////////////////////////
// Route to send message payload
///////////////////////////////////////////////////////////////////////////////////////////
router.post("/:id", auth, async (req, res) => {
  const deviceId = req.params.id;
  if (!deviceId)
    return res
      .status(400)
      .send("Device ID is requried! Please specify the FourC Device ID.");

  const { error } = ValidateMsgPayload(req.body);
  if (error) return res.status(400).send(error.details[0].message);

  const msg = req.body.message;

  // Get device by ID
  const deviceResponse = await Get4CDeviceById(deviceId);
  const device = deviceResponse.data;
  // Return if no device found
  if (!device || device === null)
    return res
      .status(404)
      .send(`No device found with ID: ${deviceId}. Command cannot be sent!`);

  // Return if Device has not network provider device id
  if (
    !device?.networkprovider_device_id ||
    device?.networkprovider_device_id === null
  )
    return res
      .status(400)
      .send(
        `Device with ID: ${deviceId} does not have any associated Network provider device id. Message cannot be sent to this device.`
      );

  const methods = await getMothodsObjects();
  if (!methods) return res.status(404).send("No methods found in db");

  let method = methods.find(
    (m) => m.description === "SET" && m.objecttype.name === "device"
  );
  if (!method)
    return res
      .status(404)
      .send("No method found to send message to device OR Method not allowed");

  // create a message structure with the content
  const content = {
    object: method?.objecttype?.name,
    method: method?.description,
    values: {
      message: msg,
    },
  };

  const contentString = JSON.stringify(content);
  const msgString = JSON.stringify(msg);

  // Calculate total size of the content in bytes
  const size = calculateTotalByteSize(msgString);

  let result,
    frames,
    messages = [];

  // If content if over 512 bytes then break the content into small chunks
  if (size > 512) {
    result = chunk(msgString, 256); //Chunck size set to bytes
    frames = createFrames(result);

    for (let i = 0; i < frames.length; i++) {
      let message = createMessageStructure(
        i,
        device,
        frames,
        null,
        null,
        method
      );
      const spanDeviceId = device?.networkprovider_device_id;
      const spanPayload = JSON.stringify(message.original);
      // Send message using span
      const spanResponse = await sendPayloadToDevice(spanDeviceId, spanPayload);
      if (spanResponse) {
        messages.push({
          span_response: spanResponse,
          original_message_size: calculateTotalByteSize(
            JSON.stringify(message.original)
          ),
          original_message: message.original,
        });
      } else {
        console.log("Error sending messages over span");
      }
    }

    res.send(messages);
  } else {
    let singleMessage = singleMessageStructure(device, size, contentString);

    const spanDeviceId = device?.networkprovider_device_id;
    const spanPayload = JSON.stringify(singleMessage.original);
    // Send message using span
    const spanResponse = await sendPayloadToDevice(spanDeviceId, spanPayload);

    if (spanResponse) {
      res.send({
        span_response: spanResponse,
        original_message: {
          size: calculateTotalByteSize(spanPayload),
          payload: singleMessage.original,
        },
      });
    } else {
      res.status(400).send("Could not send data over span!");
    }
  }
});
/////////////////////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////////////////////

module.exports = router;
