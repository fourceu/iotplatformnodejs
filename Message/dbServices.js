// Import config to set db used by api from env variable
const config = require("config");
// Import Axios to fetch data from FourC DB
const axios = require("axios");
// Import set url to set url for iotstop or hotdog
const { setUrl } = require("../Middleware/url");
const url4CMessage = setUrl("message");

// Function to get all messages from 4C IoT DB
async function Get4CMessages() {
  return await axios
    .get(url4CMessage)
    .then((res) => {
      this.response = res.data.result;
      return this.response;
    })
    .catch((err) => {
      this.response = {
        message: "Error communicating with the database!",
        error: err,
      };
      return this.response;
    });
}

module.exports = {
  Get4CMessages,
};
