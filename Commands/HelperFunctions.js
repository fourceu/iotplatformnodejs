const fs = require("fs");
const moment = require("moment");
const converter = require("image_to_epaper_converter");
const uuidv4 = require("uuid");
const { getMethods } = require("../Methods/dbServices");
const { Get4CObjectTypes } = require("../ObjectType/dbServices");

let methodsArray = [];
// Get Methods and Objects and create structure of methods with object name
async function getMothodsObjects() {
  //get methods from db
  const methodResponse = await getMethods();
  // get objects from db
  const objectResponse = await Get4CObjectTypes();
  // find the reset method based on the description
  if (methodResponse.length > 0) {
    for (i = 0; i < methodResponse.length; i++) {
      const methodItem = methodResponse[i];
      const obj = objectResponse.find((o) => o.id === methodItem.objecttype.id);
      const methodObj = {
        ...methodItem,
        objecttype: {
          id: obj.id,
          name: obj.description,
        },
      };

      methodsArray.push(methodObj);
    }
    return methodsArray;
  } else {
    return [];
  }
}

async function getMethodsByObjectId(objectName) {
  let methodsArray = [];
  const methods = await getMothodsObjects();
  if (methods.length > 0) {
    for (i = 0; i < methods.length; i++) {
      if (methods[i].objecttype.name === objectName) {
        methodsArray.push(methods[i]);
      }
    }
    return methodsArray;
  } else {
    return null;
  }
}

// Calculate byte
function calculateTotalByteSize(data) {
  return Buffer.byteLength(data, "utf8");
}

// Divide long message into chuncks
// function chunk(s, maxBytes) {
//   let buf = Buffer.from(s);
//   const result = [];
//   while (buf.length) {
//     let i = buf.lastIndexOf(32, maxBytes + 1);
//     // If no space found, try forward search
//     if (i < 0) i = buf.indexOf(32, maxBytes);
//     // If there's no space at all, take the whole string
//     if (i < 0) i = buf.length;
//     // This is a safe cut-off point; never half-way a multi-byte
//     result.push(buf.slice(0, i).toString());
//     buf = buf.slice(i + 1); // Skip space (if any)
//   }
//   return result;
// }
function chunk(str, size) {
  const numChunks = Math.ceil(str.length / size);
  const chunks = new Array(numChunks);

  for (let i = 0, o = 0; i < numChunks; ++i, o += size) {
    chunks[i] = str.substr(o, size);
  }

  return chunks;
}

// Create frames from chuncks
function createFrames(data) {
  const totalFrames = data.length;
  let frames = [];
  for (i = 0; i < totalFrames; i++) {
    frames.push({ frame: i + 1, frames: totalFrames, data: data[i] });
  }
  return frames;
}

//Convert time to epoch
function epoch(dt) {
  return Date.parse(dt);
}

// Create content structure
function contentStructure(methodName, jsonBody) {
  if (jsonBody) {
    return {
      object: methodName?.objecttype?.id,
      method: methodName?.id,
      values: jsonBody,
    };
  } else {
    return {
      object: methodName?.objecttype?.id,
      method: methodName?.id,
    };
  }
}

// Create message structure for single message
function singleMessageStructure(device, msg) {
  const tTTL = new Date();
  // tTTL.setHours(tTTL.getHours() + 1);
  tTTL.setSeconds(tTTL.getSeconds() + device.offline_messageignoretime);
  const mTTL = moment(tTTL).format("YYYY-MM-DDTHH:mm:ss.SSSZZ");

  let tCreated = moment(new Date()).format("YYYY-MM-DDTHH:mm:ss.SSSZZ");
  // tCreated.setHours(tTTL.getHours());

  const message = {
    message_id: uuidv4(),
    device_to: device?.networkprovider_device_id,
    created: tCreated,
    ttl: mTTL,
    length: calculateTotalByteSize(JSON.stringify(msg)),
    data: JSON.stringify(msg),
  };

  const messageStr = JSON.stringify(message);
  const messageByteSize = calculateTotalByteSize(messageStr);

  return [message, messageByteSize];
}

// Create message structure for multiple message
function createFramedMessageStructure(
  i,
  device,
  frames,
  msg,
  method,
  id,
  jsonBody,
  fileFormat
) {
  // const content = {
  //   object: method?.objecttype?.id,
  //   method: method?.id,
  //   values: {
  //     ...jsonBody,
  //     format: fileFormat || null,
  //     bitmap: frames[i].data,
  //   },
  // };

  // Create ttl with added message ignore time seconds from device
  let tTTL = new Date();
  // tTTL.setHours(tTTL.getHours() + 1);
  tTTL.setSeconds(tTTL.getSeconds() + device.offline_messageignoretime);
  const mTTL = moment(tTTL).format("YYYY-MM-DDTHH:mm:ss.SSSZZ");

  // Create timestamp with +1hr
  let tCreated = moment(new Date()).format("YYYY-MM-DDTHH:mm:ss.SSSZZ");
  // tCreated.setHours(tTTL.getHours());

  const message = {
    // message_id: uuidv4(),
    message_id: id,
    device_to: device?.networkprovider_device_id,
    // device_from: { id: device?.id, name: device?.name } || null,
    // in_reply_to: null,
    created: tCreated,
    ttl: mTTL,
    frame: frames[i]?.frame,
    frames: frames[i]?.frames,
    length: calculateTotalByteSize(frames[i]?.data),
    data: frames.length > 1 ? frames[i].data : msg,
  };

  return message;
}

// Create image hex to send frames over span
function createHex(image) {
  const convertResponse = converter.convert({
    source_file: image,
    target_folder: "Commands/ImageOutput",
    target_text_filename: "picture.txt",
    target_cpp_filename: "picture",
    cpp_variable_name: "PICTURE",
    tasks: ["hexadecimal"],
    display: {
      width: 480,
      height: 768,
      fitmode: "none",
      colormode: "inverted",
    },
  });
}

function getImageFile() {
  const imgBuffer = fs.readFileSync("Commands/im.jpeg", "utf8");
  const imgHex = imgBuffer.toString("hex");
  return imgHex;
}

// function decimalToHexString(number) {
//   if (number < 0) {
//     number = 0xffffffff + number + 1;
//   }

//   return number.toString(16).toLowerCase();
// }
function decimalToHexString(dec) {
  dec = parseInt(dec, 10);
  if (!isNaN(dec)) {
    hexChars = "0123456789abcdef";
    if (dec > 255) {
      return "Out of Range";
    }
    var i = dec % 16;
    var j = (dec - i) / 16;
    result = "0x";
    result += hexChars.charAt(j) + hexChars.charAt(i);
    return result;
  } else {
    return NaN;
  }
}

// base64_encode image file
async function base64_encode_file(file) {
  const base64 = await fs.readFileSync(file, "base64");
  return base64;
}

module.exports = {
  getMothodsObjects,
  getMethodsByObjectId,
  chunk,
  createFrames,
  singleMessageStructure,
  createFramedMessageStructure,
  calculateTotalByteSize,
  createHex,
  getImageFile,
  decimalToHexString,
  base64_encode_file,
  contentStructure,
};
