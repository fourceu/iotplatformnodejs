const { sendPayloadToDevice } = require("../Span/Devices/spanDevices");
const {
  contentStructure,
  calculateTotalByteSize,
  getMethodsByObjectId,
  singleMessageStructure,
} = require("./HelperFunctions");

const sensorMethods = require("./testJsons/sensorMethods.json");

// Global variable used to search for object id
const objectName = "sensor";

/*******************************************************************************************
 * Get sensor list function
 *******************************************************************************************/
async function getSensors(device, jsonBody) {
  // Variable to look for restart method
  const get = "GET";
  // Variable to hold device ID for span
  const deviceId = device?.networkprovider_device_id;
  // Get all methods by object name
  // const methods = await getMethodsByObjectId(objectName);
  const methods = sensorMethods;
  //   Find and store the restart method object
  const mGet = methods.find((m) => m.description === get);

  if (mGet) {
    cs = contentStructure(mGet, null);
    content = {
      ...cs,
      ...jsonBody,
    };

    const messageFormat = singleMessageStructure(device, content);
    const payload = JSON.stringify(messageFormat[0]);

    // Send message over span
    const spanResponse = await sendPayloadToDevice(deviceId, payload);
    // console.log(spanResponse);

    if (spanResponse?.error) {
      return {
        error: true,
        span_response: spanResponse.errorMessage,
        message: spanResponse?.error?.message,
      };
    } else {
      return {
        span_response: spanResponse,
        payload_byte_size: calculateTotalByteSize(
          JSON.stringify(spanResponse.payload)
        ),
        payload: messageFormat[0],
      };
    }
  } else {
    return null;
  }
}

/*******************************************************************************************
 * Get sensor readout function
 *******************************************************************************************/
async function getSensorReadout(device, jsonBody) {
  // Variable to look for restart method
  const get = "GET";
  // Variable to hold device ID for span
  const deviceId = device?.networkprovider_device_id;
  // Get all methods by object name
  // const methods = await getMethodsByObjectId(objectName);
  const methods = sensorMethods;
  //   Find and store the restart method object
  const mGet = methods.find((m) => m.description === get);

  if (mGet) {
    cs = contentStructure(mGet, null);
    content = {
      ...cs,
      ...jsonBody,
    };

    const messageFormat = singleMessageStructure(device, content);
    const payload = JSON.stringify(messageFormat[0]);

    // Send message over span
    const spanResponse = await sendPayloadToDevice(deviceId, payload);

    if (spanResponse?.error) {
      return {
        error: true,
        span_response: spanResponse.errorMessage,
        message: spanResponse?.error?.message,
      };
    } else {
      return {
        span_response: spanResponse,
        payload_byte_size: calculateTotalByteSize(
          JSON.stringify(spanResponse.payload)
        ),
        payload: messageFormat[0],
      };
    }
  } else {
    return null;
  }
}

/*******************************************************************************************
 * Get sensor readout function
 *******************************************************************************************/
async function getGNSS(device, jsonBody) {
  // Variable to look for restart method
  const get = "GET";
  // Variable to hold device ID for span
  const deviceId = device?.networkprovider_device_id;
  // Get all methods by object name
  // const methods = await getMethodsByObjectId(objectName);
  const methods = sensorMethods;
  //   Find and store the restart method object
  const mGet = methods.find((m) => m.description === get);

  if (mGet) {
    cs = contentStructure(mGet, null);
    content = {
      ...cs,
      ...jsonBody,
    };

    const messageFormat = singleMessageStructure(device, content);
    const payload = JSON.stringify(messageFormat[0]);

    // Send message over span
    const spanResponse = await sendPayloadToDevice(deviceId, payload);

    if (spanResponse?.error) {
      return {
        error: true,
        span_response: spanResponse.errorMessage,
        message: spanResponse?.error?.message,
      };
    } else {
      return {
        span_response: spanResponse,
        payload_byte_size: calculateTotalByteSize(
          JSON.stringify(spanResponse.payload)
        ),
        payload: messageFormat[0],
      };
    }
  } else {
    return null;
  }
}

/*******************************************************************************************
 * Get sensor readout regularly function
 *******************************************************************************************/
async function getGNSSRegular(device, jsonBody) {
  // Variable to look for restart method
  const get = "GET";
  // Variable to hold device ID for span
  const deviceId = device?.networkprovider_device_id;
  // Get all methods by object name
  // const methods = await getMethodsByObjectId(objectName);
  const methods = sensorMethods;
  //   Find and store the restart method object
  const mGet = methods.find((m) => m.description === get);

  if (mGet) {
    cs = contentStructure(mGet, null);
    content = {
      ...cs,
      ...jsonBody,
    };

    const messageFormat = singleMessageStructure(device, content);
    const payload = JSON.stringify(messageFormat[0]);

    // Send message over span
    const spanResponse = await sendPayloadToDevice(deviceId, payload);

    if (spanResponse?.error) {
      return {
        error: true,
        span_response: spanResponse.errorMessage,
        message: spanResponse?.error?.message,
      };
    } else {
      return {
        span_response: spanResponse,
        payload_byte_size: calculateTotalByteSize(
          JSON.stringify(spanResponse.payload)
        ),
        payload: messageFormat[0],
      };
    }
  } else {
    return null;
  }
}

/*******************************************************************************************
 * Set sensor state function
 *******************************************************************************************/
async function setSensor(sensorStatus, device, jsonBody) {
  // Variable to look for restart method
  const status = sensorStatus;
  // Variable to hold device ID for span
  const deviceId = device?.networkprovider_device_id;
  // Get all methods by object name
  // const methods = await getMethodsByObjectId(objectName);
  const methods = sensorMethods;
  //   Find and store the restart method object
  const mStatus = methods.find((m) => m.description === status);

  if (mStatus) {
    cs = contentStructure(mStatus, null);
    content = {
      ...cs,
      ...jsonBody,
    };

    const messageFormat = singleMessageStructure(device, content);
    const payload = JSON.stringify(messageFormat[0]);

    // Send message over span
    const spanResponse = await sendPayloadToDevice(deviceId, payload);

    if (spanResponse?.error) {
      return {
        error: true,
        span_response: spanResponse.errorMessage,
        message: spanResponse?.error?.message,
      };
    } else {
      return {
        span_response: spanResponse,
        payload_byte_size: calculateTotalByteSize(
          JSON.stringify(spanResponse.payload)
        ),
        payload: messageFormat[0],
      };
    }
  } else {
    return false;
  }
}

module.exports = {
  getSensors,
  getSensorReadout,
  getGNSS,
  getGNSSRegular,
  setSensor,
};
