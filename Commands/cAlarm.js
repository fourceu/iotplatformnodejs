const { sendPayloadToDevice } = require("../Span/Devices/spanDevices");
const {
  contentStructure,
  getMethodsByObjectId,
  singleMessageStructure,
  calculateTotalByteSize,
} = require("./HelperFunctions");

const alarmMethods = require("./testJsons/alarmMethods.json");

// Global variable used to search for object id
const objectName = "alarm";

/*******************************************************************************************
 * Set charge alarm level function
 *******************************************************************************************/
async function setChargeAlarmLevel(device, jsonBody) {
  // Variable to look for set method
  const set = "SET";
  // Variable to hold device ID for span
  const deviceId = device?.networkprovider_device_id;
  // Get all methods by object name
  // const methods = await getMethodsByObjectId(objectName);
  const methods = alarmMethods;
  //   Find and store the restart method object
  const mSet = methods.find((m) => m.description === set);

  if (mSet) {
    cs = contentStructure(mSet, null);
    content = {
      ...cs,
      ...jsonBody,
    };

    const messageFormat = singleMessageStructure(device, content);
    const payload = JSON.stringify(messageFormat[0]);

    // Send message over span
    const spanResponse = await sendPayloadToDevice(deviceId, payload);

    if (spanResponse?.error) {
      return {
        error: true,
        span_response: spanResponse.errorMessage,
        message: spanResponse?.error?.message,
      };
    } else {
      return {
        span_response: spanResponse,
        payload_byte_size: calculateTotalByteSize(
          JSON.stringify(spanResponse.payload)
        ),
        payload: messageFormat[0],
      };
    }
  } else {
    return null;
  }
}

/*******************************************************************************************
 * Set alarm status function
 *******************************************************************************************/
async function setAlarmStatus(status, device, jsonBody) {
  // Variable to look for set method
  const setStatus = status;
  // Variable to hold device ID for span
  const deviceId = device?.networkprovider_device_id;
  // Get all methods by object name
  // const methods = await getMethodsByObjectId(objectName);
  const methods = alarmMethods;
  //   Find and store the restart method object
  const mSet = methods.find((m) => m.description === setStatus);

  if (mSet) {
    cs = contentStructure(mSet, null);
    content = {
      ...cs,
      ...jsonBody,
    };

    const messageFormat = singleMessageStructure(device, content);
    const payload = JSON.stringify(messageFormat[0]);

    // Send message over span
    const spanResponse = await sendPayloadToDevice(deviceId, payload);

    if (spanResponse?.error) {
      return {
        error: true,
        span_response: spanResponse.errorMessage,
        message: spanResponse?.error?.message,
      };
    } else {
      return {
        span_response: spanResponse,
        payload_byte_size: calculateTotalByteSize(
          JSON.stringify(spanResponse.payload)
        ),
        payload: messageFormat[0],
      };
    }
  } else {
    return null;
  }
}

module.exports = {
  setChargeAlarmLevel,
  setAlarmStatus,
};
