const express = require("express");
const { Get4CDeviceById } = require("../Devices/FourCDB/devices4C");
const router = express.Router();
const auth = require("../Middleware/auth");
const {
  resetDevice,
  sleepDevice,
  turnoffDevice,
  getDeviceInfo,
} = require("./cDevice");
const Joi = require("joi");
const {
  getDisplayContent,
  setDisplayContent,
  restartDisplay,
  flickerDisplay,
  getDisplayAP,
  setDisplayAP,
} = require("./cDisplay");
const { createHex, getImageFile } = require("./HelperFunctions");
const Jimp = require("jimp");
const {
  getSensors,
  getSensorReadout,
  getGNSS,
  getGNSSRegular,
  setSensor,
} = require("./cSensor");
const { setChargeAlarmLevel, setAlarmStatus } = require("./cAlarm");
const { getDeviceTime, setDeviceTime } = require("./cClock");

/***************************************************
 * Helper functions to validate JSON body
 ***************************************************/
function validateGetInfoBody(item) {
  const schema = Joi.object({
    imei: Joi.boolean().required(),
    imsi: Joi.boolean().required(),
    firmware: Joi.boolean().required(),
  });

  return (result = schema.validate(item));
}

function validateSetDeviceSleepBody(item) {
  const schema = Joi.object({
    for: Joi.number().label("for is required"),
    time: Joi.date().iso().label("time is required"),
  });

  return (result = schema.validate(item));
}

function validateGetDisplayContentBody(item) {
  const schema = Joi.object({
    upperleft: Joi.array().items(
      Joi.number().allow(null).label("Upper left x coordinate (int)"),
      Joi.number().allow(null).label("Upper left y coordinate (int)")
    ),
    lowerright: Joi.array().items(
      Joi.number().allow(null).label("Lower left x coordinate (int)"),
      Joi.number().allow(null).label("Lower left y coordinate (int)")
    ),
  });

  return (result = schema.validate(item));
}

function validateSetDisplayContentBody(item) {
  const schema = Joi.object({
    // dimensions: Joi.array()
    //   .items(
    //     Joi.number().required().label("width (int)"),
    //     Joi.number().required().label("height (int)")
    //   )
    //   .required(),
    upperleft: Joi.array().items(
      Joi.number().allow(null).label("upper left x coordinate (int)"),
      Joi.number().allow(null).label("upper left y coordinate (int)")
    ),
    lowerright: Joi.array().items(
      Joi.number().allow(null).label("lower right x coordinate (int)"),
      Joi.number().allow(null).label("lower right y coordinate (int)")
    ),
    bitmap: Joi.array(),
  });

  return (result = schema.validate(item));
}

function validateDisplayAttrParam(item) {
  const schema = Joi.object({
    values: Joi.string().required().label("values: Attribute/Paramter values"),
  });

  return (result = schema.validate(item));
}

function validateGetSensorReadout(item) {
  const schema = Joi.object({
    values: Joi.object({
      sensors: Joi.array().items().required().label("Sensor IDs are required."),
      repeat: Joi.number().required().label("Repeat in msec is required."),
    })
      .required()
      .label("values: Sensor array and repeat in ms"),
  });

  return (result = schema.validate(item));
}

function validateGetGnss(item) {
  const schema = Joi.object({
    values: Joi.object({
      sensors: Joi.array().items().required().label("Sensor IDs are required."),
    })
      .required()
      .label("values: Sensor array"),
  });

  return (result = schema.validate(item));
}

function validateGetGnssRegular(item) {
  const schema = Joi.object({
    values: Joi.object({
      sensors: Joi.array().items().required().label("Sensor IDs are required."),
      repeat: Joi.number().required().label("Repeat time in ms"),
    })
      .required()
      .label("values: Sensor array and repeat time in ms"),
  });

  return (result = schema.validate(item));
}

function validateSetChargeAlarmBody(item) {
  const schema = Joi.object({
    name: Joi.string().required().label("alarm name <string>"),
    sensor: Joi.string().required().label("sensor name <string>"),
    values: Joi.object({
      value: Joi.number()
        .required()
        .label("'value': <float> threshold voltage value"),
      times: Joi.number()
        .required()
        .label("'times': <int> number of times the alarm will repeat"),
      pause: Joi.number()
        .required()
        .label("'pause': <int> in seconds, interval between each message"),
      for: Joi.number()
        .required()
        .label("'for': <int> in seconds, duration of alarm trigger"),
      status: Joi.string(),
    })
      .required()
      .label("'values': alarm attributes"),
  });

  return (result = schema.validate(item));
}

function validateSetClockBody(item) {
  const schema = Joi.object({
    values: Joi.object({
      time: Joi.string().required().label("time: ISO8601 string"),
    })
      .required()
      .label("values: time "),
  });

  return (result = schema.validate(item));
}

////////////////////////////////////////////////////////////////////////////////////

/****************************************************************
 * Route to restart device
 * ***************************************************************/
router.get("/device/:id/restart", auth, async (req, res) => {
  const deviceId = req.params.id;
  if (!deviceId)
    return res
      .status(400)
      .send("Device ID is requried! Please specify the FourC Device ID.");

  // Get device by ID
  const deviceResponse = await Get4CDeviceById(deviceId);
  const device = deviceResponse.data;
  // If no device, return 404 error
  if (!device || device === null)
    return res
      .status(404)
      .send(`No device found with ID: ${deviceId}. Command cannot be sent!`);
  // If device doesn't have network provider device id return 400 error
  if (
    !device?.networkprovider_device_id ||
    device?.networkprovider_device_id === null
  )
    return res
      .status(400)
      .send(
        `Device with ID: ${deviceId} does not have any associated Network provider device id. Message cannot be sent to this device.`
      );

  const restart = await resetDevice(device);

  if (!restart)
    return res
      .status(400)
      .send("Method not found or not allowed for the device!");

  if (restart?.error) return res.status(409).send(restart);

  res.send(restart);
});

/****************************************************************
 * Route to put device to sleep
 * ***************************************************************/
router.post("/device/:id/sleep", auth, async (req, res) => {
  const forTime = req.body.for;
  const tillTime = req.body.time;
  if (!forTime && !tillTime)
    return res
      .status(400)
      .send("Sleep for<ms> or time<Sleep until time, date-time> is required.");

  const { error } = validateSetDeviceSleepBody(req.body);
  if (error) return res.status(400).send(error.details[0].message);

  const deviceId = req.params.id;
  if (!deviceId)
    return res
      .status(400)
      .send("Device ID is requried! Please specify the FourC Device ID.");

  // Get device by ID
  const deviceResponse = await Get4CDeviceById(deviceId);
  const device = deviceResponse.data;
  // If no device, return 404 error
  if (!device || device === null)
    return res
      .status(404)
      .send(`No device found with ID: ${deviceId}. Command cannot be sent!`);
  // If device doesn't have network provider device id return 400 error
  if (
    !device?.networkprovider_device_id ||
    device?.networkprovider_device_id === null
  )
    return res
      .status(400)
      .send(
        `Device with ID: ${deviceId} does not have any associated Network provider device id. Message cannot be sent to this device.`
      );

  const sleep = await sleepDevice(device, forTime, tillTime);

  if (!sleep)
    return res
      .status(400)
      .send("Method not found or not allowed for the device!");

  if (sleep?.error) return res.status(409).send(sleep);

  res.send(sleep);
});

/****************************************************************
 * Route to turn off device
 * ***************************************************************/
router.get("/device/:id/turnoff", auth, async (req, res) => {
  const deviceId = req.params.id;
  if (!deviceId)
    return res
      .status(400)
      .send("Device ID is requried! Please specify the FourC Device ID.");

  // Get device by ID
  const deviceResponse = await Get4CDeviceById(deviceId);
  const device = deviceResponse.data;
  // If no device, return 404 error
  if (!device || device === null)
    return res
      .status(404)
      .send(`No device found with ID: ${deviceId}. Command cannot be sent!`);
  // If device doesn't have network provider device id return 400 error
  if (
    !device?.networkprovider_device_id ||
    device?.networkprovider_device_id === null
  )
    return res
      .status(400)
      .send(
        `Device with ID: ${deviceId} does not have any associated Network provider device id. Message cannot be sent to this device.`
      );

  const turnoff = await turnoffDevice(device);

  if (!turnoff)
    return res
      .status(400)
      .send("Method not found or not allowed for the device!");

  if (turnoff?.error) return res.status(409).send(turnoff);

  res.send(turnoff);
});

/****************************************************************
 * Route to get device info
 * ***************************************************************/
router.post("/device/:id/getinfo", auth, async (req, res) => {
  const deviceId = req.params.id;
  if (!deviceId)
    return res
      .status(400)
      .send("Device ID is requried! Please specify the FourC Device ID.");

  const { error } = validateGetInfoBody(req.body);
  if (error) return res.status(400).send(error.details[0].message);

  // Get device by ID
  const deviceResponse = await Get4CDeviceById(deviceId);
  const device = deviceResponse.data;
  // If no device, return 404 error
  if (!device || device === null)
    return res
      .status(404)
      .send(`No device found with ID: ${deviceId}. Command cannot be sent!`);
  // If device doesn't have network provider device id return 400 error
  if (
    !device?.networkprovider_device_id ||
    device?.networkprovider_device_id === null
  )
    return res
      .status(400)
      .send(
        `Device with ID: ${deviceId} does not have any associated Network provider device id. Message cannot be sent to this device.`
      );

  const body = {
    imei: req.body.imei,
    imsi: req.body.imsi,
    firmware: req.body.firmware,
  };

  const getInfo = await getDeviceInfo(device, body);

  if (!getInfo)
    return res
      .status(400)
      .send("Method not found or not allowed for the device!");

  if (getInfo?.error) return res.status(409).send(getInfo);
  res.send(getInfo);
});

/****************************************************************
 * Route to get display content
 * ***************************************************************/
router.post("/device/:id/display/getcontent", auth, async (req, res) => {
  const deviceId = req.params.id;
  if (!deviceId)
    return res
      .status(400)
      .send("Device ID is requried! Please specify the FourC Device ID.");

  const { error } = validateGetDisplayContentBody(req.body);
  if (error) return res.status(400).send(error.details[0].message);

  // Get device by ID
  const deviceResponse = await Get4CDeviceById(deviceId);
  const device = deviceResponse.data;
  // If no device, return 404 error
  if (!device || device === null)
    return res
      .status(404)
      .send(`No device found with ID: ${deviceId}. Command cannot be sent!`);
  // If device doesn't have network provider device id return 400 error
  if (
    !device?.networkprovider_device_id ||
    device?.networkprovider_device_id === null
  )
    return res
      .status(400)
      .send(
        `Device with ID: ${deviceId} does not have any associated Network provider device id. Message cannot be sent to this device.`
      );

  const jsonBody = {
    ...req.body,
  };

  const displayContent = await getDisplayContent(device, jsonBody);

  if (!displayContent)
    return res.status(500).send("Failed to get display content");

  if (displayContent?.error) return res.status(409).send(displayContent);

  return res.send(displayContent);
});

/****************************************************************
 * Route to set display content
 * ***************************************************************/
router.post("/device/:id/display/setcontent", auth, async (req, res) => {
  const { error } = validateSetDisplayContentBody(req.body);
  if (error) return res.status(400).send(error.details[0].message);

  const deviceId = req.params.id;
  if (!deviceId)
    return res
      .status(400)
      .send("Device ID is requried! Please specify the FourC Device ID.");

  // Get device by ID
  const deviceResponse = await Get4CDeviceById(deviceId);
  const device = deviceResponse.data;
  // If no device, return 404 error
  if (!device || device === null)
    return res
      .status(404)
      .send(`No device found with ID: ${deviceId}. Command cannot be sent!`);
  // If device doesn't have network provider device id return 400 error
  if (
    !device?.networkprovider_device_id ||
    device?.networkprovider_device_id === null
  )
    return res
      .status(400)
      .send(
        `Device with ID: ${deviceId} does not have any associated Network provider device id. Message cannot be sent to this device.`
      );

  const setDC = await setDisplayContent(device, req.body);
  if (!setDC) return res.status(500).send("Failed to set display content");

  if (setDC?.error) return res.status(409).send(setDC);

  return res.send(setDC);
});

/****************************************************************
 * Route to restart/intialze display
 * ***************************************************************/
router.get("/device/:id/display/restart", auth, async (req, res) => {
  const deviceId = req.params.id;
  if (!deviceId)
    return res
      .status(400)
      .send("Device ID is requried! Please specify the FourC Device ID.");

  // Get device by ID
  const deviceResponse = await Get4CDeviceById(deviceId);
  const device = deviceResponse.data;
  // If no device, return 404 error
  if (!device || device === null)
    return res
      .status(404)
      .send(`No device found with ID: ${deviceId}. Command cannot be sent!`);
  // If device doesn't have network provider device id return 400 error
  if (
    !device?.networkprovider_device_id ||
    device?.networkprovider_device_id === null
  )
    return res
      .status(400)
      .send(
        `Device with ID: ${deviceId} does not have any associated Network provider device id. Message cannot be sent to this device.`
      );

  // if (device.display.length < 1)
  //   return res
  //     .status(400)
  //     .send(
  //       `Device with ID: ${deviceId} does not have any displays. Message cannot be sent to this device.`
  //     );

  const restart = await restartDisplay(device);

  if (!restart)
    return res
      .status(400)
      .send("Method not found or not allowed for the device!");

  if (restart?.error) return res.status(409).send(restart);

  res.send(restart);
});

/****************************************************************
 * Route to flicker display
 * ***************************************************************/
router.post("/device/:id/display/flicker", auth, async (req, res) => {
  const deviceId = req.params.id;
  if (!deviceId)
    return res
      .status(400)
      .send("Device ID is requried! Please specify the FourC Device ID.");

  const { error } = validateGetDisplayContentBody(req.body);
  if (error) return res.status(400).send(error.details[0].message);

  // Get device by ID
  const deviceResponse = await Get4CDeviceById(deviceId);
  const device = deviceResponse.data;
  // If no device, return 404 error
  if (!device || device === null)
    return res
      .status(404)
      .send(`No device found with ID: ${deviceId}. Command cannot be sent!`);
  // If device doesn't have network provider device id return 400 error
  if (
    !device?.networkprovider_device_id ||
    device?.networkprovider_device_id === null
  )
    return res
      .status(400)
      .send(
        `Device with ID: ${deviceId} does not have any associated Network provider device id. Message cannot be sent to this device.`
      );

  const jsonBody = {
    ...req.body,
  };

  const displayFlicker = await flickerDisplay(device, jsonBody);

  if (!displayFlicker)
    return res.status(500).send("Failed to flicker/refresh display");

  if (displayFlicker?.error) return res.status(409).send(displayFlicker);

  return res.send(displayFlicker);
});

/****************************************************************
 * Route to get display attribute / parameters
 * ***************************************************************/
router.post("/device/:id/display/get_attr_param", auth, async (req, res) => {
  const deviceId = req.params.id;
  if (!deviceId)
    return res
      .status(400)
      .send("Device ID is requried! Please specify the FourC Device ID.");

  const { error } = validateDisplayAttrParam(req.body);
  if (error) return res.status(400).send(error.details[0].message);

  // Get device by ID
  const deviceResponse = await Get4CDeviceById(deviceId);
  const device = deviceResponse.data;
  // If no device, return 404 error
  if (!device || device === null)
    return res
      .status(404)
      .send(`No device found with ID: ${deviceId}. Command cannot be sent!`);
  // If device doesn't have network provider device id return 400 error
  if (
    !device?.networkprovider_device_id ||
    device?.networkprovider_device_id === null
  )
    return res
      .status(400)
      .send(
        `Device with ID: ${deviceId} does not have any associated Network provider device id. Message cannot be sent to this device.`
      );

  const jsonBody = {
    ...req.body,
  };

  const getdisplayAP = await getDisplayAP(device, jsonBody);

  if (!getdisplayAP)
    return res.status(500).send("Failed to flicker/refresh display");

  if (getdisplayAP?.error) return res.status(409).send(getdisplayAP);

  return res.send(getdisplayAP);
});

/****************************************************************
 * Route to set display attribute / parameters
 * ***************************************************************/
router.post("/device/:id/display/set_attr_param", auth, async (req, res) => {
  const deviceId = req.params.id;
  if (!deviceId)
    return res
      .status(400)
      .send("Device ID is requried! Please specify the FourC Device ID.");

  const { error } = validateDisplayAttrParam(req.body);
  if (error) return res.status(400).send(error.details[0].message);

  // Get device by ID
  const deviceResponse = await Get4CDeviceById(deviceId);
  const device = deviceResponse.data;
  // If no device, return 404 error
  if (!device || device === null)
    return res
      .status(404)
      .send(`No device found with ID: ${deviceId}. Command cannot be sent!`);
  // If device doesn't have network provider device id return 400 error
  if (
    !device?.networkprovider_device_id ||
    device?.networkprovider_device_id === null
  )
    return res
      .status(400)
      .send(
        `Device with ID: ${deviceId} does not have any associated Network provider device id. Message cannot be sent to this device.`
      );

  const jsonBody = {
    ...req.body,
  };

  const setdisplayAP = await setDisplayAP(device, jsonBody);

  if (!setdisplayAP)
    return res.status(500).send("Failed to flicker/refresh display");

  if (setdisplayAP?.error) return res.status(409).send(setdisplayAP);

  return res.send(setdisplayAP);
});

/****************************************************************
 * Route to get sensors list
 * ***************************************************************/
router.post("/device/:id/sensor/getdetails", auth, async (req, res) => {
  const deviceId = req.params.id;
  if (!deviceId)
    return res
      .status(400)
      .send("Device ID is requried! Please specify the FourC Device ID.");

  // Get device by ID
  const deviceResponse = await Get4CDeviceById(deviceId);
  const device = deviceResponse.data;
  // If no device, return 404 error
  if (!device || device === null)
    return res
      .status(404)
      .send(`No device found with ID: ${deviceId}. Command cannot be sent!`);
  // If device doesn't have network provider device id return 400 error
  if (
    !device?.networkprovider_device_id ||
    device?.networkprovider_device_id === null
  )
    return res
      .status(400)
      .send(
        `Device with ID: ${deviceId} does not have any associated Network provider device id. Message cannot be sent to this device.`
      );

  const jsonBody = {
    ...req.body,
  };

  const getSensor = await getSensors(device, jsonBody);

  if (!getSensor)
    return res.status(500).send("Failed to get sensor list from device");

  if (getSensor?.error) return res.status(409).send(getSensor);

  return res.send(getSensor);
});

/****************************************************************
 * Route to get sensors readout
 * ***************************************************************/
router.post("/device/:id/sensor/getregular", auth, async (req, res) => {
  const deviceId = req.params.id;
  if (!deviceId)
    return res
      .status(400)
      .send("Device ID is requried! Please specify the FourC Device ID.");

  const { error } = validateGetSensorReadout(req.body);
  if (error) return res.status(400).send(error.details[0].message);

  // Get device by ID
  const deviceResponse = await Get4CDeviceById(deviceId);
  const device = deviceResponse.data;
  // If no device, return 404 error
  if (!device || device === null)
    return res
      .status(404)
      .send(`No device found with ID: ${deviceId}. Command cannot be sent!`);
  // If device doesn't have network provider device id return 400 error
  if (
    !device?.networkprovider_device_id ||
    device?.networkprovider_device_id === null
  )
    return res
      .status(400)
      .send(
        `Device with ID: ${deviceId} does not have any associated Network provider device id. Message cannot be sent to this device.`
      );

  const jsonBody = {
    ...req.body,
  };

  const getReadout = await getSensorReadout(device, jsonBody);

  if (!getReadout)
    return res.status(500).send("Failed to get sensor readouts from device");

  if (getReadout?.error) return res.status(409).send(getReadout);

  return res.send(getReadout);
});

/****************************************************************
 * Route to get GNSS readout
 * ***************************************************************/
router.post("/device/:id/sensor/getgnss", auth, async (req, res) => {
  const deviceId = req.params.id;
  if (!deviceId)
    return res
      .status(400)
      .send("Device ID is requried! Please specify the FourC Device ID.");

  const { error } = validateGetGnss(req.body);
  if (error) return res.status(400).send(error.details[0].message);

  // Get device by ID
  const deviceResponse = await Get4CDeviceById(deviceId);
  const device = deviceResponse.data;
  // If no device, return 404 error
  if (!device || device === null)
    return res
      .status(404)
      .send(`No device found with ID: ${deviceId}. Command cannot be sent!`);
  // If device doesn't have network provider device id return 400 error
  if (
    !device?.networkprovider_device_id ||
    device?.networkprovider_device_id === null
  )
    return res
      .status(400)
      .send(
        `Device with ID: ${deviceId} does not have any associated Network provider device id. Message cannot be sent to this device.`
      );

  const jsonBody = {
    ...req.body,
  };

  const gnss = await getGNSS(device, jsonBody);

  if (!gnss)
    return res.status(500).send("Failed to get gnss values from device");

  if (gnss?.error) return res.status(409).send(gnss);

  return res.send(gnss);
});

/****************************************************************
 * Route to get GNSS readout regularly
 * ***************************************************************/
router.post("/device/:id/sensor/getgnssregular", auth, async (req, res) => {
  const deviceId = req.params.id;
  if (!deviceId)
    return res
      .status(400)
      .send("Device ID is requried! Please specify the FourC Device ID.");

  const { error } = validateGetGnssRegular(req.body);
  if (error) return res.status(400).send(error.details[0].message);

  // Get device by ID
  const deviceResponse = await Get4CDeviceById(deviceId);
  const device = deviceResponse.data;
  // If no device, return 404 error
  if (!device || device === null)
    return res
      .status(404)
      .send(`No device found with ID: ${deviceId}. Command cannot be sent!`);
  // If device doesn't have network provider device id return 400 error
  if (
    !device?.networkprovider_device_id ||
    device?.networkprovider_device_id === null
  )
    return res
      .status(400)
      .send(
        `Device with ID: ${deviceId} does not have any associated Network provider device id. Message cannot be sent to this device.`
      );

  const jsonBody = {
    ...req.body,
  };

  const gnssRegular = await getGNSSRegular(device, jsonBody);

  if (!gnssRegular)
    return res
      .status(500)
      .send("Failed to get gnss values regularly from device");

  if (gnssRegular?.error) return res.status(409).send(gnssRegular);

  return res.send(gnssRegular);
});

/****************************************************************
 * Route to set Sensor status
 * ***************************************************************/
router.post("/device/:id/sensor/:status", auth, async (req, res) => {
  const deviceId = req.params.id;
  const status = req.params.status;
  if (!deviceId)
    return res
      .status(400)
      .send("Device ID is requried! Please specify the FourC Device ID.");

  const { error } = validateGetGnss(req.body);
  if (error) return res.status(400).send(error.details[0].message);

  // Get device by ID
  const deviceResponse = await Get4CDeviceById(deviceId);
  const device = deviceResponse.data;
  // If no device, return 404 error
  if (!device || device === null)
    return res
      .status(404)
      .send(`No device found with ID: ${deviceId}. Command cannot be sent!`);
  // If device doesn't have network provider device id return 400 error
  if (
    !device?.networkprovider_device_id ||
    device?.networkprovider_device_id === null
  )
    return res
      .status(400)
      .send(
        `Device with ID: ${deviceId} does not have any associated Network provider device id. Message cannot be sent to this device.`
      );

  const jsonBody = {
    ...req.body,
  };

  const sensorStatus = await setSensor(status, device, jsonBody);

  if (!sensorStatus)
    return res
      .status(404)
      .send("Method not found! Method name: TURNON /TURNOFF");

  if (sensorStatus?.error) return res.status(409).send(sensorStatus);

  return res.send(sensorStatus);
});

/****************************************************************
 * Route to get battery charge level
 * ***************************************************************/
router.post("/device/:id/battery/getlevel", auth, async (req, res) => {
  const deviceId = req.params.id;
  if (!deviceId)
    return res
      .status(400)
      .send("Device ID is requried! Please specify the FourC Device ID.");

  // const { error } = validateGetGnss(req.body);
  // if (error) return res.status(400).send(error.details[0].message);

  // Get device by ID
  const deviceResponse = await Get4CDeviceById(deviceId);
  const device = deviceResponse.data;
  // If no device, return 404 error
  if (!device || device === null)
    return res
      .status(404)
      .send(`No device found with ID: ${deviceId}. Command cannot be sent!`);
  // If device doesn't have network provider device id return 400 error
  if (
    !device?.networkprovider_device_id ||
    device?.networkprovider_device_id === null
  )
    return res
      .status(400)
      .send(
        `Device with ID: ${deviceId} does not have any associated Network provider device id. Message cannot be sent to this device.`
      );

  const jsonBody = {
    ...req.body,
  };

  const getBat = await getSensors(device, jsonBody);

  if (!getBat)
    return res
      .status(500)
      .send("Failed to get battery charge level from device");

  if (getBat?.error) return res.status(409).send(getBat);

  return res.send(getBat);
});

/****************************************************************
 * Route to set battery charge alarm level
 * ***************************************************************/
router.post("/device/:id/alarm/setlevel", auth, async (req, res) => {
  const deviceId = req.params.id;
  if (!deviceId)
    return res
      .status(400)
      .send("Device ID is requried! Please specify the FourC Device ID.");

  const { error } = validateSetChargeAlarmBody(req.body);
  if (error) return res.status(400).send(error.details[0].message);

  // Get device by ID
  const deviceResponse = await Get4CDeviceById(deviceId);
  const device = deviceResponse.data;
  // If no device, return 404 error
  if (!device || device === null)
    return res
      .status(404)
      .send(`No device found with ID: ${deviceId}. Command cannot be sent!`);
  // If device doesn't have network provider device id return 400 error
  if (
    !device?.networkprovider_device_id ||
    device?.networkprovider_device_id === null
  )
    return res
      .status(400)
      .send(
        `Device with ID: ${deviceId} does not have any associated Network provider device id. Message cannot be sent to this device.`
      );

  const jsonBody = {
    name: "Low battery alarm",
    compare: "LT",
    ...req.body,
  };

  const setChargeAlarm = await setChargeAlarmLevel(device, jsonBody);

  if (!setChargeAlarm)
    return res
      .status(500)
      .send("Failed to set battery charge level alarm from device");

  if (setChargeAlarm?.error) return res.status(409).send(setChargeAlarm);

  return res.send(setChargeAlarm);
});

/****************************************************************
 * Route to set Alarm status
 * ***************************************************************/
router.get("/device/:id/alarm/:status", auth, async (req, res) => {
  const deviceId = req.params.id;
  const status = req.params.status;
  if (!deviceId)
    return res
      .status(400)
      .send("Device ID is requried! Please specify the FourC Device ID.");

  // Get device by ID
  const deviceResponse = await Get4CDeviceById(deviceId);
  const device = deviceResponse.data;
  // If no device, return 404 error
  if (!device || device === null)
    return res
      .status(404)
      .send(`No device found with ID: ${deviceId}. Command cannot be sent!`);
  // If device doesn't have network provider device id return 400 error
  if (
    !device?.networkprovider_device_id ||
    device?.networkprovider_device_id === null
  )
    return res
      .status(400)
      .send(
        `Device with ID: ${deviceId} does not have any associated Network provider device id. Message cannot be sent to this device.`
      );

  const jsonBody = {
    values: {
      name: "Low battery alarm",
    },
  };

  const alarmStatus = await setAlarmStatus(status, device, jsonBody);

  if (!alarmStatus)
    return res
      .status(404)
      .send("Method not found! Method name: TURNON /TURNOFF");

  if (alarmStatus?.error) return res.status(409).send(alarmStatus);

  return res.send(alarmStatus);
});

/****************************************************************
 * Route to get device time
 * ***************************************************************/
router.get("/device/:id/clock/get", auth, async (req, res) => {
  const deviceId = req.params.id;
  if (!deviceId)
    return res
      .status(400)
      .send("Device ID is requried! Please specify the FourC Device ID.");

  // Get device by ID
  const deviceResponse = await Get4CDeviceById(deviceId);
  const device = deviceResponse.data;
  // If no device, return 404 error
  if (!device || device === null)
    return res
      .status(404)
      .send(`No device found with ID: ${deviceId}. Command cannot be sent!`);
  // If device doesn't have network provider device id return 400 error
  if (
    !device?.networkprovider_device_id ||
    device?.networkprovider_device_id === null
  )
    return res
      .status(400)
      .send(
        `Device with ID: ${deviceId} does not have any associated Network provider device id. Message cannot be sent to this device.`
      );

  const getTime = await getDeviceTime(device);

  if (!getTime) return res.status(500).send("Failed to get time from device");

  if (getTime?.error) return res.status(409).send(getTime);

  return res.send(getTime);
});

/****************************************************************
 * Route to set current device time
 * ***************************************************************/
router.post("/device/:id/clock/settime", auth, async (req, res) => {
  const deviceId = req.params.id;
  const status = req.params.status;
  if (!deviceId)
    return res
      .status(400)
      .send("Device ID is requried! Please specify the FourC Device ID.");

  const { error } = validateSetClockBody(req.body);
  if (error) return res.status(400).send(error.details[0].message);

  // Get device by ID
  const deviceResponse = await Get4CDeviceById(deviceId);
  const device = deviceResponse.data;
  // If no device, return 404 error
  if (!device || device === null)
    return res
      .status(404)
      .send(`No device found with ID: ${deviceId}. Command cannot be sent!`);
  // If device doesn't have network provider device id return 400 error
  if (
    !device?.networkprovider_device_id ||
    device?.networkprovider_device_id === null
  )
    return res
      .status(400)
      .send(
        `Device with ID: ${deviceId} does not have any associated Network provider device id. Message cannot be sent to this device.`
      );

  const jsonBody = {
    ...req.body,
  };

  const setTime = await setDeviceTime(device, jsonBody);

  if (!setTime)
    return res.status(404).send("Failed to set current time to device");

  if (setTime?.error) return res.status(409).send(setTime);

  return res.send(setTime);
});

module.exports = router;
