/*
 * TODO
 ***** Set a global variable for device object name
 ***** Get all methods
 ***** Find the object id for device and store in variable
 ***** Get all methods by the object id
 ***** Store the methods in a global array
 ***** Create mesasge structure based on message protocol document
 ***** Check if total message is over 512 bytes
 ***** If more than 512 break mesasge into frames (Global helper function)
 ***** Send the payload(s) over span
 ***** If span response status is 200 send next frame or break
 */
/***********************************************************
 * Restart a device function
 ************************************************************/

const { sendPayloadToDevice } = require("../Span/Devices/spanDevices");
const {
  contentStructure,
  getMethodsByObjectId,
  singleMessageStructure,
  calculateTotalByteSize,
} = require("./HelperFunctions");
const methodsArray = require("./testJsons/deviceMethods.json");

// Global variable used to search for object id
const objectName = "device";

/*******************************************************************************************
 * Device restart/reset function
 *******************************************************************************************/
async function resetDevice(device) {
  // Variable to look for restart method
  const restart = "RESTART";
  // Variable to hold device ID for span
  const deviceId = device?.networkprovider_device_id;
  // Get all methods by object name
  // const methods = await getMethodsByObjectId(objectName);
  const methods = methodsArray;
  //   Find and store the restart method object
  const mRestart = methods.find((m) => m.description === restart);

  if (mRestart) {
    content = contentStructure(mRestart, null);

    const messageFormat = singleMessageStructure(device, content);
    const payload = JSON.stringify(messageFormat[0]);

    // Send message over span
    const spanResponse = await sendPayloadToDevice(deviceId, payload);
    // console.log(spanResponse)

    if (spanResponse?.error) {
      return {
        error: true,
        span_response: spanResponse.errorMessage,
        message: spanResponse?.error?.message,
      };
    } else {
      return {
        span_response: spanResponse,
        payload_byte_size: calculateTotalByteSize(
          JSON.stringify(spanResponse.payload)
        ),
        payload: messageFormat[0],
      };
    }
  } else {
    return null;
  }
}

/*******************************************************************************************
 * Device sleep function
 *******************************************************************************************/
async function sleepDevice(device, forTime, tillTime) {
  // Variable to look for sleep method
  const sleep = "SLEEP";
  // Variable to hold device ID for span
  const deviceId = device?.networkprovider_device_id;
  // Get all methods by object name
  // const methods = await getMethodsByObjectId(objectName);
  const methods = methodsArray;
  //   Find and store the restart method object
  const mSleep = methods.find((m) => m.description === sleep);

  if (mSleep) {
    let content = contentStructure(mSleep, null);

    // Create content based on for or time in request body
    if (forTime) {
      content = { ...content, values: { for: forTime } };
    } else if (tillTime) {
      content = { ...content, values: { time: tillTime } };
    }

    const messageFormat = singleMessageStructure(device, content);
    const payload = JSON.stringify(messageFormat[0]);

    // Send message over span
    const spanResponse = await sendPayloadToDevice(deviceId, payload);

    if (spanResponse?.error) {
      return {
        error: true,
        span_response: spanResponse.errorMessage,
        message: spanResponse?.error?.message,
      };
    } else {
      return {
        span_response: spanResponse,
        payload_byte_size: calculateTotalByteSize(
          JSON.stringify(spanResponse.payload)
        ),
        payload: messageFormat[0],
      };
    }
  } else {
    return null;
  }
}

/*******************************************************************************************
 * Device turn off function
 *******************************************************************************************/
async function turnoffDevice(device) {
  // Variable to look for restart method
  const turnoff = "TURNOFF";
  // Variable to hold device ID for span
  const deviceId = device?.networkprovider_device_id;
  // Get all methods by object name
  // const methods = await getMethodsByObjectId(objectName);
  const methods = methodsArray;
  //   Find and store the restart method object
  const mTurnOff = methods.find((m) => m.description === turnoff);

  if (mTurnOff) {
    content = contentStructure(mTurnOff, null);

    const messageFormat = singleMessageStructure(device, content);
    const payload = JSON.stringify(messageFormat[0]);

    // Send message over span
    const spanResponse = await sendPayloadToDevice(deviceId, payload);

    if (spanResponse?.error) {
      return {
        error: true,
        span_response: spanResponse.errorMessage,
        message: spanResponse?.error?.message,
      };
    } else {
      return {
        span_response: spanResponse,
        payload_byte_size: calculateTotalByteSize(
          JSON.stringify(spanResponse.payload)
        ),
        payload: messageFormat[0],
      };
    }
  } else {
    return null;
  }
}

/*******************************************************************************************
 * Get device info function
 *******************************************************************************************/
async function getDeviceInfo(device, jsonBody) {
  // Variable to look for restart method
  const getInfo = "GET";
  // Variable to hold device ID for span
  const deviceId = device?.networkprovider_device_id;
  // Get all methods by object name
  // const methods = await getMethodsByObjectId(objectName);
  const methods = methodsArray;
  // console.log(methods);
  //   Find and store the restart method object
  const mGet = methods.find((m) => m.description === getInfo);

  if (mGet) {
    content = contentStructure(mGet, jsonBody);

    const messageFormat = singleMessageStructure(device, content);
    const payload = JSON.stringify(messageFormat[0]);

    // Send message over span
    const spanResponse = await sendPayloadToDevice(deviceId, payload);

    if (spanResponse?.error) {
      return {
        error: true,
        span_response: spanResponse.errorMessage,
        message: spanResponse?.error?.message,
      };
    } else {
      return {
        span_response: spanResponse,
        payload_byte_size: calculateTotalByteSize(
          JSON.stringify(spanResponse.payload)
        ),
        payload: messageFormat[0],
      };
    }
  } else {
    return null;
  }
}

module.exports = {
  resetDevice,
  sleepDevice,
  turnoffDevice,
  getDeviceInfo,
};
