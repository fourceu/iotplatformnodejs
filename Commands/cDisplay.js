const fs = require("fs");
const path = require("path");
const Jimp = require("jimp");
const { sendPayloadToDevice } = require("../Span/Devices/spanDevices");
const {
  getMethodsByObjectId,
  singleMessageStructure,
  decimalToHexString,
  calculateTotalByteSize,
  chunk,
  createFrames,
  createFramedMessageStructure,
  base64_encode_file,
  contentStructure,
} = require("./HelperFunctions");
const displayMethods = require("./testJsons/displayMethods.json");
const uuidv4 = require("uuid");
const getPixels = require("get-pixels");

// Global variable used to search for object id
const objectName = "display";

// function to encode file data to base64 encoded string
function base64_encode(file) {
  // read binary data
  var bitmap = fs.readFileSync(file);
  // convert binary data to base64 encoded string
  return new Buffer(bitmap).toString("hex");
}

/*******************************************************************************************
 * Get display content function
 *******************************************************************************************/
async function getDisplayContent(device, jsonBody) {
  // Variable to look for restart method
  const get = "GET";
  // Variable to hold device ID for span
  const deviceId = device?.networkprovider_device_id;
  // Get all methods by object name
  // const methods = await getMethodsByObjectId(objectName);
  const methods = displayMethods;
  //   Find and store the restart method object
  const mGetDisplayContent = methods.find((m) => m.description === get);

  if (mGetDisplayContent) {
    content = contentStructure(mGetDisplayContent, jsonBody);

    const messageFormat = singleMessageStructure(device, content);
    const payload = JSON.stringify(messageFormat[0]);

    // Send message over span
    const spanResponse = await sendPayloadToDevice(deviceId, payload);
    // const spanResponse = true;

    if (spanResponse?.error) {
      return {
        error: true,
        span_response: spanResponse.errorMessage,
        message: spanResponse?.error?.message,
      };
    } else {
      return {
        span_response: spanResponse,
        payload_byte_size: calculateTotalByteSize(
          JSON.stringify(spanResponse.payload)
        ),
        payload: messageFormat[0],
      };
    }
  } else {
    return null;
  }
}

/*******************************************************************************************
 * Set display content function
 *******************************************************************************************/
async function setDisplayContent(device, jsonBody) {
  /*
  // read handle image with jimp
  // let fileFormat = path.extname("Commands/im.png");
  // console.log(fileFormat);
  const img = await Jimp.read("Commands/im.png");
  // await img.resize(480, 768);
  // resize image to certain width x height
  await img.resize(jsonBody.dimensions[0], jsonBody.dimensions[1]);
  // convert image to greyscale
  await img.greyscale();
  // write image to file for debugging
  await img.write("Commands/im_resized.png");

  // Convert image to buffer with JIMP
  let imgBuffer = [];
  let buff;
  img.getBuffer(Jimp.AUTO, (err, buffer) => {
    // console.log(buffer.length);
    // console.log(buffer);
    buff = Buffer.from(buffer, "hex");
    buffer.forEach((element) => {
      let hexString = decimalToHexString(element);
      imgBuffer.push(hexString);
    });
  });

  // Write bitmap array to file for debugging
  const path = "Commands/outBuffer.txt";
  fs.writeFileSync(path, JSON.stringify(imgBuffer));
  const jsonBuffer = buff.toJSON();
  const bufferData = jsonBuffer.data;

  // Convert image to base64encode
  const b64e_img = await base64_encode_file("Commands/im_resized.png");
  const b64e_img_string = JSON.stringify(bufferData);
  // console.log(b64e_img);

  ///////////////////////////////////// ImageMAGIK Secion /////////////////////////////////////
  const imFile = "Commands/im.png";
  var pixels = [];
  const tstIm = await Jimp.read(imFile);
  tstIm.resize(jsonBody.dimensions[0], jsonBody.dimensions[1]);
  tstIm.greyscale().contrast(1);
  tstIm.write("Commands/Test_Image_BW.png");
  var width = tstIm.bitmap.width;
  var height = tstIm.bitmap.height;
  console.log(`Width: ${width}`);
  console.log(`Height: ${height}`);
  for (var y = 0; y < height; y++) {
    var rowPixels = [];
    var bitCounter = 0;
    var byteEval = 0;

    for (var x = 0; x < width; x++) {
      var pixel;
      var color = Jimp.intToRGBA(tstIm.getPixelColor(x, y));
      if (color.r === 0 && color.g === 0 && color.b === 0) {
        pixel = 1;
      } else if (color.r === 255 && color.g === 255 && color.b === 255) {
        pixel = 0;
      } else {
        console.log("Image is not monochrome!");
        return;
      }
      byteEval += pixel << (7 - bitCounter);
      bitCounter++;
      if (bitCounter >= 8) {
        rowPixels.push(byteEval);
        bitCounter = 0;
        byteEval = 0;
      }
    }

    if (bitCounter > 0) {
      rowPixels.push(byteEval);
    }
    pixels.push(rowPixels);
  }

  var imgPixelArr = pixels.flat();
  console.log(`Pixel Length: ${imgPixelArr.length}`);

  /////////////////////////////////////////////////////////////////////////////////////////////

  */

  // Variable to look for restart method
  const set = "SET";
  // Variable to hold device ID for span
  const deviceId = device?.networkprovider_device_id;
  // Get all methods by object name
  // const methods = await getMethodsByObjectId(objectName);
  const methods = displayMethods;
  //   Find and store the restart method object
  const mSetDisplayContent = methods.find((m) => m.description === set);

  if (mSetDisplayContent) {
    // content = contentStructure(mSetDisplayContent, {
    //   ...jsonBody,
    //   bitmap: b64e_img_string,
    // });
    // content = b64e_img_string;
    content = {
      object: mSetDisplayContent?.objecttype?.id,
      method: mSetDisplayContent?.id,
      values: {
        // ...jsonBody,
        // bitmap: b64e_img_string,
        // bitmap: imgPixelArr,
        upperleft: jsonBody.upperleft,
        lowerright: jsonBody.lowerright,
        bitmap: jsonBody.bitmap,
        // bitmap: [
        //   255, 0, 0, 255, 255, 0, 0, 255, 255, 0, 0, 255, 255, 0, 0, 255, 255,
        //   255, 255, 255, 0, 0, 0, 255, 0, 0, 0, 255, 0, 0, 0, 255,
        // ],
      },
    };

    // console.log(content);
    // console.log("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
    // console.log(jsonBody.bitmap);
    // console.log("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");

    // TODO:
    // JSON stringify content
    const contentString = JSON.stringify(content);
    // Get the content byte size
    const size = calculateTotalByteSize(contentString);

    // variables to hold chunks, frames and message arrays
    let chunks,
      frames,
      messages = [];
    // If content byte size > 512 break the content into small chunks
    if (size > 512) {
      // Set chunk size to 256 bytes
      chunks = chunk(contentString, 256);
      frames = createFrames(chunks);
      const messageId = uuidv4();

      for (let i = 0; i < frames.length; i++) {
        let message = createFramedMessageStructure(
          i,
          device,
          frames,
          null,
          mSetDisplayContent,
          messageId,
          jsonBody,
          "png"
        );

        // messages.push(message);

        const spanPayload = JSON.stringify(message);
        // Send message using span
        const spanResponse = await sendPayloadToDevice(deviceId, spanPayload);
        if (spanResponse?.error) {
          return {
            error: true,
            span_response: spanResponse.errorMessage,
            message: spanResponse?.error?.message,
          };
        } else {
          if (spanResponse) {
            messages.push({
              span_response: spanResponse,
              b64_message_size: calculateTotalByteSize(
                JSON.stringify(spanResponse.payload)
              ),
              original_message_size: calculateTotalByteSize(
                JSON.stringify(message)
              ),
              original_message: message,
            });
          } else {
            console.log("Error sending messages over span");
          }
        }
      }

      return messages;
    } else {
      // Else create single message structure and send over span
      let message = singleMessageStructure(device, content);

      // messages.push(message);

      const spanPayload = JSON.stringify(message[0]);
      // // Send message using span
      const spanResponse = await sendPayloadToDevice(deviceId, spanPayload);
      if (spanResponse?.error) {
        return {
          error: true,
          span_response: spanResponse.errorMessage,
          message: spanResponse?.error?.message,
        };
      } else {
        if (spanResponse) {
          messages.push({
            span_response: spanResponse,
            b64_message_size: calculateTotalByteSize(
              JSON.stringify(spanResponse.payload)
            ),
            original_message_size: calculateTotalByteSize(
              JSON.stringify(message)
            ),
            original_message: message,
          });
        } else {
          console.log("Error sending messages over span");
        }
      }

      return messages;
    }
  } else {
    return null;
  }
}

/*******************************************************************************************
 * Restart/initialize display function
 *******************************************************************************************/
async function restartDisplay(device) {
  // Variable to look for restart method
  const restart = "RESTART";
  // Variable to hold device ID for span
  const deviceId = device?.networkprovider_device_id;
  // Get all methods by object name
  // const methods = await getMethodsByObjectId(objectName);
  const methods = displayMethods;
  //   Find and store the restart method object
  const mRestartDisplay = methods.find((m) => m.description === restart);

  if (mRestartDisplay) {
    content = contentStructure(mRestartDisplay, null);

    const messageFormat = singleMessageStructure(device, content);
    const payload = JSON.stringify(messageFormat[0]);

    // Send message over span
    const spanResponse = await sendPayloadToDevice(deviceId, payload);
    // const spanResponse = true;

    if (spanResponse?.error) {
      return {
        error: true,
        span_response: spanResponse.errorMessage,
        message: spanResponse?.error?.message,
      };
    } else {
      return {
        span_response: spanResponse,
        payload_byte_size: calculateTotalByteSize(
          JSON.stringify(spanResponse.payload)
        ),
        payload: messageFormat[0],
      };
    }
  } else {
    return null;
  }
}

/*******************************************************************************************
 * Flicker display function
 *******************************************************************************************/
async function flickerDisplay(device, jsonBody) {
  // Variable to look for restart method
  const flicker = "REFRESH";
  // Variable to hold device ID for span
  const deviceId = device?.networkprovider_device_id;
  // Get all methods by object name
  // const methods = await getMethodsByObjectId(objectName);
  const methods = displayMethods;
  //   Find and store the restart method object
  const mFlickerDisplay = methods.find((m) => m.description === flicker);

  if (mFlickerDisplay) {
    content = contentStructure(mFlickerDisplay, jsonBody);

    const messageFormat = singleMessageStructure(device, content);
    const payload = JSON.stringify(messageFormat[0]);

    // Send message over span
    const spanResponse = await sendPayloadToDevice(deviceId, payload);
    // const spanResponse = true;

    if (spanResponse?.error) {
      return {
        error: true,
        span_response: spanResponse.errorMessage,
        message: spanResponse?.error?.message,
      };
    } else {
      return {
        span_response: spanResponse,
        payload_byte_size: calculateTotalByteSize(
          JSON.stringify(spanResponse.payload)
        ),
        payload: messageFormat[0],
      };
    }
  } else {
    return null;
  }
}

/*******************************************************************************************
 * Get display attributes and parameters function
 *******************************************************************************************/
async function getDisplayAP(device, jsonBody) {
  // Variable to look for restart method
  const get = "GET";
  // Variable to hold device ID for span
  const deviceId = device?.networkprovider_device_id;
  // Get all methods by object name
  // const methods = await getMethodsByObjectId(objectName);
  const methods = displayMethods;
  //   Find and store the restart method object
  const mGetDisplayAP = methods.find((m) => m.description === get);

  if (mGetDisplayAP) {
    // TODO: Find what values to get
    content = contentStructure(mGetDisplayAP, jsonBody);

    const messageFormat = singleMessageStructure(device, content);
    const payload = JSON.stringify(messageFormat[0]);

    // Send message over span
    const spanResponse = await sendPayloadToDevice(deviceId, payload);
    // const spanResponse = true;

    if (spanResponse?.error) {
      return {
        error: true,
        span_response: spanResponse.errorMessage,
        message: spanResponse?.error?.message,
      };
    } else {
      return {
        span_response: spanResponse,
        payload_byte_size: calculateTotalByteSize(
          JSON.stringify(spanResponse.payload)
        ),
        payload: messageFormat[0],
      };
    }
  } else {
    return null;
  }
}

/*******************************************************************************************
 * Set display attributes and parameters function
 *******************************************************************************************/
async function setDisplayAP(device, jsonBody) {
  // Variable to look for restart method
  const set = "SET";
  // Variable to hold device ID for span
  const deviceId = device?.networkprovider_device_id;
  // Get all methods by object name
  // const methods = await getMethodsByObjectId(objectName);
  const methods = displayMethods;
  //   Find and store the restart method object
  const mSetDisplayAP = methods.find((m) => m.description === set);

  if (mSetDisplayAP) {
    // TODO: Find what values to get
    content = contentStructure(mSetDisplayAP, jsonBody);

    const messageFormat = singleMessageStructure(device, content);
    const payload = JSON.stringify(messageFormat[0]);

    // Send message over span
    const spanResponse = await sendPayloadToDevice(deviceId, payload);
    // const spanResponse = true;

    if (spanResponse?.error) {
      return {
        error: true,
        span_response: spanResponse.errorMessage,
        message: spanResponse?.error?.message,
      };
    } else {
      return {
        span_response: spanResponse,
        payload_byte_size: calculateTotalByteSize(
          JSON.stringify(spanResponse.payload)
        ),
        payload: messageFormat[0],
      };
    }
  } else {
    return null;
  }
}

module.exports = {
  getDisplayContent,
  setDisplayContent,
  restartDisplay,
  flickerDisplay,
  getDisplayAP,
  setDisplayAP,
};
