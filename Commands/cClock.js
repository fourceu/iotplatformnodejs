const { sendPayloadToDevice } = require("../Span/Devices/spanDevices");
const {
  contentStructure,
  getMethodsByObjectId,
  singleMessageStructure,
  calculateTotalByteSize,
} = require("./HelperFunctions");

const clockMethods = require("./testJsons/clockMethods.json");

// Global variable used to search for object id
const objectName = "clock";

/*******************************************************************************************
 * Get current device time function
 *******************************************************************************************/
async function getDeviceTime(device) {
  // Variable to look for set method
  const get = "GET";
  // Variable to hold device ID for span
  const deviceId = device?.networkprovider_device_id;
  // Get all methods by object name
  // const methods = await getMethodsByObjectId(objectName);
  const methods = clockMethods;
  //   Find and store the restart method object
  const mGet = methods.find((m) => m.description === get);

  if (mGet) {
    content = contentStructure(mGet, null);

    const messageFormat = singleMessageStructure(device, content);
    const payload = JSON.stringify(messageFormat[0]);

    // Send message over span
    const spanResponse = await sendPayloadToDevice(deviceId, payload);

    if (spanResponse?.error) {
      return {
        error: true,
        span_response: spanResponse.errorMessage,
        message: spanResponse?.error?.message,
      };
    } else {
      return {
        span_response: spanResponse,
        payload_byte_size: calculateTotalByteSize(
          JSON.stringify(spanResponse.payload)
        ),
        payload: messageFormat[0],
      };
    }
  } else {
    return null;
  }
}

/*******************************************************************************************
 * Set current device time function
 *******************************************************************************************/
async function setDeviceTime(device, jsonBody) {
  // Variable to look for set method
  const set = "SET";
  // Variable to hold device ID for span
  const deviceId = device?.networkprovider_device_id;
  // Get all methods by object name
  // const methods = await getMethodsByObjectId(objectName);
  const methods = clockMethods;
  //   Find and store the restart method object
  const mSet = methods.find((m) => m.description === set);

  if (mSet) {
    cs = contentStructure(mSet, null);
    content = {
      ...cs,
      ...jsonBody,
    };

    const messageFormat = singleMessageStructure(device, content);
    const payload = JSON.stringify(messageFormat[0]);

    // Send message over span
    const spanResponse = await sendPayloadToDevice(deviceId, payload);

    if (spanResponse?.error) {
      return {
        error: true,
        span_response: spanResponse.errorMessage,
        message: spanResponse?.error?.message,
      };
    } else {
      return {
        span_response: spanResponse,
        payload_byte_size: calculateTotalByteSize(
          JSON.stringify(spanResponse.payload)
        ),
        payload: messageFormat[0],
      };
    }
  } else {
    return null;
  }
}

module.exports = {
  getDeviceTime,
  setDeviceTime,
};
